# Edutex Android Client

This project provides the Android client for the Edutex project. It contains Android applications for smartwatch and smartphone. The application is collecting sensor data from the Smartwatch and Smartphone and sending it via a Websocket and MQTT to an external infrastructure. Furthermore, Ecological Momentary Assessment is done on the Smartwatch to collect annotations for the sensor data and to conduct surveys.

# Authors and acknowledgment
Various developers have contributed since the beginning of this project.

## Core developers

- George-Petru Ciordas-Hertel (since 2020)
- Sebastian Rödling (2020-2022)

## Previous Student Developers & Researchers

- Felix Jian
- Lara Tabea Galkowski

# License
This Project is Open Source Software. You can redistribute it and modify it under the terms of the GNU General Public License version 3, as published by the Free Software Foundation, or any later version.

# Third Party Libraries 
- Application Crash Report for Android (**ACRA**), License: Apache-2.0 [Link](https://opensource.org/licenses/Apache-2.0), Github:  [Link](https://github.com/ACRA/acra)
- android-gif-drawable, License: MIT [Link](https://opensource.org/licenses/MIT), Github:  [Link](https://github.com/koral--/android-gif-drawable)
- ok-http3, License: Apache-2.0 [Link](https://opensource.org/licenses/Apache-2.0), MVNRepo:  [Link](https://mvnrepository.com/artifact/com.squareup.okhttp/okhttp)
- java-android-websocket-client, License: Apache-2.0 [Link](https://opensource.org/licenses/Apache-2.0), Github:  [Link](https://github.com/gusavila92/java-android-websocket-client)
- geofirebase, License: Apache-2.0 [Link](https://opensource.org/licenses/Apache-2.0), Github: [Link](https://github.com/firebase/geofire-android) 

# Project status
This project is currently used in multiple research projects such as Edutex (edutex.de).