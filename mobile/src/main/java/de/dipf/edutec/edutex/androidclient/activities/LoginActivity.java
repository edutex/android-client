package de.dipf.edutec.edutex.androidclient.activities;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.auth.AccountUtils;
import de.dipf.edutec.edutex.androidclient.dialogs.ErrorDialog;
import de.dipf.edutec.edutex.androidclient.dialogs.LoginSettingsDialog;
import de.dipf.edutec.edutex.androidclient.dialogs.PrivacyDialog;
import de.dipf.edutec.edutex.androidclient.dialogs.ProgressDialog;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;

public class LoginActivity extends AccountAuthenticatorActivity {



    public static final String ARG_ACCOUNT_TYPE = "accountType";
    public static final String ARG_AUTH_TOKEN_TYPE = "authTokenType";
    public static final String ARG_IS_ADDING_NEW_ACCOUNT = "isAddingNewAccount";
    public static final String PARAM_USER_PASSWORD = "password";
    private static final String TAG = LoginActivity.class.getSimpleName();

    private TextInputEditText username, password, host, port, identifier, study_identifier;
    private TextInputEditText moodle_id;
    private Button loginClicked;
    private ImageButton  settingsClicked;
    private TextView errorView;

    public ProgressDialog progressDialog;

    private AccountManager mAccountManager;
    private UserLoginTask mAuthTask = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.mAccountManager = AccountManager.get(this);
        findGUIElements();
        initServerIfMissing();

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        if(!esmConfigurationPreferences.getIsPrivacyAccepted()){
            PrivacyDialog privacyDialog = new PrivacyDialog(this, this);
            privacyDialog.showDialog();
        }

    }


    private void findGUIElements(){

        username = findViewById(R.id.login_username);
        password = findViewById(R.id.login_password);
        errorView = findViewById(R.id.login_error);
        host = findViewById(R.id.login_host);
        port = findViewById(R.id.login_port);
        port.setVisibility(View.GONE);
        identifier = findViewById(R.id.login_participant_id);
        study_identifier = findViewById(R.id.login_study_id);
        //moodle_id = findViewById(R.id.login_moodle_id);

        progressDialog = new ProgressDialog();

        loginClicked = findViewById(R.id.login_button);
        loginClicked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog.showProgress(LoginActivity.this,"Authenticating");
                attemptLogin();
            }
        });


        settingsClicked = findViewById(R.id.login_settings);
        settingsClicked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSettingsDialog();
            }
        });

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        String old_host = esmConfigurationPreferences.getHost();
        String old_port = esmConfigurationPreferences.getPort();
        String old_studyIdentifier = esmConfigurationPreferences.getStudyIdentifier();
        String old_moodle_id = esmConfigurationPreferences.getMoodleId();

        //if(old_moodle_id != null){moodle_id.setText(old_moodle_id);}
        if(old_host != null){ host.setText(old_host); }
        if(old_port != null){ port.setText(old_port); }
        if(old_studyIdentifier != null){ study_identifier.setText(old_studyIdentifier); }

        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
        String username = sessionPreferencesUtil.getUsername();
        if(username != ""){ this.username.setText(username);}

    }



    private void showSettingsDialog(){
        LoginSettingsDialog dialog = new LoginSettingsDialog(this);
        dialog.showDialog();



    }


    public void attemptLogin(){

        if(mAuthTask != null){
            return;
        }



        String mUsername = username.getText().toString();
        String mPassword = password.getText().toString();
        String mHost = host.getText().toString();
        String mStudyID = study_identifier.getText().toString();
        //String mMoodleID = moodle_id.getText().toString();
        errorView.setVisibility(View.GONE);
        errorView.setText("");

        boolean cancel = false;
        String errorMessage = "";

        if(TextUtils.isEmpty(mPassword)){
            errorMessage += "Provide a password. \n";
            cancel = true;
        }

        if(TextUtils.isEmpty(mUsername)){
            errorMessage +=  "Provide a username. \n";
            cancel = true;
        }

        if(TextUtils.isEmpty(mHost)){
            errorMessage += "Provide a host. \n";
            cancel = true;
        }

        if(TextUtils.isEmpty(mStudyID)){
            errorMessage += "Provide study id. \n";
            cancel = true;
        }



        if(cancel){
            errorView.setVisibility(View.VISIBLE);
            errorView.setText(errorMessage);
        } else {


            ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
            esmConfigurationPreferences.set_host(mHost);
            esmConfigurationPreferences.setStudyIdentifier(mStudyID);
            //esmConfigurationPreferences.setMoodleId(mMoodleID);

            mAuthTask = new UserLoginTask();
            mAuthTask.execute((Void) null);
        }



    }


    private void initServerIfMissing(){

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        esmConfigurationPreferences.set_is_https_if_missing(true);
        esmConfigurationPreferences.set_is_wss_if_missing(true);

    }

    public void displayErrorDialog(){
        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        String error_message = esmConfigurationPreferences.getErrorMessage();
        ErrorDialog errorDialog = new ErrorDialog(this);
        if(error_message != null){

            errorDialog.showDialog(error_message);
        }else{
            errorDialog.showDialog("Invalid Credentials.");
        }
    }


    public class UserLoginTask extends AsyncTask<Void, Void, Intent> {

        @Override
        protected Intent doInBackground(Void... params) {

            // TODO: attempt authentication against a network service.
            String mUsername = username.getText().toString();
            String mPassword = password.getText().toString();
            String authToken = AccountUtils.mServerAuthenticator.signIn(mUsername, mPassword, getApplicationContext());
            Log.d(TAG, "Authtoken:" + authToken);

            final Intent res = new Intent();

            res.putExtra(AccountManager.KEY_ACCOUNT_NAME, mUsername);
            res.putExtra(AccountManager.KEY_ACCOUNT_TYPE, AccountUtils.ACCOUNT_TYPE);
            res.putExtra(AccountManager.KEY_AUTHTOKEN, authToken);
            res.putExtra(PARAM_USER_PASSWORD, mPassword);

            return res;
        }

        @Override
        protected void onPostExecute(final Intent intent) {

            progressDialog.hideProgress();
            mAuthTask = null;
            //showProgress(false);

            if (null == intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)) {
                //errorView.setVisibility(View.VISIBLE);
                Log.d(TAG,"Showing error message");
                displayErrorDialog();
                password.requestFocus();
            } else {

                finishLogin(intent);
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }

        private void finishLogin(Intent intent) {
            final String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            final String accountPassword = intent.getStringExtra(PARAM_USER_PASSWORD);
            final Account account = new Account(accountName, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));
            String authToken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);

            if (getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
                // Creating the account on the device and setting the auth token we got
                // (Not setting the auth token will cause another call to the server to authenticate the user)
                mAccountManager.addAccountExplicitly(account, accountPassword, null);
                mAccountManager.setAuthToken(account, AccountUtils.AUTH_TOKEN_TYPE, authToken);
            } else {
                mAccountManager.setPassword(account, accountPassword);
            }

            setAccountAuthenticatorResult(intent.getExtras());
            setResult(AccountAuthenticatorActivity.RESULT_OK, intent);


            finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(AccountAuthenticatorActivity.RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void onPostCreate(Bundle save){
        super.onPostCreate(save);
        // Checking if client already accepted the privacy terms
        //PrivacyDialog privacyDialog = new PrivacyDialog(getApplicationContext());
        //privacyDialog.showDialog();
    }





}