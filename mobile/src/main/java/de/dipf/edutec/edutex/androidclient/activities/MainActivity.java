package de.dipf.edutec.edutex.androidclient.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.Wearable;
import com.google.android.material.navigation.NavigationView;

import org.acra.ACRA;
import org.acra.BuildConfig;
import org.acra.config.CoreConfigurationBuilder;
import org.acra.config.DialogConfigurationBuilder;
import org.acra.config.HttpSenderConfigurationBuilder;
import org.acra.data.StringFormat;

import java.util.Arrays;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.auth.AccountUtils;
import de.dipf.edutec.edutex.androidclient.auth.AuthPreferences;
import de.dipf.edutec.edutex.androidclient.boot.RunAfterBootService;
import de.dipf.edutec.edutex.androidclient.dialogs.PermissionDialog;
import de.dipf.edutec.edutex.androidclient.dialogs.PrivacyDialog;
import de.dipf.edutec.edutex.androidclient.fragments.HomeFragment;
import de.dipf.edutec.edutex.androidclient.fragments.ReplyCollectionFragment;
import de.dipf.edutec.edutex.androidclient.fragments.SensorFragment;
import de.dipf.edutec.edutex.androidclient.fragments.TrackingFragment;
import de.dipf.edutec.edutex.androidclient.messageservice.ESMService;
import de.dipf.edutec.edutex.androidclient.processtracking.WindowChangeDetectingService;
import de.dipf.edutec.edutex.androidclient.util.BetterActivityResult;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import de.dipf.edutec.edutex.androidclient.util.NotificationChannelUtil;
import de.dipf.edutec.edutex.androidclient.util.ServiceRunningUtil;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import de.dipf.edutec.edutex.androidclient.util.TrackingPermissionUtil;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    protected final BetterActivityResult<Intent, ActivityResult> activityLauncher = BetterActivityResult.registerActivityForResult(this);

    private HomeFragment homeFragment;

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;

    final int ASK_MULTIPLE_PERMISSION_REQUEST_CODE = 124;

    private AccountManager mAccountManager;
    private AuthPreferences mAuthPreferences;
    private String authToken;




    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        authToken = null;
        mAuthPreferences = new AuthPreferences(getApplicationContext());
        mAccountManager = AccountManager.get(this);

        // Ask for an auth token
        mAccountManager.getAuthTokenByFeatures(AccountUtils.ACCOUNT_TYPE, AccountUtils.AUTH_TOKEN_TYPE, null, this, null, null, new GetAuthTokenCallback(), null);


        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.CHANGE_WIFI_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.MODIFY_AUDIO_SETTINGS,
                        Manifest.permission.BODY_SENSORS},
                ASK_MULTIPLE_PERMISSION_REQUEST_CODE);


        setNavigationView();
        NotificationChannelUtil.create_notification_channel(getApplicationContext());


        Wearable.getCapabilityClient(getApplicationContext())
                .getCapability("bluetooth-status-mobile", CapabilityClient.FILTER_REACHABLE);

        //ReceiversManager.getInstance().registerReceivers(this);

    }



    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(this);
        if(esmConfigurationPreferences.isReminderActive()){
            Boolean noticiationEnabled = TrackingPermissionUtil.isNotificationServiceEnabled(this);
            Boolean appusagedEnabled = TrackingPermissionUtil.getGrantStatus(this);
            Boolean windowChangeEnabled = TrackingPermissionUtil.isAccessibilityServiceEnabled(this, WindowChangeDetectingService.class);
            if(!noticiationEnabled || !appusagedEnabled || !windowChangeEnabled){
                PermissionDialog permissionDialog = new PermissionDialog(this, noticiationEnabled,appusagedEnabled,windowChangeEnabled, this);
                permissionDialog.showDialog();
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mDrawer.openDrawer(GravityCompat.START);
            if (drawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setNavigationHeader() {
        nvDrawer = findViewById(R.id.nvView);

        View headerView = nvDrawer.getHeaderView(0);
        TextView headerVersionName = headerView.findViewById(R.id.nav_header_app_version);
        TextView headermoodleID= headerView.findViewById(R.id.nav_header_moodleId);
        TextView headerUsername = headerView.findViewById(R.id.nav_header_username);
        TextView headerParticipantID = headerView.findViewById(R.id.nav_header_participantId);
        AuthPreferences authPreferences = new AuthPreferences(getApplicationContext());
        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        String moodleID = esmConfigurationPreferences.getMoodleId();
        if(moodleID != null){
            headermoodleID.setText("Moodle ID: " + moodleID);
        }else{
            headermoodleID.setText("Moodle ID: " + "Not set");
        }

        headerVersionName.setText("Version: " + authPreferences.getAppVersion());
        headerUsername.setText("Username: " + authPreferences.getAccountName());
        headerParticipantID.setText("Study name: " + sessionPreferencesUtil.getStudyName());
    }

    public void setNavigationView() {
        toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        mDrawer = findViewById(R.id.drawer_layout);

        nvDrawer = findViewById(R.id.nvView);

        homeFragment = new HomeFragment();
        // Setting first default Layout
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.flContent, homeFragment)
                .commit();

        nvDrawer.getMenu().getItem(0).getSubMenu().getItem(0).setChecked(true);

        nvDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });

        drawerToggle = new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();

        mDrawer.addDrawerListener(drawerToggle);

        setNavigationHeader();

    }

    public void selectDrawerItem(MenuItem menuItem) {
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (menuItem.getItemId()) {
            case R.id.nav_item_tracking:
                fragmentClass = TrackingFragment.class;
                break;
            case R.id.nav_item_messages:
                fragmentClass = ReplyCollectionFragment.class;
                break;
            case R.id.nav_item_sensors:
                fragmentClass = SensorFragment.class;
                break;
            case R.id.nav_items_settings:
                fragmentClass = null;
                Intent intent = new Intent(this, SettingsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.nav_item_policy:

                PrivacyDialog privacyDialog = new PrivacyDialog(this,this);
                privacyDialog.showDialog();

                return;
                /**
            case R.id.nav_item_logout:

                stopService(new Intent(this, ESMService.class));
                finishAndRemoveTask();
                break;
                 **/

            case R.id.nav_item_logout_close:

                NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancelAll();

                ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
                esmConfigurationPreferences.setTrackingReminder(true);

                try{
                    AccountManager accountManager = (AccountManager) this.getSystemService(ACCOUNT_SERVICE);
                    AuthPreferences authPreferences = new AuthPreferences(getApplicationContext());
                    final Account account = new Account(authPreferences.getAccountName(), AccountUtils.ACCOUNT_TYPE);
                    accountManager.removeAccount(account,null,null,null);
                    accountManager.removeAccountExplicitly(account);
                }catch (Exception er){}


                mAccountManager.invalidateAuthToken(AccountUtils.ACCOUNT_TYPE, authToken);
                mAuthPreferences.removePreferences();

                stopService(new Intent(this, ESMService.class));
                stopService(new Intent(this, RunAfterBootService.class));

                finishAndRemoveTask();
                break;
            default:
                super.onOptionsItemSelected(menuItem);
                break;
        }

        if (fragmentClass != null) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        } else {

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, homeFragment).commit();
        }
        setTitle(menuItem.getTitle());

        // Reset selected MenuItem
        for (int i = 0; i <= nvDrawer.getMenu().size() - 1; i++) {
            for (int j = 0; j <= nvDrawer.getMenu().getItem(i).getSubMenu().size() - 1; j++) {
                nvDrawer.getMenu().getItem(i).getSubMenu().getItem(j).setChecked(false);
            }
        }
        menuItem.setChecked(true);
        mDrawer.closeDrawers();
    }

    private class GetAuthTokenCallback implements AccountManagerCallback<Bundle> {

        @Override
        public void run(AccountManagerFuture<Bundle> result) {
            Bundle bundle;

            try {

                bundle = result.getResult();


                final Intent intent = (Intent) bundle.get(AccountManager.KEY_INTENT);

                if (null != intent) {

                    activityLauncher.launch(intent, result1 -> {
                        if (result1.getResultCode() == Activity.RESULT_OK) {
                            Log.d(TAG, "Result okay");
                        }
                    });

                } else {


                    authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                    final String accountName = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);

                    // Save session username & auth token
                    mAuthPreferences.setAuthToken(authToken);
                    mAuthPreferences.setUsername(accountName);

                    // If the logged account didn't exist, we need to create it on the device
                    Account account = AccountUtils.getAccount(MainActivity.this, accountName);
                    if (null == account) {
                        account = new Account(accountName, AccountUtils.ACCOUNT_TYPE);
                        mAccountManager.addAccountExplicitly(account, bundle.getString(LoginActivity.PARAM_USER_PASSWORD), null);
                        mAccountManager.setAuthToken(account, AccountUtils.AUTH_TOKEN_TYPE, authToken);
                    }


                    if (!ServiceRunningUtil.isMyServiceRunning(ESMService.class, getApplicationContext())) {
                        startForegroundService(new Intent(getApplicationContext(), ESMService.class));
                    }

                    if(!ServiceRunningUtil.isMyServiceRunning(RunAfterBootService.class, getApplicationContext())){
                        startForegroundService(new Intent(getApplicationContext(), RunAfterBootService.class));
                    }

                    createACRA();

                }
            } catch (OperationCanceledException e) {

                // If signup was cancelled, force activity termination
                Log.e(TAG, Arrays.toString(e.getStackTrace()));
                e.printStackTrace();
                finish();

            } catch (Exception e) {

                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            }


            setNavigationHeader();

            /**
            if (!ServiceRunningUtil.isMyServiceRunning(ESMService.class, getApplicationContext())) {
                startForegroundService(new Intent(getApplicationContext(), ESMService.class));
            }

            if(!ServiceRunningUtil.isMyServiceRunning(RunAfterBootService.class, getApplicationContext())){
                startForegroundService(new Intent(getApplicationContext(), RunAfterBootService.class));
            }

            createACRA();
            */


        }

    }


    private void createACRA(){
        CoreConfigurationBuilder builder = new CoreConfigurationBuilder(getApplication());

        AuthPreferences authPreferences = new AuthPreferences(getApplicationContext());
        String username = authPreferences.getAccountName();
        String password = authPreferences.getPassword();

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        String suffix = "/acra/_design/acra-storage/_update/report/";
        String url = esmConfigurationPreferences.get_configured_url() + suffix;

        if(username == null || password == null){
            return;
        }

        builder
                .withBuildConfigClass(BuildConfig.class)
                .withReportFormat(StringFormat.JSON)
                .setExcludeMatchingSharedPreferencesKeys(new String[]{"key_esm_password"});

        builder.getPluginConfigurationBuilder(HttpSenderConfigurationBuilder.class)
                .withBasicAuthLogin(username)
                .withBasicAuthPassword(password)
                .withUri(url)
                .withEnabled(true);

        //each plugin you chose above can be configured with its builder like this:
        builder.getPluginConfigurationBuilder(DialogConfigurationBuilder.class)
                .withResText(R.string.acra_toast_text)
                .withEmailPrompt("Freiwillige Email Adresse:")
                .withCommentPrompt("Beschreibe bitte wie es zu dem Fehler kam. So können wir den Bug reproduzieren und beheben.")
                .withNegativeButtonText("Bericht nicht senden.")
                .withPositiveButtonText("Bericht senden.")
                .withText("Leider ist etwas schief gelaufen. Bitte starte deine Lernsession erneut.")
                .withTitle("Ein Fehler ist aufgetreten.")
                .withEnabled(true);

        ACRA.init(getApplication(), builder);

    }

}

