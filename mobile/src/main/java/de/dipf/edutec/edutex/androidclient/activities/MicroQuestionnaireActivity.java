package de.dipf.edutec.edutex.androidclient.activities;

import static android.view.View.GONE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.TextViewCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.microquestionnaire.MicroChoiceAdapter;
import de.dipf.edutec.edutex.androidclient.microquestionnaire.MicroQuestionnairePreference;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class MicroQuestionnaireActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = "MicroInteractionSurveyActivity";
    private Intent mIntent;
    private JSONObject mSurvey;
    private JSONArray mAnswers;
    private MicroChoiceAdapter microChoiceAdapter;
    private RecyclerView recyclerView;
    private Button firstButton, secondButton, thirdButton;
    private TextView questionText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_micro_questionnaire);

        MicroQuestionnairePreference prefs = new MicroQuestionnairePreference(getApplicationContext());
        if(prefs.isMicroQuestionnaireAvailable()){
            try {
                mSurvey = prefs.getCurrentMicroQuestionnaire();
                buildLayout();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            finish();
        }



    }


    private void buildLayout() throws JSONException {

        questionText = findViewById(R.id.uSurvey_question);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
                questionText,
                6,
                16,1,
                TypedValue.COMPLEX_UNIT_SP);

        firstButton = findViewById(R.id.first_answers);
        secondButton = findViewById(R.id.second_answers);
        thirdButton = findViewById(R.id.third_answers);

        questionText.setText(mSurvey.getJSONObject("micro_questions").getString("question"));

        // Preparing Answers
        mAnswers = mSurvey.getJSONObject("micro_questions").getJSONArray("micro_answers");
        String[] answers_strings = new String[mAnswers.length()];


        for(int i = 0; i < mAnswers.length(); i++){
            answers_strings[i] = mAnswers.getJSONObject(i).getString("answer");
        }

        if ( mAnswers.length() == 3){

            firstButton.setText(answers_strings[0]);
            firstButton.setOnClickListener(this);
            secondButton.setText(answers_strings[1]);
            secondButton.setOnClickListener(this);
            thirdButton.setText(answers_strings[2]);
            thirdButton.setOnClickListener(this);


        } else if ( mAnswers.length() == 2){
            firstButton.setText(answers_strings[0]);
            firstButton.setOnClickListener(this);
            secondButton.setText(answers_strings[1]);
            secondButton.setOnClickListener(this);
            thirdButton.setVisibility(GONE);

        } else if ( mAnswers.length() == 1){
            firstButton.setText(answers_strings[0]);
            firstButton.setOnClickListener(this);
            secondButton.setVisibility(GONE);
            thirdButton.setVisibility(GONE);

        }
    }



    @Override
    protected void onPause(){
        super.onPause();
    }



    @SneakyThrows
    @Override
    public void onClick(View v) {
        Instant instant = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();
        int choosen_id = v.getId();
        String answer_text = "";
        if(choosen_id == firstButton.getId()){
            answer_text = firstButton.getText().toString();

        } else if (choosen_id == secondButton.getId()){
            answer_text = secondButton.getText().toString();

        } else if (choosen_id == thirdButton.getId()){
            answer_text = thirdButton.getText().toString();
        }

        if(mAnswers.length() > 0){

            JSONArray timestamps_asked = new JSONArray();
            Log.d(TAG,mAnswers.toString());

            if(mSurvey.has("initial_timestamp_asked")){
                try {
                    timestamps_asked.put(mSurvey.getString("initial_timestamp_asked"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if(mSurvey.has("second_timestamp_asked")){
                try {
                    timestamps_asked.put(mSurvey.getString("second_timestamp_asked"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            try{
                Log.d(TAG,"Choosen ID " + String.valueOf(choosen_id));
                mSurvey.put("timestamp_question_answered", RequestResponseUtils.getTimeStamps(instant,elapsed));
                mSurvey.put("timestamps_question_asked",timestamps_asked);
                mSurvey.put("user_answer", getAnswerId(answer_text));
            } catch (JSONException e){
                e.printStackTrace();
            }

            MessagesSingleton.getInstance().addMessageReceived(mSurvey);

            Intent intent = new Intent();
            intent.setAction(getString(R.string.ACTION_MICRO_SURVEY));
            intent.putExtra("remove_message",mSurvey.toString());
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

            finishAffinity();
        }

    }

    public int getAnswerId(String answer){
        for(int i=0; i<mAnswers.length(); i++){
            try {
                if(mAnswers.getJSONObject(i).getString("answer").equals(answer)){
                    return mAnswers.getJSONObject(i).getInt("id");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

}

