package de.dipf.edutec.edutex.androidclient.activities;

import static java.lang.StrictMath.abs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.SwipeDismissFrameLayout;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.questionnaire.QuestionnaireFragmentChoice;
import de.dipf.edutec.edutex.androidclient.questionnaire.QuestionnaireFragmentSlider;
import de.dipf.edutec.edutex.androidclient.questionnaire.SubmitFragment;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import lombok.SneakyThrows;

public class QuestionnaireActivity extends AppCompatActivity {


    public static String TAG = QuestionnaireActivity.class.getSimpleName();
    private int frameLayoutID = R.id.questionnaire_frame;
    // Backup
    JSONObject surveyTotal;
    JSONArray questionSequence;

    // Flow & Response
    JSONObject currObj;
    public JSONArray toEditSequence;
    private int numQuestions;
    private int currIndex;
    private long lastGestureEvent;
    public boolean surveyFinished;

    @SneakyThrows
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);
        setGestureDetector();

        Intent intent = getIntent();
        String surveyToDigest = intent.getStringExtra("message");
        surveyTotal = new JSONObject(surveyToDigest);

        if(surveyTotal.has("faked")){
            if(surveyTotal.getString("faked").equals("pre_survey")){

                SessionPreferencesUtil sessionPreferences = new SessionPreferencesUtil(getApplicationContext());
                sessionPreferences.setSessionRunning(true);
                MessagesSingleton.getInstance().addMessageReceived(surveyTotal);
                MessagesSingleton.getInstance().RequestSessionStart();
                finish();

            } else if (surveyTotal.getString("faked").equals("post_survey")){

                MessagesSingleton.getInstance().addMessageReceived(surveyTotal);
                finish();

            }
        } else {

            questionSequence = new JSONArray(surveyTotal.getString("questions"));
            toEditSequence = new JSONArray(surveyTotal.getString("questions"));
            Log.d(TAG,questionSequence.toString());
            currIndex = -1;
            surveyFinished = false;
            setNextFragment();

        }


    }


    @SneakyThrows
    @Override
    protected void onStop(){
        super.onStop();
        if(!surveyFinished){
            // Renotify edited Sequence
            JSONObject toSend = surveyTotal;
            toSend.put("questions",toEditSequence);
            Intent intent = new Intent();
            intent.setAction(getString(R.string.ACTION_SURVEY));
            intent.putExtra("message",toSend.toString());
            intent.putExtra("renotification","true");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

            // Removing Survey from
            JSONObject obj = surveyTotal;
            obj.remove("identifier");
            obj.remove("timestamp_question_asked");

            finish();
        }

    }



    private void buildFragmentChoices(JSONObject obj) throws JSONException {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(frameLayoutID, new QuestionnaireFragmentChoice(this, obj))
                .commit();
    }
    private void buildFragmentSlider(JSONObject obj) throws JSONException {
        obj.put("user_answer",0);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(frameLayoutID, new QuestionnaireFragmentSlider(obj))
                .commit();

    }
    public void setOnTopFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(frameLayoutID, fragment)
                .commit();
    }

    @SneakyThrows
    public void getBackToFragment(){
        setNextFragment();
        setLastFragment();
    }
    public void setNextFragment() throws JSONException {


        Log.d(TAG, "setNext called"+ " currIndex: " + String.valueOf(currIndex));

        currIndex += 1;
        if(currIndex == questionSequence.length() ){
            // Call Submit Fragment
            JSONObject toSend = surveyTotal;
            toSend.put("questions",toEditSequence);

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right,R.anim.slide_out_left)
                    .replace(frameLayoutID, new SubmitFragment( toSend))
                    .commit();

        } else {
            if(currIndex < questionSequence.length()){

                // Call any other Survey Question
                try{
                    JSONObject next = toEditSequence.getJSONObject(currIndex);
                    if(next.getInt("display_type") == 1){
                        buildFragmentChoices(next);
                    } else {
                        if(next.getInt("display_type") == 0){
                            buildFragmentSlider(next);
                        }
                    }

                } catch(Exception e){
                    e.printStackTrace();
                }

            }

        }



    }
    public void setLastFragment() throws JSONException {

        Log.d(TAG, "setLast called" + " currIndex: " + String.valueOf(currIndex));

        if(currIndex - 1 >= 0){
            currIndex -= 1;
            FragmentManager fragmentManager = getSupportFragmentManager();
            JSONObject obj = toEditSequence.getJSONObject(currIndex);
            if(obj.getInt("display_type") == 1 ){

                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_right)
                        .replace(frameLayoutID, new QuestionnaireFragmentChoice(this, obj))
                        .commit();
            } else {
                if(obj.getInt("display_type") == 0){
                    fragmentManager
                            .beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_right)
                            .replace(frameLayoutID, new QuestionnaireFragmentSlider( obj))
                            .commit();
                }
            }


        }




    }
    public void replaceQuestion(JSONObject obj) throws JSONException {
        String order = obj.getString("display_order");

        for(int i = 0; i < toEditSequence.length(); i++){

            if(toEditSequence.getJSONObject(i).getString("display_order").equals(order)){
                toEditSequence.put(i,obj);
            }

        }
    }








    // Gesture Recognition
    private SwipeDismissFrameLayout mDismissOverlay;
    private GestureDetector mDetector;
    private void setGestureDetector(){

        mDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {


            @SneakyThrows
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                                    float distanceY) {

                if(System.currentTimeMillis() - lastGestureEvent > 500L) {
                    lastGestureEvent = System.currentTimeMillis();
                    if (abs(distanceX) > 5) {

                        if (distanceX < 0) {

                            try {
                                setLastFragment();
                                Log.d("GestureTAG", "swipeLeft");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        if (distanceX > 0) {

                            try {
                                setNextFragment();
                                Log.d("GestureTAG", "swipeRight");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                } else{
                    System.out.println(lastGestureEvent - System.currentTimeMillis());
                }

                return true;
            }

        });
    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return mDetector.onTouchEvent(ev) || super.onTouchEvent(ev);
    }
}

