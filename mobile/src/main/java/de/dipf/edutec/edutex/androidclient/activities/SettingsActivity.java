package de.dipf.edutec.edutex.androidclient.activities;

import android.content.*;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import org.json.JSONObject;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.boot.RunAfterBootService;
import de.dipf.edutec.edutex.androidclient.messageservice.SendMessageLayer;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import de.dipf.edutec.edutex.androidclient.util.ServiceRunningUtil;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import lombok.SneakyThrows;


public class SettingsActivity extends AppCompatActivity {

    public static final String TAG = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        setContentView(R.layout.activity_settings);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


    }

    public static class SettingsFragment extends PreferenceFragmentCompat {

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            Context context = getActivity().getApplicationContext();
            ListPreference listPreference = findPreference("key_pref_device");
            ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);

            listPreference.setValueIndex(esmConfigurationPreferences.getPrefDevice());
            listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getActivity().getApplicationContext());
                    try{
                        if (sessionPreferencesUtil.getSessionRunning()){
                            Toast.makeText(getActivity().getApplicationContext(),"You cannot change settings during a session",Toast.LENGTH_LONG).show();
                            return false;
                        }
                    }catch (Exception er){}


                    int myValue = Integer.valueOf(String.valueOf(newValue));
                    esmConfigurationPreferences.setPrefDevice(myValue);
                    updateSW();
                    return true;
                }
            });

            /*
            ListPreference listPreference1 = findPreference("key_pref_study_mechanics");
            listPreference1.setValueIndex(esmConfigurationPreferences.getStudyStartMechanics());
            listPreference1.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    int myValue = Integer.valueOf(String.valueOf(newValue));
                    esmConfigurationPreferences.setStudyStartMechanics(myValue);
                    updateSW();

                    if(myValue == 0){
                        if(!ServiceRunningUtil.isMyServiceRunning(RunAfterBootService.class,getActivity().getApplicationContext())){
                            getActivity().startForegroundService(new Intent(getActivity().getApplicationContext(),RunAfterBootService.class));
                        }
                    }

                    return true;
                }
            });
            */

            /*
            EditTextPreference preference = findPreference(context.getString(R.string.key_general_user_id));

            SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getActivity().getApplicationContext());


            preference.setText(sessionPreferencesUtil.getParticipantId());
            preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    Log.d(TAG, "New Value: " + newValue);
                    try{
                        String tmp = String.valueOf(newValue);
                        sessionPreferencesUtil.setParticipantId(tmp);
                        return true;
                    }catch (Exception e){
                        Toast.makeText(context,"Your ID should be an integer (number)",Toast.LENGTH_LONG).show();
                        return false;
                    }

                }
            });
            */

            /*
             // MQTT Handling

            accountManager = AccountManager.get(getContext());

            removeCredentials = findPreference(getString(R.string.key_mqtt_remove_credentials));
            Account account = getAccount();
            if (account == null) {
                removeCredentials.setEnabled(false);
            }

            FragmentActivity activity = requireActivity();

            ((Preference) Objects.requireNonNull(findPreference(getString(R.string.key_mqtt_edit_credentials)))).setOnPreferenceClickListener(preference -> {
                Account ac = getAccount();
                if (ac == null) {
                    Log.e(TAG, "Account not found.");
                    accountManager.addAccount(ACCOUNT_TYPE, null, null, null, activity, null, null);
                    return true;
                }

                accountManager.updateCredentials(ac, null, null, activity, null, null);
                removeCredentials.setEnabled(true);
                return true;
            });

            removeCredentials.setOnPreferenceClickListener(preference -> {
                accountManager.removeAccount(getAccount(), activity, null, null);
                preference.setEnabled(false);
                disconnect();
                return true;
            });


            ((Preference) Objects.requireNonNull(findPreference(getString(R.string.key_mqtt_host)))).setOnPreferenceClickListener(this::disconnect);
            ((Preference) Objects.requireNonNull(findPreference(getString(R.string.key_mqtt_port)))).setOnPreferenceClickListener(this::disconnect);
            ((Preference) Objects.requireNonNull(findPreference(getString(R.string.key_general_user_id)))).setOnPreferenceClickListener(this::disconnect);


            Context applicationContext = Objects.requireNonNull(activity).getApplicationContext();
            SharedPreferences sharedPreferences = applicationContext.getSharedPreferences(
                    applicationContext.getPackageName(), Context.MODE_PRIVATE);


            Preference session_id = Objects.requireNonNull(findPreference(getString(R.string.key_general_session_id)));
            String prefTitle = "Session ID%s";
            boolean hasAnySession = true;
            String activeSession = sharedPreferences.getString(getString(R.string.key_general_session_id), null);

            if (activeSession != null) {
                session_id.setTitle(String.format(prefTitle, " (active)"));
                session_id.setSummary(activeSession);
            } else {
                List<String> pastSessions = loadListPreference(sharedPreferences, "past_sessions");
                if (pastSessions != null && !pastSessions.isEmpty()) {
                    String lastSessionId = pastSessions.get(0);
                    session_id.setTitle(String.format(prefTitle, " (inactive)"));
                    session_id.setSummary(lastSessionId);
                } else {
                    hasAnySession = false;
                    session_id.setTitle(String.format(prefTitle, ""));
                    session_id.setSummary("None");
                }
            }

            if (hasAnySession) {
                session_id.setOnPreferenceClickListener(preference -> {
                    copyToClipboard(activity, preference.getSummary(), "Session ID");
                    return true;
                });
            }

            // ESM HANDLING
            ListPreference esm_mode_lp = findPreference(getString(R.string.key_lp_esm_mode));


            esm_mode_lp.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String tmp = String.valueOf(newValue);
                    Log.d(TAG, "New Mode: " + tmp + " is selected");
                    if (tmp == getString(R.string.esm_mode_deployed)) {
                        findPreference(getString(R.string.key_pc_esm_development)).setVisible(false);
                        findPreference(getString(R.string.key_pc_esm_deployed)).setVisible(true);
                        Toast.makeText(getContext(), "Deployed Mode enabled", Toast.LENGTH_SHORT).show();
                    } else if (tmp == getString(R.string.esm_mode_development)) {
                        findPreference(getString(R.string.key_pc_esm_deployed)).setVisible(false);
                        findPreference(getString(R.string.key_pc_esm_development)).setVisible(true);
                        Toast.makeText(getContext(), "Development Mode enabled", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });
            */
        }



        @Override
        public void onResume() {
            super.onResume();
            Log.d(TAG, "onResume is called");

            /*
            removeCredentials.setEnabled(getAccount() != null);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            PreferenceCategory devlopment = findPreference(getString(R.string.key_pc_esm_development));
            PreferenceCategory deployed = findPreference(getString(R.string.key_pc_esm_deployed));
            Log.d(TAG, "onResume is called, current Mode: " + sharedPreferences.getString(getString(R.string.key_lp_esm_mode), ""));
            if (sharedPreferences.getString(getString(R.string.key_lp_esm_mode), "").equals("mode_deployed")) {
                Log.d(TAG, "onResume: Deployed Mode is selected");
                deployed.setVisible(true);
                devlopment.setVisible(false);
            } else if (sharedPreferences.getString(getString(R.string.key_lp_esm_mode), "").equals("mode_development")) {
                Log.d(TAG, "onResume: Development Mode is selected");
                deployed.setVisible(false);
                devlopment.setVisible(true);
            }
            */
        }

        private boolean disconnect(Preference preference) {
            //return disconnect();
            return true;
        }

        private boolean disconnect() {
            //((CustomApplication) getActivity().getApplication()).getContext().getMqttService().disconnect();
            return true;
        }

        @SneakyThrows
        private void updateSW(){
            SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getContext());
            SendMessageLayer sendMessageLayer = new SendMessageLayer(getContext());
            JSONObject response = sessionPreferencesUtil.getSessionInformation();
            if(response != null){
                sendMessageLayer.sendMessage(getString(R.string.PATH_TO_SMARTWATCH_RESPONSE_SESSION_STATE),response);
            }
        }


    }
}