package de.dipf.edutec.edutex.androidclient.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Log;

public class AccountUtils {

    private static final String TAG = AccountUtils.class.getSimpleName();
    public static final String ACCOUNT_TYPE = "edutex";
    public static final String AUTH_TOKEN_TYPE = "edutex.aaa";

    public static IServerAuthenticator mServerAuthenticator = new MyServerAuthenticator();

    public static Account getAccount(Context context, String accountName) {
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
        for (Account account : accounts) {
            Log.d(TAG, "Account Name: " + account.name + " in search of " + accountName);
            if (account.name.equalsIgnoreCase(accountName)) {
                return account;
            }
        }
        return null;
    }

}
