package de.dipf.edutec.edutex.androidclient.auth;
import de.dipf.edutec.edutex.androidclient.BuildConfig;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import lombok.SneakyThrows;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONObject;

public class AuthPreferences {

    private Context context;

    private static final String PREFS_NAME = "auth";
    private static final String KEY_ACCOUNT_NAME = "account_name";
    private static final String KEY_AUTH_TOKEN = "auth_token";
    private static final String KEY_ROLE = "user_role";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_PARTICIPATN_ID = "participant_id";
    private static final String KEY_PASSWORD = "password";


    private SharedPreferences preferences;

    public AuthPreferences(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        this.context = context;
    }

    public String getAccountName() {
        return preferences.getString(KEY_ACCOUNT_NAME, null);
    }

    public String getAuthToken() {
        return preferences.getString(KEY_AUTH_TOKEN, null);
    }

    public void setUsername(String accountName) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_ACCOUNT_NAME, accountName);
        editor.commit();
    }
    public void setPassword(String password){
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString(KEY_PASSWORD, password);
        editor.commit();
    }
    public void setAuthToken(String authToken) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_AUTH_TOKEN, authToken);
        editor.commit();
    }

    public void setUserID(int id){
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_USER_ID, id);
        editor.commit();
    }


    public String getAppVersion(){
        return BuildConfig.VERSION_NAME;
    }

    public void removePreferences(){
        final SharedPreferences.Editor editor = preferences.edit();
        editor.remove(KEY_ACCOUNT_NAME);
        editor.remove(KEY_ROLE);
        editor.remove(KEY_USER_ID);
        editor.remove(KEY_AUTH_TOKEN);
        editor.remove(KEY_PASSWORD);
        editor.commit();
    }

    public int getUserID(){
        return preferences.getInt(KEY_USER_ID, -1);
    }


    public void setParticipantID(String id){
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_PARTICIPATN_ID,id);
        editor.commit();
    }

    public String getParticipantID(){
        return preferences.getString(KEY_PARTICIPATN_ID,null);
    }
    public String getPassword(){return this.preferences.getString(KEY_PASSWORD,null);}

    @SneakyThrows
    public JSONObject getCredentials(){
        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(this.context);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pw",getPassword());
        jsonObject.put("username",getAccountName());
        jsonObject.put("url",esmConfigurationPreferences.get_configured_url());
        return jsonObject;
    }



}
