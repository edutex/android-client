package de.dipf.edutec.edutex.androidclient.auth;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;

import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import okhttp3.Call;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MyServerAuthenticator implements IServerAuthenticator{

    private final String TAG = MyServerAuthenticator.class.getSimpleName();

    private final String TOKEN_API = "/auth/token/";


    @Override
    public String signUp(String email, String username, String password) {
        // TODO: register new user on the server and return its auth token
        return null;
    }

    @Override
    public String signIn(String username, String password, Context context) {


        String authToken = null;

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);
        String used_url = esmConfigurationPreferences.get_configured_url();


       RequestBody requestBody = new MultipartBody.Builder()
               .setType(MultipartBody.FORM)
               .addFormDataPart("username",username)
               .addFormDataPart("password",password)
               .build();

       Request request = new Request.Builder()
               .post(requestBody)
               .url(used_url + TOKEN_API)
               .build();

       Log.d(TAG, "Used url: " + used_url);

        OkHttpClient client = new OkHttpClient();
        Call call  = client.newCall(request);
        int second_request_okay = 0;
        try{
            Response response = call.execute();
            if(response.isSuccessful()){

                String token = response.body().string();
                Log.d(TAG,"Response Login: " + token);
                JSONObject json_token = new JSONObject(token);
                authToken = json_token.getString("token");
                second_request_okay = getPseudonym(authToken, used_url, esmConfigurationPreferences, context);

                AuthPreferences authPreferences = new AuthPreferences(context);
                authPreferences.setPassword(password);

            }else{

                String response_message = response.body().string();
                JSONObject jsonObject = new JSONObject(response_message);
                esmConfigurationPreferences.setErrorMessage("Authentication failed. \nCode: " + String.valueOf(response.code()) +
                        "\nResponse:\n" + jsonObject.toString());
            }
        }catch (Exception e){
            esmConfigurationPreferences.setErrorMessage(e.getMessage());
            Log.e(TAG,e.toString());
        }


        if(second_request_okay == 200){
            Log.d(TAG, "Returning Token: " + authToken);
            return authToken;
        } else {

            return null;
        }
    }

    private int getPseudonym(String token, String url, ESMConfigurationPreferences conf, Context context){

        String final_token = "Token " + token;
        String final_url = url + "/auth/participants/";
        String study_identifier = conf.getStudyIdentifier();
        String moodle_id = conf.getMoodleId();
        Log.d(TAG,"getPseudonym: " + final_token);
        Log.d(TAG, "getPseudonym: " + final_url);
        Log.d(TAG, "getPseudonym: studyIdentifier: " +study_identifier );
        Log.d(TAG,"Moodle ID: " + moodle_id);
        if(moodle_id == null){
            moodle_id = "Unknown";
        }
        try{

            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("study_identifier",study_identifier)
                    .addFormDataPart("moodle_id",moodle_id)
                    .build();


            Request request = new Request.Builder()
                    .addHeader("Authorization", final_token)
                    .post(requestBody)
                    .url(final_url)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Call call = client.newCall(request);

            Response response = call.execute();
            if(response.isSuccessful()){
                String body = response.body().string();
                Log.d(TAG, "Details: " + body);
                JSONObject jsonObject = new JSONObject(body);

                AuthPreferences authPreferences = new AuthPreferences(context);
                authPreferences.setUserID(jsonObject.getJSONObject("user").getInt("id"));

                SessionPreferencesUtil sessionPreferencesUtil  = new SessionPreferencesUtil(context);
                sessionPreferencesUtil.setStudyName(jsonObject.getJSONObject("study").getString("name"));
                sessionPreferencesUtil.setParticipantId(jsonObject.getString("pseudonym"));

                ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);
                boolean study_is_remote_only = jsonObject.getJSONObject("study").getBoolean("remote_started");
                int remote_started = study_is_remote_only ? 0 : 1;

                esmConfigurationPreferences.setStudyStartMechanics(remote_started);

                try{
                    String moodle_id_backend = jsonObject.getString("moodle_id");
                    if(moodle_id_backend != null){
                        esmConfigurationPreferences.setMoodleId(moodle_id_backend);
                    }

                }catch (Exception er){
                    Log.e(TAG,"Could not fetch Moodle ID.");
                }


                return 200;
            } else {
                Log.d(TAG,"getPseudonym() request was not successful");
                String body = response.body().string();
                Log.d(TAG,"Details :" + body);
                JSONObject jsonObject = new JSONObject(body);
                if(jsonObject.has("message")){
                    conf.setErrorMessage(jsonObject.getString("message"));
                }else{
                    conf.setErrorMessage("Could not fetch pseudonym data.");
                }
                return 400;
            }

        } catch (JSONException | IOException e) {
            conf.setErrorMessage("JSONException in getPseudonym()");
            Log.e(TAG, e.getMessage());



        }

        return 400;
    }



}
