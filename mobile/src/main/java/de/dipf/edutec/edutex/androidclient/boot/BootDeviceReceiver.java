package de.dipf.edutec.edutex.androidclient.boot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import de.dipf.edutec.edutex.androidclient.util.ServiceRunningUtil;

public class BootDeviceReceiver extends BroadcastReceiver {


    private static final String TAG = BootDeviceReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        String message = "BootDeviceReceiver onReceive, action is " + action;
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        Log.d(TAG, action);
        if(action.equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED))
        {
            startServiceDirectly(context);
            //startServiceByAlarm(context);
        }
    }

    private void startServiceDirectly(Context context)
    {
        String message = "BootDeviceReceiver onReceive start service directly.";
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        Log.d(TAG, message);
        // This intent is used to start background service. The same service will be invoked for each invoke in the loop.
        if(!ServiceRunningUtil.isMyServiceRunning(RunAfterBootService.class,context)){
            Intent startServiceIntent = new Intent(context, RunAfterBootService.class);
            context.startForegroundService(startServiceIntent);
        }


    }


}
