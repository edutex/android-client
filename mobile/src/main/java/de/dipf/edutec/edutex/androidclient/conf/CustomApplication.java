package de.dipf.edutec.edutex.androidclient.conf;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import androidx.core.app.NotificationCompat;

import de.dipf.edutec.edutex.androidclient.R;
import lombok.Getter;


@Getter
public class CustomApplication extends Application {
    private ApplicationContext context;
    private static Thread.UncaughtExceptionHandler mDefaultUncaughtExceptionHandler;

    private Thread.UncaughtExceptionHandler mCaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            String CHANNEL_ID = getResources().getString(R.string.notiChannelID);

            Notification notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                    .setContentTitle("Smartphone App Crashed")
                    .setContentText("Die Smartphone App hatte einen Fehler. Bitte starte die Apps/Lernsession erneut.")
                    .setSmallIcon(R.drawable.ic_cc_checkmark)
                    .build();

            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0, notification);
            mDefaultUncaughtExceptionHandler.uncaughtException(thread, ex);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        this.context = new ApplicationContext(
                getApplicationContext(),
                getSystemService(NotificationManager.class));


        // Second, cache a reference to default uncaught exception handler
        mDefaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        // Third, set custom UncaughtExceptionHandler
        Thread.setDefaultUncaughtExceptionHandler(mCaughtExceptionHandler);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
      /*
      CoreConfigurationBuilder builder = new CoreConfigurationBuilder(this);


      builder
              .withBuildConfigClass(BuildConfig.class)
              .withReportFormat(StringFormat.JSON)
              .setExcludeMatchingSharedPreferencesKeys(new String[]{"key_esm_password"});

      builder.getPluginConfigurationBuilder(HttpSenderConfigurationBuilder.class)
              .withBasicAuthLogin(getResources().getString(R.string.acra_user))
              .withBasicAuthPassword(getResources().getString(R.string.acra_password))
              .withUri(getResources().getString(R.string.acra_url))
              .withEnabled(true);

      //each plugin you chose above can be configured with its builder like this:
      builder.getPluginConfigurationBuilder(DialogConfigurationBuilder.class)
              .withResText(R.string.acra_toast_text)
              .withEmailPrompt("Freiwillige Email Adresse:")
              .withCommentPrompt("Beschreibe bitte wie es zu dem Fehler kam. So können wir den Bug reproduzieren und beheben.")
              .withNegativeButtonText("Bericht nicht senden.")
              .withPositiveButtonText("Bericht senden.")
              .withText("Leider ist etwas schief gelaufen. Bitte starte deine Lernsession erneut.")
              .withTitle("Ein Fehler ist aufgetreten.")
              .withEnabled(true);

      ACRA.init(this, builder);

      */

    }

    public ApplicationContext getContext() {
        return context;
    }
}
