package de.dipf.edutec.edutex.androidclient.conf;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.dipf.edutec.edutex.androidclient.sensorservice.DataLayerListenerService;

public class OnBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent i) {
        Intent intent = new Intent(context, DataLayerListenerService.class);
        context.startForegroundService(intent);
    }
}
