package de.dipf.edutec.edutex.androidclient.conf;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.Objects;
import java.util.Set;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.microquestionnaire.MicroSurveyNotificationPublisher;
import de.dipf.edutec.edutex.androidclient.notification.NotificationPublisher;
import de.dipf.edutec.edutex.androidclient.questionnaire.QuestionnaireNotificationPublisher;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import lombok.SneakyThrows;

public class ReceiversManager {

    public static ReceiversManager instance;
    private final String TAG = ReceiversManager.class.getSimpleName();

    QuestionnaireNotificationPublisher questionnaireNotificationPublisher;
    NotificationPublisher notificationPublisher;
    MicroSurveyNotificationPublisher microSurveyNotificationPublisher;
    CapabilityClient capabilityClient;
    CapabilityClient.OnCapabilityChangedListener listener;

    public ReceiversManager(){}

    public static ReceiversManager getInstance(){
        if(ReceiversManager.instance == null){
            ReceiversManager.instance = new ReceiversManager();
        }
        return ReceiversManager.instance;
    }


    public void registerReceivers(Context context){
        try{
            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(context);

            IntentFilter filterSurvey = new IntentFilter(context.getString(R.string.ACTION_SURVEY));
            questionnaireNotificationPublisher = new QuestionnaireNotificationPublisher();
            manager.registerReceiver(questionnaireNotificationPublisher,filterSurvey);

            IntentFilter filterNotify = new IntentFilter(context.getString(R.string.ACTION_NOTIFY));
            notificationPublisher = new NotificationPublisher();
            manager.registerReceiver(notificationPublisher,filterNotify);


            IntentFilter filterMicroSurvey = new IntentFilter(context.getString(R.string.ACTION_MICRO_SURVEY));
            microSurveyNotificationPublisher = new MicroSurveyNotificationPublisher();
            manager.registerReceiver(microSurveyNotificationPublisher,filterMicroSurvey);
            Log.d(TAG,"Registered Receivers");
        }catch (Exception er){
            Log.e(TAG,er.getMessage());
        }



    }


    public void registerCapabilityClient(Context context){
        try{



            ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);
            SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(context);

            Task<CapabilityInfo> capabilityInfoTask = Wearable.getCapabilityClient(context)
                    .getCapability("bluetooth-status-wear",CapabilityClient.FILTER_REACHABLE);
            capabilityInfoTask.addOnCompleteListener(new OnCompleteListener<CapabilityInfo>() {
                @Override
                public void onComplete(@NonNull Task<CapabilityInfo> task) {
                    if(task.isSuccessful()){

                        CapabilityInfo capabilityInfo = task.getResult();
                        Set<Node> nodes = Objects.requireNonNull(capabilityInfo).getNodes();
                        String tmp = "";
                        for(Node node: nodes){
                            tmp += node.getDisplayName() + " ";
                        }
                        if(tmp.equals("")){
                            esmConfigurationPreferences.setConnectionStatusWear("Disconnected");
                        } else {
                            esmConfigurationPreferences.setConnectionStatusWear(tmp);
                        }
                    }

                }
            });


            capabilityClient = Wearable.getCapabilityClient(context);

            listener = new CapabilityClient.OnCapabilityChangedListener() {
                @SneakyThrows
                @Override
                public void onCapabilityChanged(@NonNull CapabilityInfo capabilityInfo) {
                    Log.d(TAG,"onCapabilityChanged is called");
                    Set<Node> nodes = capabilityInfo.getNodes();
                    if(nodes.size() > 0){
                        String tmp = "";
                        for(Node node: nodes){
                            tmp += node.getDisplayName() + " ";
                        }
                        esmConfigurationPreferences.setConnectionStatusWear(tmp);
                    } else {
                        esmConfigurationPreferences.setConnectionStatusWear("Disconnected");
                        if(sessionPreferencesUtil.getSessionRunning()){
                            NotificationManager notificationManager =
                                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                            String CHANNEL_ID = context.getResources().getString(R.string.notiChannelID);
                            NotificationCompat.Builder notification = new NotificationCompat.Builder(context,CHANNEL_ID)
                                    .setContentText("Lost connection to wearable")
                                    .setSmallIcon(R.drawable.ic_survey)
                                    .setOngoing(false)
                                    .setAutoCancel(true);
                            notificationManager.notify("ConnectionLost", 0101010,notification.build());
                        }
                    }
                }
            };


            capabilityClient.addListener(listener, "bluetooth-status-wear");

        }catch (Exception er){
            Log.e(TAG,er.getMessage());
        }
    }



    public void unregisterSessionReceiver(Context context){
        try{
            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(context);
            manager.unregisterReceiver(microSurveyNotificationPublisher);
            manager.unregisterReceiver(notificationPublisher);
            manager.unregisterReceiver(questionnaireNotificationPublisher);
            capabilityClient.removeListener(listener);
            Log.d(TAG,"Unregistered receivers");

        }catch (Exception er){
            Log.e(TAG, er.getMessage());
        }


    }


}
