package de.dipf.edutec.edutex.androidclient.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import de.dipf.edutec.edutex.androidclient.R;

public class ErrorDialog {

    private String TAG = ErrorDialog.class.getSimpleName();
    private Dialog cDialog;
    private Context cContext;


    TextView errorMessage;
    Button cancelButton;


    public ErrorDialog(Context context){
        this.cContext = context;

    }


    public void showDialog(String text){
        cDialog = new Dialog(cContext);

        ColorDrawable dialogColor = new ColorDrawable(Color.WHITE);
        cDialog.getWindow().setBackgroundDrawable(dialogColor);
        cDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cDialog.setContentView(R.layout.dialog_error);
        cDialog.setCancelable(true);
        cDialog.setCanceledOnTouchOutside(true);
        cDialog.show();

        findGUIElements(text);
    }




    private void findGUIElements(String text){

        cancelButton = cDialog.findViewById(R.id.error_dialog_cancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cDialog.dismiss();
            }
        });

        errorMessage = cDialog.findViewById(R.id.error_dialog_msg);
        errorMessage.setText(text);
    }




}
