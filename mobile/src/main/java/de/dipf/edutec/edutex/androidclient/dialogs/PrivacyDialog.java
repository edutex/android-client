package de.dipf.edutec.edutex.androidclient.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.LoginActivity;
import de.dipf.edutec.edutex.androidclient.activities.MainActivity;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;

public class PrivacyDialog {

    private Dialog cDialog;
    private Context cContext;
    private Button accept, close;
    private TextView tvPrivacyText;
    private Activity activity;

    public PrivacyDialog(Context context, Activity activity){
        this.cContext =context;
        this.activity = activity;
    }

    public void showDialog(){

        cDialog = new Dialog(cContext);
        cDialog.setContentView(R.layout.dialog_privacy);
        cDialog.setCancelable(false);
        cDialog.setCanceledOnTouchOutside(false);
        cDialog.show();

        findGUIElements();
    }



    private void findGUIElements(){
       accept = cDialog.findViewById(R.id.dialog_accept_privacy);
       close = cDialog.findViewById(R.id.dialog_close_app);
       tvPrivacyText = cDialog.findViewById(R.id.tv_privacyText);

       String text =
               "Datenschutzrichtlinie\n" +
                       "Educational Technologies (https://edutec.science) hat Edutex als Open-Source-Applikation entwickelt. Dieser Service wird von Educational Technologies kostenlos zur Verfügung gestellt und ist für die Nutzung in der vorliegenden Form gedacht.\n" +
                       "\n" +
                       "Diese Seite dient dazu, Nutzer über unsere Richtlinien bezüglich der Erfassung, Verwendung und Weitergabe von persönlichen Daten zu informieren, falls sich jemand für die Nutzung unseres Dienstes entscheidet.\n" +
                       "\n" +
                       "Wenn Sie sich entscheiden, unseren Service zu nutzen, dann stimmen Sie der Sammlung und Nutzung von Informationen in Bezug auf diese Richtlinie zu. Die von uns gesammelten persönlichen Daten werden für die Bereitstellung und Verbesserung des Dienstes verwendet. Wir werden Ihre Daten nur wie in dieser Datenschutzrichtlinie beschrieben verwenden oder weitergeben.\n" +
                       "\n" +
                       "Abschnitte\n" +
                       "Erfassung und Verwendung von Informationen\n" +
                       "Log-Daten\n" +
                       "Messverfahren - Sensordaten, Nutzungsdaten, Standort und Sonstiges\n" +
                       "Sicherheit\n" +
                       "Datenschutz für Kinder\n" +
                       "Änderungen an dieser Datenschutzrichtlinie\n" +
                       "Kontaktieren Sie uns\n" +
                       "Erfassung und Verwendung von Informationen\n" +
                       "\n" +
                       "Um Ihnen die Nutzung unseres Dienstes zu erleichtern, bitten wir Sie möglicherweise, uns bestimmte personenbezogene Daten zur Verfügung zu stellen. Die von uns angeforderten Informationen werden von uns gespeichert und wie in dieser Datenschutzrichtlinie beschrieben verwendet.\n" +
                       "\n" +
                       "Edutex nutzt Dienste von Drittanbietern, die möglicherweise Informationen sammeln, mit denen Sie identifiziert werden können.\n" +
                       "\n" +
                       "Link zu den Datenschutzrichtlinien von Drittanbietern, die von der App genutzt werden\n" +
                       "\n" +
                       "Google Play Services (https://www.google.com/policies/privacy/)\n" +
                       "Google Analytics for Firebase (https://firebase.google.com/policies/analytics)\n" +
                       "\n" +
                       "Log-Daten\n" +
                       "\n" +
                       "Wir möchten Sie darüber informieren, dass wir bei der Nutzung unseres Dienstes im Falle eines Fehlers in der App Daten und Informationen auf Ihrem Telefon sammeln, die sogenannten Logdaten. Diese Protokolldaten können Informationen wie die Konfiguration Ihres Geräts, den Gerätenamen, die Version des Betriebssystems, die Konfiguration der App bei der Nutzung unseres Dienstes, die Uhrzeit und das Datum Ihrer Nutzung des Dienstes sowie andere Statistiken enthalten.\n" +
                       "\n" +
                       "Messverfahren - Sensordaten, Nutzungsdaten, Standort und Sonstiges\n" +
                       "\n" +
                       "Mit \"Daten\" sind in diesem Abschnitt alle Informationen gemeint, die aus Sensordaten (Beschleunigungsmesser, Gyroskop, Herzfrequenz, Lichtintensität), Nutzungsdaten (App-Statistik, Benachrichtigungen, App-Nutzung) und Standortdaten (von GPS, Wifi und anderen) bestehen, die von Edutex mit der Smartwatch und dem Smartphone erfasst werden.\n" +
                       "\n" +
                       "Auf welche Daten kann Edutex zugreifen?\n" +
                       "Sensor-Daten\n" +
                       "Edutex kann auf die internen Sensoren und deren Ausgaben wie Beschleunigungsmesser, Gyroskop, Herzfrequenz und andere zugreifen, die vom Gerätehersteller in Ihrem Smartphone und der Smartwatch installiert sind.\n" +
                       "Nutzungsdaten\n" +
                       "Um ihre Nutzung des Smartphones und der Smartwatch aufzeichnen zu können sammelt Edutex Informationen über eingehende Benachrichtigungen, die allgemeine App-Nutzungsstatistik und ihre Navigation auf der Smartwatch und dem Smartphone. Dem Zugriff auf diese Daten müssen Sie nochmals explizit in der App zustimmen.\n" +
                       "Standortdaten\n" +
                       "Um Standortdaten über GPS, Wifi und Bluetooth auf Ihrem Smartphone und der Smartwatch zu speichern, müssen die Standortdienste im Hintergrund aktiviert sein. Der Zugriff auf diese Daten erfolgt nur mit Zustimmung des Nutzers.\n" +
                       "Liste der Funktionen mit Verwendung des Standorts:\n" +
                       "\n" +
                       "Der Geohash der Koordinaten wird gespeichert\n" +
                       "Wann wird auf die Daten zugegriffen?\n" +
                       "Der Zugriff auf die Daten erfolgt nur wenn sich der Nutzer bei einer spezifischen Studie angemeldet hat. Die Messung kann in den beiden Modi Anwendergesteuert und Lernumgebungsgesteuert gestartet werden. Der Modus hängt von der Teilnahme an einer Studie ab. Der Nutzer entscheidet sich selbstständig für die Teilnahme indem er sich aktiv auf dem Gerät bei der jeweiligen Studie anmeldet. Im anwendergesteuererten Modus kann die Messung nur vom Benutzer gestartet werden, indem er eine Lernsession auf der Smartwatch oder dem Smartphone startet. Edutex kann in diesem Modus die Messung nicht von sich aus starten. Im lernumgebungsgesteuerten Modus wird die Messung vom Nutzer durch aktive Nutzung einer spezifischen Lernumgebung gestarted. Die Messung wird in beiden Modi durch eine Benachrichtigung oder durch die Benutzeroberfläche von Edutex angezeigt, die für die Anzeige der Messung vorgesehen ist.\n" +
                       "Wo werden die Daten gespeichert?\n" +
                       "Die Daten werden auf einem Server der Universität Frankfurt gespeichert.\n" +
                       "Werden die Daten weitergegeben oder an Dritte verkauft?\n" +
                       "Nein. Keine der Daten werden an Dritte weitergegeben. Die Daten werden nur für die jeweiligen spezifischen Studienzwecke gespeichert.\n" +
                       "Können Kosten bei der Datenübertragung entstehen?\n" +
                       "Die Services von Edutex sind kostenfrei. Es können jedoch Kosten auf Nutzerseite durch die Datenübertragung von den Endgeräten zu den Edutex-Servern entstehen.\n" +
                       "Sicherheit\n" +
                       "Wir wissen Ihr Vertrauen in die Übermittlung Ihrer persönlichen Daten zu schätzen und bemühen uns daher, sie im Rahme unserer Möglichkeiten zu schützen. Denken Sie jedoch daran, dass keine Methode der Übertragung über das Internet oder der elektronischen Speicherung zu 100 % sicher und zuverlässig ist, und wir können keine absolute Sicherheit garantieren.\n" +
                       "Datenschutz für Kinder\n" +
                       "\n" +
                       "Diese Dienste richten sich nicht an Personen unter 13 Jahren. Wir sammeln nicht wissentlich persönlich identifizierbare Informationen von Kindern unter 13 Jahren. Sollten wir feststellen, dass ein Kind unter 13 Jahren uns persönliche Daten zur Verfügung gestellt hat, löschen wir diese sofort von unseren Servern. Wenn Sie ein Elternteil oder Erziehungsberechtigter sind und wissen, dass Ihr Kind uns personenbezogene Daten zur Verfügung gestellt hat, setzen Sie sich bitte mit uns in Verbindung, damit wir die erforderlichen Maßnahmen ergreifen können.\n" +
                       "Änderungen an dieser Datenschutzrichtlinie\n" +
                       "Wir können unsere Datenschutzrichtlinie von Zeit zu Zeit aktualisieren. Wir empfehlen Ihnen daher, diese Seite regelmäßig auf Änderungen zu überprüfen. Wir werden Sie über alle Änderungen informieren, indem wir die neue Datenschutzrichtlinie auf dieser Seite veröffentlichen.\n" +
                       "\n" +
                       "Diese Richtlinie gilt ab dem 2022-01-01\n" +
                       "Kontaktieren Sie uns\n" +
                       "Wenn Sie Fragen oder Anregungen zu unserer Datenschutzrichtlinie haben, zögern Sie nicht, uns unter edutex@edutec.science oder ciordas@dipf.de zu kontaktieren.\n" +
                       "\n" +
                       "Diese Datenschutzseite wurde mit Hilfe von privacypolicytemplate.net (https://privacypolicytemplate.net) erstellt und von App Privacy Policy Generator (https://app-privacy-policy-generator.nisrulz.com/) modifiziert bzw. generiert.\n"
                        +
                        "Privacy Policy\n" +
                       "Educational Technologies (https://edutec.science) has developed Edutex as an Open Source application. This service is provided free of charge by Educational Technologies and is intended for use as is.\n" +
                       "\n" +
                       "This page is intended to inform users of our policies regarding the collection, use, and disclosure of personal information should anyone choose to use our service.\n" +
                       "\n" +
                       "If you choose to use our service, then you consent to the collection and use of information related to this policy. The personal information we collect will be used to provide and improve the Service. We will only use or share your information as described in this Privacy Policy.\n" +
                       "Table of Contents\n" +
                       "Collection and Use of Information\n" +
                       "Log Data\n" +
                       "Measurement Process - Sensor Data, Usage Data, Location, and Other\n" +
                       "Security\n" +
                       "Privacy for Children\n" +
                       "Changes to this Privacy Policy\n" +
                       "Contact Us\n" +
                       "\n" +
                       "Collection and Use of Information\n" +
                       "\n" +
                       "In order to facilitate your use of our service, we may ask you to provide us with certain personal information. The information we request will be stored by us and used as described in this Privacy Policy.\n" +
                       "\n" +
                       "Edutex uses third-party services that may collect information that can identify you.\n" +
                       "\n" +
                       "Link to the privacy policies of third parties used by the App.\n" +
                       "Google Play Services (https://www.google.com/policies/privacy/)\n" +
                       "Google Analytics for Firebase (https://firebase.google.com/policies/analytics)\n" +
                       "Log Data\n" +
                       "We would like to inform you that when you use our service, in case of an error in the app, we collect data and information on your phone, called log data. This log data may include information such as the configuration of your device, the device name, the version of the operating system, the configuration of the App when you use our Service, the time and date of your use of the Service, and other statistics.\n" +
                       "\n" +
                       "Measurement Process - Sensor Data, Usage Data, Location, and Other\n" +
                       "\"Data\" in this section means all information consisting of sensor data (accelerometer, gyroscope, heart rate, light intensity), usage data (App statistics, notifications, App usage), and location data (from GPS, Wifi and others) collected by Edutex using the Smartwatch and the Smartphone.\n" +
                       "What data can Edutex access?\n" +
                       "Sensor data\n" +
                       "Edutex can access the internal sensors and their outputs such as accelerometer, gyroscope, heart rate, and others installed by the device manufacturer in your smartphone and smartwatch.\n" +
                       "Usage Data\n" +
                       "To record their usage of the smartphone and smartwatch, Edutex collects information about incoming notifications, general app usage statistics, and their navigation on the smartwatch and smartphone. You must again explicitly agree to access this data in the app.\n" +
                       "Location data\n" +
                       "To store location data via GPS, Wifi, and Bluetooth on your smartphone and smartwatch, the location services must be activated in the background. Access to this data is only with the user's consent.\n" +
                       "List of functions with location usage:\n" +
                       "The geohash of the coordinates is stored\n" +
                       "When are the data accessed?\n" +
                       "The data is only accessed when the user has logged in to a specific study. The measurement can be started in two modes: user-controlled and learning environment-controlled. The mode depends on the participation in a study. The user independently decides to participate by actively logging into the specific study on the device. In user-controlled mode, the measurement can only be started by the user by starting a learning session on the smartwatch or smartphone. Edutex cannot start the measurement by itself in this mode. In the learning-environment-driven mode, the measurement is started by the user by actively using a specific learning environment. In both modes, the measurement is indicated by a notification or by the Edutex user interface, which is designed to display the measurement.\n" +
                       "Where is the data stored?\n" +
                       "The data is stored on a server at the University of Frankfurt.\n" +
                       "Will the data be shared or sold to third parties?\n" +
                       "No. None of the data will be shared with third parties. The data will only be stored for specific study purposes.\n" +
                       "Can there be costs for data transfer?\n" +
                       "The Edutex services are free of charge. However, costs may arise on the user side due to the data transfer from the end devices to the Edutex servers.\n" +
                       "Security\n" +
                       "We appreciate your trust in the transmission of your personal data and therefore make every effort to protect it within the scope of our possibilities. However, please remember that no method of transmission over the Internet or electronic storage is 100% secure or reliable, and we cannot guarantee absolute security.\n" +
                       "Privacy for Children\n" +
                       "These services are not directed to anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. If we discover that a child under the age of 13 has provided us with personal information, we will immediately delete it from our servers. If you are a parent or guardian and know that your child has provided us with personally identifiable information, please contact us so that we can take appropriate action.\n" +
                       "Changes to this Privacy Policy\n" +
                       "We may update our Privacy Policy from time to time. Therefore, we encourage you to check this page periodically for changes. We will notify you of any changes by posting the new privacy policy on this page.\n" +
                       "\n" +
                       "This policy is effective as of 2022-01-01\n" +
                       "Contact Us\n" +
                       "If you have any questions or comments about our privacy policy, please do not hesitate to contact us at edutex@edutec.science or ciordas@dipf.de.\n" +
                       "\n" +
                       "This privacy page was created using privacypolicytemplate.net (https://privacypolicytemplate.net) and modified or generated by App Privacy Policy Generator (https://app-privacy-policy-generator.nisrulz.com/).\n" +
                       "\n" + "\n";

       tvPrivacyText.setText(text);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(cContext);
                esmConfigurationPreferences.setAcceptedPrivacy(true);
                cDialog.dismiss();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cDialog.dismiss();
                activity.finish();
            }
        });


        if(activity.getClass() == MainActivity.class){
            cDialog.setCanceledOnTouchOutside(true);
            accept.setVisibility(View.GONE);
            close.setVisibility(View.GONE);
        }



    }


    public void hideProgress(){
        if(cDialog != null){
            cDialog.dismiss();
            cDialog = null;
        }
    }

}

