package de.dipf.edutec.edutex.androidclient.dialogs;

import static com.mikepenz.iconics.Iconics.getApplicationContext;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.preference.Preference;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.MainActivity;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.util.ServiceRunningUtil;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;

public class SessionDialog {

    private Dialog cDialog;
    private TextView survey_requested, services_reseted,preferences_reseted, notification_channel;
    Activity activity;
    Context context;
    Handler handler;
    Boolean isPre;
    SharedPreferences sessionPreferences;
    SharedPreferences.OnSharedPreferenceChangeListener sessionListener;

    Runnable services_stopped = new Runnable() {
        @Override
        public void run() {
            if(!ServiceRunningUtil.areServiceClosed(context)){
                services_reseted.setTextColor(ContextCompat.getColor(context, R.color.orange));
                handler.postDelayed(this,1000);
            }else{

                services_reseted.setTextColor(ContextCompat.getColor(context, R.color.green_ok));

                SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(context);
                sessionPreferencesUtil.cleanUpSessionPreferences();
                preferences_reseted.setTextColor(ContextCompat.getColor(context, R.color.green_ok));

                NotificationManager notificationManager =
                        (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancelAll();
                notification_channel.setTextColor(ContextCompat.getColor(context, R.color.green_ok));

                if(isPre){
                    try{
                        MessagesSingleton.getInstance().RequestPreSurveys();
                        survey_requested.setTextColor(ContextCompat.getColor(context, R.color.green_ok));
                    }catch (Exception er){
                        er.printStackTrace();
                    }
                }else{
                    try{
                        MessagesSingleton.getInstance().RequestPostSurveys();
                    }catch (Exception er){
                        er.printStackTrace();
                    }

                }



            }
        }
    };


    public SessionDialog(Context context, Boolean isPre){
        this.context = context;
        handler = new Handler();
        this.isPre = isPre;
    }

    public void showProgress(){

        cDialog = new Dialog(context);
        cDialog.setContentView(R.layout.dialog_session);
        cDialog.setCancelable(false);
        cDialog.setCanceledOnTouchOutside(false);
        cDialog.show();
        findGUIElements();

        if(!isPre){
            survey_requested.setText("Post-Survey requested");
        }

        sessionPreferences =  context.getSharedPreferences(SessionPreferencesUtil.getPrefsName(),Context.MODE_PRIVATE);
        sessionListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                try{
                    switch(key){
                        case SessionPreferencesUtil.KEY_SESSION_RUNNING:
                            SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(context);
                            if(sessionPreferencesUtil.getSessionRunning() && isPre){
                                hideProgress();
                            }

                            if(!sessionPreferencesUtil.getSessionRunning() && !isPre){
                                hideProgress();
                            }

                            break;

                        default:
                            break;
                    }
                }catch (Exception er){
                    er.printStackTrace();
                }
            }
        };
        sessionPreferences.registerOnSharedPreferenceChangeListener(sessionListener);
        handler.postDelayed(services_stopped,1000);
    }

    public void findGUIElements(){
        survey_requested = cDialog.findViewById(R.id.survey_requested);
        services_reseted = cDialog.findViewById(R.id.services_reseted);
        preferences_reseted = cDialog.findViewById(R.id.preferences_reseted);
        notification_channel = cDialog.findViewById(R.id.notification_channel);
    }

    public void hideProgress(){
        if(cDialog != null){
            handler.removeCallbacks(services_stopped);
            sessionPreferences.unregisterOnSharedPreferenceChangeListener(sessionListener);
            cDialog.dismiss();
            cDialog = null;
        }
    }
}