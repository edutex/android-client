package de.dipf.edutec.edutex.androidclient.fragments;

import static com.mikepenz.iconics.Iconics.getApplicationContext;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.dialogs.SessionDialog;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import lombok.SneakyThrows;

public class HomeFragment extends Fragment {

    private static String TAG = "HomeFragment";

    // GUI Elements
    TextView  tv_conn_django, tv_conn_phone, tv_conn_mqtt, tv_home_sessionRunning,
            tv_recieved_msg_esm, tv_home_sessionID, tv_received_responses;
    Button bt_home_exit, bt_start_session, bt_stop_session;
    LinearLayout layout_bluetooth_conn, remote_information;

    SharedPreferences sessionPreferences, esmPreferences;
    SharedPreferences.OnSharedPreferenceChangeListener sessionListener, esmListener;


    LinearLayout linearLayout_sessionTimer;
    TextView sp_timer;
    long start_value;
    Handler handler;

    Runnable runnable_monitor_time = new Runnable() {
        @Override

        public void run()
        {
            if(start_value == -1L){
                Log.d(TAG, "Start-Value is -1. Returning");
                return;
            }

            long current = (System.currentTimeMillis() / 1000) - start_value;
            long hours = current / 3600;
            long minutes = (current % 3600) / 60;
            long secs = current % 60;

            String time
                    = String
                    .format(Locale.getDefault(),
                            "%d:%02d:%02d", hours,
                            minutes, secs);

            if(sp_timer == null){
                sp_timer = getActivity().findViewById(R.id.home_session_timer);
            }
            sp_timer.setText(time);
            handler.postDelayed(this, 1000);
        }
    };

    SessionDialog sessionDialog;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.hasExtra("close_dialog")){
                try{
                    sessionDialog.hideProgress();
                }catch (Exception er){}
            }
        }
    };

    public HomeFragment() {}

    @Override
    public void onStart(){
        super.onStart();
        findGUIElements();

        Context context = getActivity().getApplicationContext();

        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(context);
        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);
        handler = new Handler();
        sessionPreferences = context.getSharedPreferences(SessionPreferencesUtil.getPrefsName(),Context.MODE_PRIVATE);
        esmPreferences = context.getSharedPreferences(ESMConfigurationPreferences.getPrefsName(),Context.MODE_PRIVATE);

        sessionListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                try{

                    switch(key){
                        case SessionPreferencesUtil.KEY_SESSION_RUNNING:
                            tv_home_sessionRunning.setText(String.valueOf(sessionPreferencesUtil.getSessionRunning()));
                            if(esmConfigurationPreferences.getPrefDevice() == 1){

                                updateGUIElements();

                                if(sessionPreferencesUtil.getSessionRunning()){
                                    //bt_stop_session.setVisibility(View.VISIBLE);
                                    //bt_start_session.setVisibility(View.GONE);
                                    linearLayout_sessionTimer.setVisibility(View.VISIBLE);
                                    runTimer();

                                }else{

                                    //bt_start_session.setVisibility(View.GONE);
                                    //bt_stop_session.setVisibility(View.VISIBLE);
                                    handler.removeCallbacks(runnable_monitor_time);
                                    start_value = -1L;
                                    sessionPreferencesUtil.setSPSessionTimer(-1L);
                                    linearLayout_sessionTimer.setVisibility(View.GONE);

                                }

                            }

                            if(esmConfigurationPreferences.getStudyStartMechanics() == 0){
                                bt_stop_session.setVisibility(View.GONE);
                                bt_start_session.setVisibility(View.GONE);
                                remote_information.setVisibility(View.VISIBLE);

                            }
                            break;

                        case SessionPreferencesUtil.KEY_SESSION_ID:
                            tv_home_sessionID.setText(sessionPreferencesUtil.getSessionId());
                            break;

                        default:
                            break;
                    }

                }catch (Exception er){

                }
            }
        };

        esmListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                try{

                    switch(key){
                        case ESMConfigurationPreferences.KEY_CONNECTION_STATUS:
                            String status = esmConfigurationPreferences.getConnectionStatus();
                            tv_conn_django.setText(status);
                            if(status.equals("Connected")){
                                tv_conn_django.setTextColor(ContextCompat.getColor(context,R.color.green_ok));
                            } else if(status.equals("Reconnecting")) {
                                tv_conn_django.setTextColor(ContextCompat.getColor(context,R.color.orange));
                            } else{
                                tv_conn_django.setTextColor(ContextCompat.getColor(context,R.color.red_ok));
                            }
                            break;
                        case ESMConfigurationPreferences.KEY_CONNECTION_STATUS_WEAR:
                            String status_wear = esmConfigurationPreferences.getConnectionStatusWear();
                            tv_conn_phone.setText(status_wear);
                            if(status_wear.equals("Disconnected")){
                                tv_conn_phone.setTextColor(ContextCompat.getColor(context,R.color.red_ok));
                            } else if(status_wear.equals("Reconnecting")) {
                                tv_conn_phone.setTextColor(ContextCompat.getColor(context,R.color.orange));
                            } else{
                                tv_conn_phone.setTextColor(ContextCompat.getColor(context,R.color.green_ok));
                            }
                        case ESMConfigurationPreferences.KEY_PREF_DEVICE:
                            updateGUIElements();
                            /*
                            if(esmConfigurationPreferences.getPrefDevice() == 0){
                                bt_start_session.setVisibility(View.GONE);
                            }else {
                                if(esmConfigurationPreferences.getStudyStartMechanics() == 1){
                                    bt_start_session.setVisibility(View.VISIBLE);
                                }
                            }*/
                            break;
                        default:
                            break;
                    }



                }catch (Exception er){

                }
            }
        };

        sessionPreferences.registerOnSharedPreferenceChangeListener(sessionListener);
        esmPreferences.registerOnSharedPreferenceChangeListener(esmListener);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        IntentFilter filter = new IntentFilter("close_on_error");
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver,filter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    public void findGUIElements(){
        tv_conn_django = getActivity().findViewById(R.id.tv_conn_django);
        tv_conn_phone = getActivity().findViewById(R.id.tv_conn_bluethooth);
        tv_home_sessionRunning = getActivity().findViewById(R.id.tv_home_sessionRunning);
        tv_conn_mqtt = getActivity().findViewById(R.id.tv_conn_mqtt);
        tv_home_sessionID =  getActivity().findViewById(R.id.tv_home_sessionID);
        tv_recieved_msg_esm = getActivity().findViewById(R.id.tv_recieved_msg_esm);
        tv_received_responses = getActivity().findViewById(R.id.tv_received_responses);
        bt_home_exit = getActivity().findViewById(R.id.bt_home_exit);
        layout_bluetooth_conn = getActivity().findViewById(R.id.ll_conn_bluetooth);
        bt_start_session = getActivity().findViewById(R.id.bt_start_session);
        bt_stop_session = getActivity().findViewById(R.id.bt_stop_session);
        linearLayout_sessionTimer = getActivity().findViewById(R.id.ll_sessionTimer);
        sp_timer = getActivity().findViewById(R.id.home_session_timer);
        remote_information = getActivity().findViewById(R.id.remote_information);
        bt_start_session.setOnClickListener(new View.OnClickListener() {
            @SneakyThrows
            @Override
            public void onClick(View view) {

                sessionDialog = new SessionDialog(getActivity(), true);
                sessionDialog.showProgress();

            }
        });

        bt_stop_session.setOnClickListener(new View.OnClickListener() {
            @SneakyThrows
            @Override
            public void onClick(View view) {
                bt_stop_session.setVisibility(View.GONE);
                SessionDialog sessionDialog = new SessionDialog(getActivity(), false);
                sessionDialog.showProgress();


            }
        });



    }

    @Override
    public void onPause() {
        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
        sessionPreferencesUtil.setSPSessionTimer(start_value);
        super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        Context context = getActivity().getApplicationContext();
        if(context == null){return;}

        sessionListener.onSharedPreferenceChanged(sessionPreferences,SessionPreferencesUtil.KEY_SESSION_RUNNING);
        sessionListener.onSharedPreferenceChanged(sessionPreferences,SessionPreferencesUtil.KEY_SESSION_ID);
        esmListener.onSharedPreferenceChanged(esmPreferences, ESMConfigurationPreferences.KEY_CONNECTION_STATUS);
        esmListener.onSharedPreferenceChanged(esmPreferences, ESMConfigurationPreferences.KEY_CONNECTION_STATUS_WEAR);
        esmListener.onSharedPreferenceChanged(esmPreferences,  ESMConfigurationPreferences.KEY_PREF_DEVICE);
        esmListener.onSharedPreferenceChanged(esmPreferences,  ESMConfigurationPreferences.KEY_STUDY_MECHANICS);

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);
        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(context);

        if(esmConfigurationPreferences.getPrefDevice() == 1 && sessionPreferencesUtil.getSessionRunning()){
            runTimer();
        }

        updateGUIElements();

        /*
        if(esmConfigurationPreferences.getPrefDevice() == 0){

            layout_bluetooth_conn.setVisibility(View.VISIBLE);
            bt_start_session.setVisibility(View.GONE);
            bt_stop_session.setVisibility(View.GONE);

        } else {

            layout_bluetooth_conn.setVisibility(View.GONE);
            if(sessionPreferencesUtil.getSessionRunning()){

                bt_start_session.setVisibility(View.GONE);
                bt_stop_session.setVisibility(View.VISIBLE);
                runTimer();

            } else {

                bt_stop_session.setVisibility(View.GONE);
                bt_start_session.setVisibility(View.VISIBLE);
            }
        }

        if(esmConfigurationPreferences.getStudyStartMechanics() == 0){
            bt_start_session.setVisibility(View.GONE);
            bt_stop_session.setVisibility(View.GONE);
            remote_information.setVisibility(View.VISIBLE);
        }*/

    }


    @Override
    public void onDestroy(){
        try{
            sessionPreferences.unregisterOnSharedPreferenceChangeListener(sessionListener);
        } catch (Exception e){}

        try{
            esmPreferences.unregisterOnSharedPreferenceChangeListener(esmListener);
        }catch (Exception e){}

        try{
            handler.removeCallbacks(runnable_monitor_time);
        }catch (Exception e){}

        try{
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
        }catch (Exception er){}

        super.onDestroy();
    }

    private void runTimer(){
        Log.d(TAG, "runTimer called");

        SessionPreferencesUtil sessionPreferences = new SessionPreferencesUtil(getApplicationContext());
        long starting_point = sessionPreferences.getSPSessionTimer();
        Log.d(TAG, "SessionPreferenceValueSW: " + String.valueOf(starting_point));

        if (starting_point == -1L){
            Log.d(TAG, "Starting Point was -1.");
            starting_point = System.currentTimeMillis() / 1000;
        }

        start_value = starting_point;
        Log.d(TAG, "Current value: " + String.valueOf(start_value));
        handler.postDelayed(runnable_monitor_time,500);
    }


    private void updateGUIElements(){

        Context context = getActivity().getApplicationContext();
        if(context == null){return;}

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);
        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(context);

        int mechanic = esmConfigurationPreferences.getStudyStartMechanics();
        int prefDevice = esmConfigurationPreferences.getPrefDevice();
        Boolean running = sessionPreferencesUtil.getSessionRunning();



        if(mechanic == 0){ // Remote

            remote_information.setVisibility(View.VISIBLE);
            bt_start_session.setVisibility(View.GONE);
            bt_stop_session.setVisibility(View.GONE);

            if(prefDevice == 0){ // Using Smartwatch and Smartphone

                layout_bluetooth_conn.setVisibility(View.VISIBLE);

            }else{ // Using only Smartphone

                layout_bluetooth_conn.setVisibility(View.GONE);

            }

        }else{ // Not Remote

            remote_information.setVisibility(View.GONE);

            if(prefDevice == 0){ // Using Smartwatch and Smartphone

                layout_bluetooth_conn.setVisibility(View.VISIBLE);
                bt_start_session.setVisibility(View.GONE);
                bt_stop_session.setVisibility(View.GONE);

            }else{ // Using only Smartphone

                layout_bluetooth_conn.setVisibility(View.GONE);

                if(running){

                    bt_start_session.setVisibility(View.GONE);
                    bt_stop_session.setVisibility(View.VISIBLE);

                }else{

                    bt_start_session.setVisibility(View.VISIBLE);
                    bt_stop_session.setVisibility(View.GONE);

                }

            }

        }







    }



}
