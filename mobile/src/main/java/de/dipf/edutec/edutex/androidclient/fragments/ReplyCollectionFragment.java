package de.dipf.edutec.edutex.androidclient.fragments;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import lombok.SneakyThrows;

public class ReplyCollectionFragment extends Fragment implements ReplyCollectionAdapter.ItemClickListener {

    //GUI
    ExtendedFloatingActionButton floatingActionButton;

    public MessagesSingleton messagesSingleton;
    public ReplyCollectionAdapter adapter;


    public static ReplyCollectionFragment newInstance() {
        ReplyCollectionFragment fragment = new ReplyCollectionFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        messagesSingleton = MessagesSingleton.getInstance();

    }

    @SneakyThrows
    @Override
    public void onStart() {
        super.onStart();
        floatingActionButton = getView().findViewById(R.id.bt_replyColl_refresh);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @SneakyThrows
            @Override
            public void onClick(View v) {
                try {
                    refreshLayout();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        messagesSingleton.registerListener(new MessagesSingleton.Listener() {
            @SneakyThrows
            @Override
            public void onStateChange(JSONObject uuid) throws JSONException {
                refreshLayout();
            }

            @Override
            public void onActionTriggered(String action) {

            }
        });
        try {
            refreshLayout();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reply_collection, container, false);
    }


    public void refreshLayout() throws JSONException {
        List<String> questionList = new ArrayList<String>();
        List<String> answerList = new ArrayList<String>();

        for(JSONObject msg : messagesSingleton.messagesReceived){
            questionList.add(msg.getString("question"));
            answerList.add(msg.getString("userAnswer"));
        }

        RecyclerView recyclerView = getView().findViewById(R.id.rv_reply_collection);
        adapter = new ReplyCollectionAdapter(getContext(), questionList, answerList);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
