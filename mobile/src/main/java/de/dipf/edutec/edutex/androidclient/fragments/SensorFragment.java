package de.dipf.edutec.edutex.androidclient.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.dipf.edutec.edutex.androidclient.R;


public class SensorFragment extends Fragment {


    public SensorFragment() {}

    @Override
    public void onStart(){
        super.onStart();
    }






    // Included factory methods
    public static SensorFragment newInstance(String param1, String param2) {
        SensorFragment fragment = new SensorFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sensor, container, false);
    }
}
