package de.dipf.edutec.edutex.androidclient.messageservice;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;

import android.os.Handler;
import android.os.IBinder;

import android.os.SystemClock;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.QuestionnaireActivity;
import de.dipf.edutec.edutex.androidclient.auth.AuthPreferences;
import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import de.dipf.edutec.edutex.androidclient.processtracking.ProcessTrackingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.AmbientNoiseRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.BehavioralRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.BluetoothRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.GPSRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.PhysicalRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.PhysiologicalRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.VirtualSensorService;
import de.dipf.edutec.edutex.androidclient.sensorservice.WifiRecordingService;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import de.dipf.edutec.edutex.androidclient.util.ServiceRunningUtil;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import lombok.SneakyThrows;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import de.dipf.edutec.edutex.androidclient.sharedUtils.MessageLayerStatics;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class ESMService extends Service {

    private static String TAG = ESMService.class.getSimpleName();
    private static final int RECONNECT_INTERVAL = 5000;

    WebSocket websocket;
    ForegroundNotificationCreator fgNotificationManager;
    Handler handler;
    Boolean isWorking;

    // Communication with ESM Backend and Smartwatch
    MessagesSingleton messagesSingleton;
    SendMessageLayer messageService;


    private Runnable repeatingConnect = new Runnable() {
        @Override
        public void run() {
            if(! isWorking){
                Log.d(TAG, "Repeating Connect is called.");
                if(getApplicationContext() != null){
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.name_sharedpreference_status),Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(getString(R.string.key_esm_websocket_status),"Reconnecting");
                    editor.apply();
                    Log.d(TAG, "Mainactivity is not null. Could change Status Websocket to Reconnecting");
                    esm_authentication();
                    Log.d(TAG, "Executed ESM Authentication");
                    isWorking = true;
                }
            }
        }
    };


    @Override
    public void onCreate() {
        super.onCreate();

        this.fgNotificationManager =
                ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();

        this.handler = new Handler();
        isWorking = false;


        Log.d(TAG, "ESMService is created");

    }

    @SneakyThrows
    @Override
    public void onDestroy(){
        handler.removeCallbacks(repeatingConnect);

        if(websocket != null){
            websocket.close(1000, "Goodbye");
            websocket = null;
        }

        messagesSingleton.unregisterListener();

        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
        sessionPreferencesUtil.cleanUpSessionPreferences();

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        esmConfigurationPreferences.setConntectionStatus("Disconnected");

        send_not_ready_update_to_sw();

        isWorking = true;
        stopForeground(true);
        super.onDestroy();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startid){

        if(getApplicationContext() != null){
            Log.d(TAG, "Service Starting.");


            startForeground(
                    fgNotificationManager.getId(),
                    fgNotificationManager.getNotification());

            messageService = new SendMessageLayer(this);
            messagesSingleton = MessagesSingleton.getInstance();
            messagesSingleton.registerListener(new MessagesSingleton.Listener() {

                @SneakyThrows
                @Override
                public void onStateChange(JSONObject message) {

                    SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
                    sessionPreferencesUtil.incrementReceivedRsp();

                    if(message.has("questionnaire_type")){

                        if(message.getInt("questionnaire_type") == 2){

                            message.put("android_smartphone_timestamp_start", RequestResponseUtils.getTimeStamps(Instant.now(),SystemClock.elapsedRealtimeNanos()));
                        }

                        if(message.getInt("questionnaire_type") == 3){
                            message.put("android_smartphone_timestamp_end", RequestResponseUtils.getTimeStamps(Instant.now(),SystemClock.elapsedRealtimeNanos()));
                            sessionPreferencesUtil.setSessionRunning(false);
                            sessionPreferencesUtil.setSessionTerminated(true);
                            MessagesSingleton.getInstance().cleanData();
                            ServiceRunningUtil.terminate_services(getApplicationContext());
                        }
                    }


                    //Packing for esm backend
                    JSONObject msg_for_server = new JSONObject();
                    msg_for_server.put("client_response",message);

                    if(websocket != null){
                        websocket.send(msg_for_server.toString());
                    }





                }

                @Override
                public void onActionTriggered(String action) throws JSONException {

                    Log.d(TAG, "Action Triggered: "+ action);
                    esm_send_request(action);

                }
            });

            esm_authentication();
        }

        return START_NOT_STICKY;
    }

    private void esm_authentication(){

        // Authentication was done via Login //
        AuthPreferences authPreferences = new AuthPreferences(getApplicationContext());
        String token = authPreferences.getAuthToken();

        try{
            esm_ws_connect(token);
        }catch (Exception e){
            Log.d(TAG, e.getMessage());
        }
    }

    private void esm_ws_connect(String token) throws InterruptedException {

        Context context = getApplicationContext();

        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(context);
        String participantID = sessionPreferencesUtil.getParticipantId();
        Log.d(TAG,"Used participant identifier: " + participantID);

        if(participantID == ""){
            handler.postDelayed(repeatingConnect, RECONNECT_INTERVAL);
            return;
        }

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);
        String url = esmConfigurationPreferences.get_configured_url_websocket();


        Log.i(TAG, "Websocket connecting to: " + url );

        Request request = new Request.Builder()
                .addHeader("authorization", "Token " + token)
                .addHeader("participantIdentifier", participantID)
                .url(url)
                .build();

        final OkHttpClient client_websocket = new OkHttpClient();
        client_websocket.retryOnConnectionFailure();


        websocket = client_websocket.newWebSocket(request, new WebSocketListener() {
            @Override
            public void onClosed(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
                super.onClosed(webSocket, code, reason);
                Log.d(TAG, "Websocket onClose is called");
                isWorking = false;
                client_websocket.connectionPool().evictAll();
            }

            @Override
            public void onClosing(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
                super.onClosing(webSocket, code, reason);
                Log.d(TAG, "Websocket onClosing is called.");

                update_ws_status("Disconnected");
                isWorking = false;

            }

            @Override
            public void onFailure(@NotNull WebSocket webSocket, @NotNull Throwable t, @Nullable Response response) {
                super.onFailure(webSocket, t, response);
                update_ws_status("onFailure");

                isWorking = false;
                handler.postDelayed(repeatingConnect,RECONNECT_INTERVAL);
                Log.d(TAG, "Websocket on Failure. Trying to Reconnect in " + String.valueOf(RECONNECT_INTERVAL));
                send_not_ready_update_to_sw();
                client_websocket.connectionPool().evictAll();

            }

            @Override
            public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
                super.onMessage(webSocket, text);

                try {
                    JSONObject j_msg = new JSONObject(text);
                    if(j_msg.has("error")){
                        sendErrorMessage(j_msg.getString("error"));
                        sessionPreferencesUtil.setError(j_msg.getString("error"));

                        Intent intent = new Intent();
                        intent.setAction("close_on_error");
                        intent.putExtra("close_dialog",true);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                        if(esmConfigurationPreferences.getPrefDevice() == 0){
                            messageService.sendMessage(MessageLayerStatics.TOSMARTWATCH_ERROR_NOTIFICATION, j_msg);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if(esmConfigurationPreferences.getPrefDevice() == 0){
                    esm_digest_request_smartwatch(text);
                } else {
                    esm_digest_request_handheld(text);
                }

            }
            @Override
            public void onOpen(@NotNull WebSocket webSocket, @NotNull Response response) {
                Log.i(TAG,"Websocket onOpen Called");
                super.onOpen(webSocket, response);
                esm_request_session_state();
                update_ws_status("Connected");
                update_esm_session(false);
            }

        });

    }

    private void esm_send_request(String action) throws JSONException {
        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());

        JSONObject jsonObject = new JSONObject(action);
        Boolean session_running = sessionPreferencesUtil.getSessionRunning();
        Log.d(TAG, "Send Request: " + action + " Session Running: " + String.valueOf(session_running));

        try{

            if(jsonObject.has("action_recover_last_session")){

                sessionPreferencesUtil.setSessionRecoveryNecessary(false);
                if(websocket != null){
                    websocket.send(jsonObject.toString());
                }
            }

            if(jsonObject.has("action_pre_session_surveys")){
                ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
                jsonObject.put("primary_device",esmConfigurationPreferences.getPrefDevice());

                if(websocket != null){
                    websocket.send(jsonObject.toString());
                }
                sessionPreferencesUtil.setSessionId("-");
                return;
            }
            if(jsonObject.has("action_post_session") || jsonObject.has("action_force_stop")){


                if(websocket != null){
                    Log.d(TAG,"Sended stop session");
                    websocket.send(jsonObject.toString());
                }
                ServiceRunningUtil.terminate_services(getApplicationContext());

                // Cleaning up Session Preference Data
                sessionPreferencesUtil.cleanUpSessionPreferences();
                return;
            }
            if(jsonObject.has("action_start_session")){

                ServiceRunningUtil.terminate_services(getApplicationContext());




                if(websocket != null){
                    Log.w(TAG,"Sended Start Service");
                    websocket.send(jsonObject.toString());
                }
                sessionPreferencesUtil.setSessionRequestedInter(true);
                return;
            }

            if(jsonObject.has("action_remote_session_start")){
                ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
                jsonObject.put("primary_device",esmConfigurationPreferences.getPrefDevice());

                if(websocket != null){
                    Log.w(TAG,"Sended remote start service");
                    websocket.send(jsonObject.toString());
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @SneakyThrows
    private void esm_request_session_state(){
        if(websocket !=  null){
            JSONObject obj = new JSONObject("{'action_recover_last_session_if_available':'1'}");
            websocket.send(obj.toString());
        }
    }

    @SneakyThrows
    private void esm_digest_session(JSONObject object){
        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());

        // Todo Clean Up for new Session Database
        Boolean wasNotSessionRunning = !sessionPreferencesUtil.getSessionRunning();

        if(object.has("isRecovery") || !sessionPreferencesUtil.getSessionRunning()){
            sessionPreferencesUtil.setSessionObject(object);
            Log.d(TAG, "Published new esm session to handheld");

            if(object.has("isRecovery")){
                SendMessageLayer sendMessageLayer = new SendMessageLayer(this);
                sendMessageLayer.sendMessage("/tosmartwatch/response_recovery",object);
            }

        }

    }

    private void update_ws_status(String status){
        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        esmConfigurationPreferences.setConntectionStatus(status);
    }

    private void update_esm_session(Boolean status){
        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
        sessionPreferencesUtil.setSessionRunning(status);
    }

    @SneakyThrows
    private void update_esm_recover_session(JSONObject status){

        SendMessageLayer sendMessageLayer = new SendMessageLayer(this);
        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        if(status.getBoolean("participant_finished") == true){

            Log.d(TAG, "Nothing to recover.");

            sessionPreferencesUtil.setSessionRecoveryNecessary(false);

            JSONObject update = sessionPreferencesUtil.getSessionInformation();
            sendMessageLayer.sendMessage(getString(R.string.PATH_TO_SMARTWATCH_RESPONSE_SESSION_STATE),update);


        } else {
            // Nicht alles in Ordnung. Wir senden der Smartwatch die Möglichkeit eine Session wiederherzustellen
            if(esmConfigurationPreferences.getPrefDevice() == 0){
                sendMessageLayer.sendMessage(getString(R.string.PATH_TO_SMARTWATCH_ASK_RECOVERY),status);
            }
            sessionPreferencesUtil.setSessionRecoveryNecessary(true);
            Log.d(TAG, "Asked Wearable for recovery.");
        }
    }

    @SneakyThrows
    private void send_not_ready_update_to_sw(){

        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
        sessionPreferencesUtil.cleanUpSessionPreferences();

        SendMessageLayer sendMessageLayer = new SendMessageLayer(this);

        JSONObject update = sessionPreferencesUtil.getSessionInformation();
        sendMessageLayer.sendMessage(getString(R.string.PATH_TO_SMARTWATCH_RESPONSE_SESSION_STATE),update);

    }






    private void esm_digest_request_smartwatch(String text){
        Log.d(TAG, "onMessage called. Message: " + text);
        try {
            JSONObject j_msg = new JSONObject(text);

            if(j_msg.has("request_type")){

                String request_type = j_msg.getString("request_type");
                String path = "";

                switch (request_type){
                    case "questionnaire":
                        path = MessageLayerStatics.TOSMARTWATCH_QUESTIONNAIRE;
                        break;
                    case "pre_post_questionnaire":
                        path = MessageLayerStatics.TOSMARTWATCH_PRE_POST_QUESTIONNAIRE;
                        break;
                    case "micro_questionnaire":
                        path = MessageLayerStatics.TOSMARTWATCH_MICRO_SURVEY;
                        break;
                    case "notification":
                        path = MessageLayerStatics.TOSMARTWATCH_NOTIFICATION;
                        break;
                    case "sensor_service":
                        path = MessageLayerStatics.TOSMARTWATCH_SENSOR_REQUEST;
                        JSONArray sensor_services = j_msg.getJSONArray("sensor_services");
                        digest_requested_sensor_services(sensor_services,j_msg);
                        break;

                    default:
                        Log.e(TAG, "Could not assign request.");
                        Log.e(TAG,j_msg.toString());
                        break;
                }
                messageService.sendMessage(path, j_msg);
                Log.d(TAG,"Message send to smartwatch");

            } else if (j_msg.has("participant_finished")){
                Log.d(TAG,"Received Participant - Session Informations");
                update_esm_recover_session(j_msg);

            } else if((j_msg.has("participant") || j_msg.has("isRecovery")) && j_msg.has("requested_pre") ){
                Log.d(TAG, "Received Session Object");
                esm_digest_session(j_msg);
            } else {
                Log.e(TAG, "Could not assign request. [IF-BLOCK]");
                Log.e(TAG,j_msg.toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void esm_digest_request_handheld(String text){
        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();
        Log.d(TAG,"Inside esm_digest_handheld");
        Log.d(TAG, "onMessage called. Message: " + text);
        try {
            JSONObject j_msg = new JSONObject(text);

            if(j_msg.has("request_type")){

                String request_type = j_msg.getString("request_type");
                Intent intent = null;

                switch (request_type){
                    case "questionnaire":
                        intent = new Intent();
                        intent.setAction(getString(R.string.ACTION_SURVEY));
                        intent.putExtra("message", text);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        break;
                    case "pre_post_questionnaire":
                        int esm_msg_hash = text.hashCode();
                        j_msg.put("timestamp_question_asked", RequestResponseUtils.getTimeStamps(now, elapsed));
                        j_msg.put("identifier", esm_msg_hash);

                        if(j_msg.has("faked")){
                            Intent intent1 = new Intent(getApplicationContext(), VibrationService.class);
                            getApplicationContext().startForegroundService(intent1);
                            if(j_msg.getString("faked").equals("pre_survey")){

                                SessionPreferencesUtil sessionPreferences = new SessionPreferencesUtil(getApplicationContext());
                                sessionPreferences.setSessionRunning(true);
                                MessagesSingleton.getInstance().addMessageReceived(j_msg);
                                MessagesSingleton.getInstance().RequestSessionStart();
                            } else if (j_msg.getString("faked").equals("post_survey")){
                                MessagesSingleton.getInstance().addMessageReceived(j_msg);
                            }
                        } else {

                            intent = new Intent(this, QuestionnaireActivity.class);
                            intent.putExtra("message", j_msg.toString());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        }



                        break;
                    case "micro_questionnaire":
                        intent = new Intent();
                        intent.setAction(getString(R.string.ACTION_MICRO_SURVEY));
                        intent.putExtra("message", text);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        break;
                    case "notification":
                        Log.d(TAG,"Notifcation found");
                        intent = new Intent();
                        intent.setAction(getString(R.string.ACTION_NOTIFY));
                        intent.putExtra("message", text);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        break;
                    case "sensor_service":

                        JSONArray sensor_services = j_msg.getJSONArray("sensor_services");
                        digest_requested_sensor_services(sensor_services,j_msg);
                        break;
                    default:
                        Log.e(TAG, "Could not assign request.");
                        Log.e(TAG,j_msg.toString());
                        break;
                }

            } else if (j_msg.has("participant_finished")){
                Log.d(TAG,"Received Participant - Session Informations");
                update_esm_recover_session(j_msg);

            } else if((j_msg.has("participant") || j_msg.has("isRecovery")) && j_msg.has("requested_pre") ){
                Log.d(TAG, "Received Session Object");
                esm_digest_session(j_msg);
            } else {
                Log.e(TAG, "Could not assign request. [IF-BLOCK]");
                Log.e(TAG,j_msg.toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @SneakyThrows
    public void digest_requested_sensor_services(JSONArray services, JSONObject original_msg){
        Intent intent = null;

        for(int i = 0; i < services.length(); i++){

            int sensor_type = services.getJSONObject(i).getInt("service_type");
            Class tmp_class = null;


            if(services.getJSONObject(i).getInt("datasource") == 1){

                switch (sensor_type){
                    case 0:
                        Log.d(TAG, "Registering handheld sensor services");
                        Log.w(TAG,original_msg.getString("event_id"));
                        intent = new Intent(getApplicationContext(), BluetoothRecordingService.class);
                        intent.putExtra("request",services.getJSONObject(i).toString());
                        intent.putExtra("original_request",original_msg.toString());

                        if(!ServiceRunningUtil.isMyServiceRunning(BluetoothRecordingService.class,
                                getApplicationContext())){
                            getApplicationContext().startForegroundService(intent);
                        }else{
                            Log.e(TAG, BluetoothRecordingService.class.getSimpleName() + " is running");
                        }

                        intent = new Intent(getApplicationContext(), WifiRecordingService.class);
                        intent.putExtra("request",services.getJSONObject(i).toString());
                        intent.putExtra("original_request",original_msg.toString());

                        if(!ServiceRunningUtil.isMyServiceRunning(WifiRecordingService.class,
                                getApplicationContext())){
                            getApplicationContext().startForegroundService(intent);
                        }else{
                            Log.e(TAG, WifiRecordingService.class.getSimpleName() + " is running");
                        }

                        intent = new Intent(getApplicationContext(), GPSRecordingService.class);
                        intent.putExtra("request",services.getJSONObject(i).toString());
                        intent.putExtra("original_request",original_msg.toString());
                        tmp_class = GPSRecordingService.class;
                        break;
                    case 1:
                        intent = new Intent(getApplicationContext(), PhysicalRecordingService.class);
                        intent.putExtra("request",services.getJSONObject(i).toString());
                        intent.putExtra("original_request",original_msg.toString());

                        if(!ServiceRunningUtil.isMyServiceRunning(PhysicalRecordingService.class,
                                getApplicationContext())){
                            getApplicationContext().startForegroundService(intent);
                        }else{
                            Log.e(TAG, PhysicalRecordingService.class.getSimpleName() + " is running");
                        }

                        intent = new Intent(getApplicationContext(),AmbientNoiseRecordingService.class);
                        intent.putExtra("request",services.getJSONObject(i).toString());
                        intent.putExtra("original_request",original_msg.toString());
                        tmp_class = AmbientNoiseRecordingService.class;
                        Log.d(TAG, tmp_class.getSimpleName() + " should run");
                        break;
                    case 2:
                        intent = new Intent(getApplicationContext(), PhysiologicalRecordingService.class);
                        intent.putExtra("request",services.getJSONObject(i).toString());
                        intent.putExtra("original_request",original_msg.toString());
                        tmp_class = PhysiologicalRecordingService.class;
                        Log.d(TAG, tmp_class.getSimpleName() + " should run");
                        break;
                    case 3:
                        intent = new Intent(getApplicationContext(), BehavioralRecordingService.class);
                        intent.putExtra("request",services.getJSONObject(i).toString());
                        intent.putExtra("original_request",original_msg.toString());
                        tmp_class = BehavioralRecordingService.class;
                        Log.d(TAG, tmp_class.getSimpleName() + " should run");
                        break;
                    case 4:
                        intent = new Intent(getApplicationContext(), VirtualSensorService.class);
                        intent.putExtra("request",services.getJSONObject(i).toString());
                        intent.putExtra("original_request",original_msg.toString());
                        tmp_class = VirtualSensorService.class;
                        Log.d(TAG, tmp_class.getSimpleName() + " should run");
                        break;
                    case 5:
                        intent = new Intent(getApplicationContext(), ProcessTrackingService.class);
                        intent.putExtra("request",services.getJSONObject(i).toString());
                        intent.putExtra("original_request",original_msg.toString());
                        tmp_class = ProcessTrackingService.class;
                        Log.d(TAG, tmp_class.getSimpleName() + " should run");
                        break;
                }


                if(tmp_class != null && intent != null){
                    if(!ServiceRunningUtil.isMyServiceRunning(tmp_class, getApplicationContext())){
                        getApplicationContext().startForegroundService(intent);
                    }else{
                        Log.e(TAG, tmp_class.getSimpleName() + " is running");
                    }
                }
            }
        }

        messageService.sendMessage(MessageLayerStatics.TOSMARTWATCH_SENSOR_REQUEST,original_msg);


    }


    @androidx.annotation.Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void sendErrorMessage(String text){
        NotificationManager notificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        String CHANNEL_ID = getApplicationContext().getResources().getString(R.string.notiChannelID);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(),CHANNEL_ID)
                .setContentText(text)
                .setSmallIcon(R.drawable.icon_exit)
                .setOngoing(false)
                .setAutoCancel(true);
        notificationManager.notify("error-message", 9696, notification.build());
    }


}
