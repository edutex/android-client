package de.dipf.edutec.edutex.androidclient.messageservice;
import org.json.JSONException;
import org.json.JSONObject;

import de.dipf.edutec.edutex.androidclient.messagestruct.OnSuccessSendPair;

import java.util.ArrayList;
import java.util.List;

public class MessagesSingleton {

    public static MessagesSingleton instance;
    public List<JSONObject> messagesSend;
    public List<JSONObject> messagesReceived;
    public List<OnSuccessSendPair> msgSendList;

    public MessagesSingleton(){
        messagesSend = new ArrayList<JSONObject>();
        messagesReceived = new ArrayList<JSONObject>();
        msgSendList = new ArrayList<OnSuccessSendPair>();
    }
    public static MessagesSingleton getInstance(){
        if(MessagesSingleton.instance == null){
            MessagesSingleton.instance = new MessagesSingleton();
        }
        return MessagesSingleton.instance;
    }


    public void addMessageReceived(JSONObject message) throws JSONException {
        if(mListener != null) mListener.onStateChange(message);
    }

    public void RequestRemotePreSurvey() throws JSONException{
        if(mListener != null) mListener.onActionTriggered("{'action_remote_session_start':1}");
    }

    public void RequestPreSurveys() throws JSONException {
        if(mListener != null) mListener.onActionTriggered("{'action_pre_session_surveys':1}");
    }

    public void RequestSessionStart() throws JSONException {
        if(mListener != null) mListener.onActionTriggered("{'action_start_session':1}");
    }

    public void RequestPostSurveys() throws JSONException {
        if(mListener != null) mListener.onActionTriggered("{'action_post_session':1}");
    }

    public void RequestForceStopSession() throws JSONException {
        if(mListener != null) mListener.onActionTriggered("{'action_force_stop':1}");
    }

    public void RequestSessionRecovery(Boolean recover) throws  JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("action_recover_last_session",recover);
        if(mListener != null) mListener.onActionTriggered(jsonObject.toString());
    }
    public void cleanData(){
        messagesReceived = new ArrayList<JSONObject>();
        messagesSend = new ArrayList<JSONObject>();
        msgSendList = new ArrayList<OnSuccessSendPair>();
    }

    // Variabel change Listener
    private MessagesSingleton.Listener mListener = null;
    public void registerListener(MessagesSingleton.Listener listener) {mListener = listener; }
    public void unregisterListener(){mListener = null;}
    public interface Listener{
        void onStateChange(JSONObject message) throws JSONException;
        void onActionTriggered(String action) throws JSONException;
    }





}
