package de.dipf.edutec.edutex.androidclient.messageservice;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.auth.AuthPreferences;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import de.dipf.edutec.edutex.androidclient.util.ServiceRunningUtil;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.MessageLayerStatics;


public class ReceiverMessageLayer extends WearableListenerService {

    private String TAG = "ReceiverMessageLayer";


    public ReceiverMessageLayer(){}

    @SneakyThrows
    @SuppressLint("HardwareIds")
    @Override
    public void onMessageReceived(MessageEvent messageEvent){

        Log.d(TAG,"Received Message from path: " + messageEvent.getPath());

        SessionPreferencesUtil sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        MessagesSingleton messagesSingleton = MessagesSingleton.getInstance();
        String path = messageEvent.getPath();
        String message = new String(messageEvent.getData());
        String android_id = sessionPreferencesUtil.getAndroidId();
        String session_id = sessionPreferencesUtil.getSessionId();
        String user_id =sessionPreferencesUtil.getParticipantId();
        SendMessageLayer sendMessageLayer  = new SendMessageLayer(getApplicationContext());

        // Checking if wearable wants to get a delivered ping
        try{
            JSONObject object = new JSONObject(message);
            if(object.has("deliveryUUID")){

                JSONObject ping = new JSONObject();
                ping.put("deliveryUUID",object.getString("deliveryUUID"));
                sendMessageLayer.sendMessage(MessageLayerStatics.TOSMARTWATCH_PING, ping);
                Log.d(TAG, "PING: " + object.getString("deliveryUUID"));
            }
        }catch (Exception er){
            Log.e(TAG, er.toString());
        }



        switch(path){
            case MessageLayerStatics.TOHANDHELD_REQUEST_ACRA_CREDENTIALS:
                AuthPreferences authPreferences = new AuthPreferences(getApplicationContext());
                sendMessageLayer.sendMessage(MessageLayerStatics.TOSMARTWATCH_RESPONSE_ACRA_CREDENTIALS, authPreferences.getCredentials());
                break;
            case MessageLayerStatics.TOHANDHELD_REQUEST_SESSIONSTATE:
                JSONObject response = sessionPreferencesUtil.getSessionInformation();
                if(response != null){
                    sendMessageLayer.sendMessage(getString(R.string.PATH_TO_SMARTWATCH_RESPONSE_SESSION_STATE),response);
                }
                break;
            case MessageLayerStatics.TOHANDHELD_START_SESSION:
                Log.d(TAG, "Starting ESM Service..");
                try {
                    String ws_status = esmConfigurationPreferences.getConnectionStatus();
                    if(ws_status.equals("Connected")){
                        MessagesSingleton.getInstance().RequestPreSurveys();
                    } else {
                        Log.d(TAG, "Websocket Status: " + ws_status);
                        JSONObject obj = new JSONObject();
                        obj.put("error_notification","Websocket Status is not connected");
                        sendMessageLayer.sendMessage(getString(R.string.PATH_TO_SMARTWATCH_TOAST_TO_USER),obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case MessageLayerStatics.TOHANDHELD_STOP_SESSION:
                try {
                    MessagesSingleton.getInstance().RequestPostSurveys();
                    ServiceRunningUtil.terminate_services(getApplicationContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case MessageLayerStatics.TOHANDHELD_RESPONSE_RECOVERY:
                try{
                    if (message.equals(String.valueOf(true))){
                        MessagesSingleton.getInstance().RequestSessionRecovery(true);
                    } else if (message.equals(String.valueOf(false))){
                        MessagesSingleton.getInstance().RequestSessionRecovery(false);
                    }
                    Log.d(TAG, "received response for recovery: " + message);

                }catch (Exception e ){
                    Log.d(TAG, "Exception while transmitting Response Recovery");
                }
                break;

            case MessageLayerStatics.TOHANDHELD_SMARTWATCH_CRASHED:
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                String CHANNEL_ID = getResources().getString(R.string.notiChannelID);
                Notification notification =  new NotificationCompat.Builder(getApplicationContext(),CHANNEL_ID)
                        .setContentTitle("Smartwatch App Crashed")
                        .setContentText("Die Smartwatch App hatte einen Fehler. Bitte starte die Lernsession erneut.")
                        .setSmallIcon(R.drawable.ic_cc_checkmark)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                notificationManager.notify(0, notification);
                break;

            case MessageLayerStatics.TOHANDHELD_RESPONSE_SENSOR_REQUEST:
                JSONObject object = new JSONObject(message);
                object.put("userID", user_id);
                object.put("sessionID", session_id);
                MessagesSingleton.getInstance().addMessageReceived(object);
                break;

            default:
                JSONObject msg = null;
                try {
                    msg = new JSONObject(new String(messageEvent.getData()));
                    msg.put("androidID",android_id);
                    msg.put("sessionID",session_id);
                    msg.put("userID",user_id);
                    messagesSingleton.addMessageReceived(msg);

                    if(msg.has("questionnaire_type")){
                        if(msg.getInt("questionnaire_type") == 2){
                            MessagesSingleton.getInstance().RequestSessionStart();
                            Log.w(TAG,"Sended start session, normal pre.");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }





    }




}
