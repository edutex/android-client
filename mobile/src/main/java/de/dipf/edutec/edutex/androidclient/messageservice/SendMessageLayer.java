package de.dipf.edutec.edutex.androidclient.messageservice;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONException;
import org.json.JSONObject;

import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.MessageLayerStatics;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class SendMessageLayer extends Application {

    private static String TAG = "SendMessageLayer";

    public static SendMessageLayer instance;
    public MessagesSingleton messagesSingleton;
    public Context context;

    public SendMessageLayer(Context context){
        this.context = context;
        messagesSingleton = MessagesSingleton.getInstance();
    }

    public void sendMessage(String path, JSONObject message) throws JSONException {


        if(path.equals(MessageLayerStatics.TOSMARTWATCH_RESPONSE_SESSION_STATE)){
            NewThread sendMessage = new NewThread(path,message.toString());
            sendMessage.start();
        }else{
            if(!(context == null)){
                ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);
                if(esmConfigurationPreferences.getPrefDevice() == 0){
                    NewThread sendMessage = new NewThread(path,message.toString());
                    sendMessage.start();
                } else{
                    Log.d(TAG, "Current pref device is 'mobile'");
                }
            }else{
                Log.e(TAG,"Context is null..");
            }
        }


    }




    public class NewThread extends Thread{
        String path;
        JSONObject message;

        @SneakyThrows
        public NewThread(String path, String message) throws JSONException {
            this.path = path;
            this.message = new JSONObject(message);
        }

        public void run(){

            try {
                Task<List<Node>> nodeListTask =
                        Wearable.getNodeClient(context).getConnectedNodes();

                List<Node> nodes = Tasks.await(nodeListTask);
                for(Node node :nodes){
                    Task<Integer> sendMessageTask =
                            Wearable.getMessageClient(context).sendMessage(node.getId(), path, message.toString().getBytes());

                    sendMessageTask.addOnSuccessListener(new OnSuccessListener<Integer>() {
                        @Override
                        public void onSuccess(Integer integer) {
                            Log.d(TAG, "Nachricht wurde an die Smartwatch erfolgreich übermittelt");
                        }
                    });

                    sendMessageTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d(TAG, "Nachricht wurde an die Smartwatch nicht erfolgreich übermittelt");
                        }
                    });

                }
            } catch (InterruptedException e) {
                Log.e(TAG, String.valueOf(e));
            } catch (ExecutionException e) {
                Log.e(TAG, String.valueOf(e));
            } catch (Exception e){

            }

        }

    }



}
