package de.dipf.edutec.edutex.androidclient.processtracking;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import de.dipf.edutec.edutex.androidclient.util.TrackingSensorPreferences;

public class ProcessTrackingService  extends Service {

    public static final String TAG = ProcessTrackingService.class.getSimpleName();

    JSONObject request;
    JSONObject original_request;
    TrackingSensorPreferences trackingSensorPreferences;
    int duration;
    long begin;


    private Handler handler;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            stopSelf();
        }
    };



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(), fgNotificationCreator.getNotification());
        Log.d(TAG, "OnStartIsCalled");
        try{

            request = new JSONObject(intent.getStringExtra("request"));
            original_request = new JSONObject(intent.getStringExtra("original_request"));
            duration = request.getInt("duration");
            begin = System.currentTimeMillis();
        }catch (Exception er){

            stopSelf();

        }

        // Window Change Detection
        // NotificationListener
        trackingSensorPreferences = new TrackingSensorPreferences(getApplicationContext());
        trackingSensorPreferences.setTrackRequest(request);
        trackingSensorPreferences.setTrackOriginalRequest(original_request);
        trackingSensorPreferences.setAllTrue();
        Log.d(TAG,"Sensors should be running");
        // AppUsageStats
        UStats.getUsageStatsList(getApplicationContext());
        Log.d(TAG,"Usage Stats should be sended back");
        handler = new Handler();
        handler.postDelayed(runnable,duration*1000);

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy(){

        UStats.getUsageStatsList(getApplicationContext());
        try{
            UStats.getUsageStatistics(begin, System.currentTimeMillis(),getApplicationContext());
            UStats.getAllEvents(begin,System.currentTimeMillis(),getApplicationContext());
        }catch (Exception er){

        }
        Log.d(TAG,"Usage Stats should be sended back, onDestroy");
        trackingSensorPreferences.setAllFalse();
        trackingSensorPreferences = null;
        handler.removeCallbacks(runnable);
        stopForeground(false);
        super.onDestroy();
    }










    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
