package de.dipf.edutec.edutex.androidclient.processtracking;


import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.time.format.DateTimeFormatter;

import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.util.TrackingSensorPreferences;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class WindowChangeDetectingService extends AccessibilityService {

    private final String TAG = WindowChangeDetectingService.class.getSimpleName();
    public String ServiceName = "WindowChangeDetectingService";
    public double ServiceVersion = 1.0;

    ForegroundNotificationCreator fgNotificationManager;
    JSONObject request;
    JSONObject original_request;
    TrackingSensorPreferences trackingSensorPreferences;

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.d(TAG,"OnServiceConnected");

        //Configure these here for compatibility with API 13 and below.
        AccessibilityServiceInfo config = new AccessibilityServiceInfo();
        config.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED;
        config.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        config.flags = AccessibilityServiceInfo.FLAG_INCLUDE_NOT_IMPORTANT_VIEWS;
        setServiceInfo(config);

        trackingSensorPreferences = new TrackingSensorPreferences(getApplicationContext());

    }



    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "creating WindowChangeDetectingService ");
        this.fgNotificationManager =
                ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startid){

        Log.d(TAG,"Started Service: WindowChangeDetectingService");

        startForeground(
                fgNotificationManager.getId(),
                fgNotificationManager.getNotification());

        return START_NOT_STICKY;
    }


    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Instant now = Instant.now();
        long elasped = SystemClock.elapsedRealtimeNanos();

        Boolean tracking = trackingSensorPreferences.getTrackWindowChange();
        Log.d(TAG, "onAccessibilityEvent, Tracking: " + String.valueOf(tracking));

        if(tracking){
            request = trackingSensorPreferences.getTrackRequest();
            original_request = trackingSensorPreferences.getTrackOriginalRequest();
            JSONObject jsonObject = new JSONObject();
            try{
                Log.d(TAG, String.format("%s %s", "New foreground activity:", event.getPackageName().toString()));
                //jsonObject.put("datasource","mobile");
                jsonObject.put("package_name", event.getPackageName().toString());
                jsonObject.put("event_type", event.getEventType());
                jsonObject.put("event_time",event.getEventTime());
                jsonObject.put("action",event.getAction());
                jsonObject.put("date_time", String.valueOf(DateTimeFormatter.ISO_INSTANT.format(now)));

                Log.d(TAG, jsonObject.toString(4));

            } catch (Exception er){
                Log.e(TAG, "JSON Exception occured");
            }


            try {
                JSONObject tmp = new JSONObject();
                tmp.put("service_name",ServiceName);
                tmp.put("service_version",ServiceVersion);
                tmp.put("sensor_name","WindowChangeDetectingService");
                tmp.put("sensor_type", -8);
                tmp.put("datasource","mobile");
                tmp.put("timestamp", RequestResponseUtils.getTimeStamps(now, elasped));
                tmp.put("values",jsonObject);
                tmp.put("sensor_service_id",request.getInt("id"));
                tmp.put("service_type",request.getInt("service_type"));
                tmp.put("request_id",original_request.getInt("id"));
                tmp.put("request_type", original_request.getString("request_type"));
                tmp.put("event_id",original_request.getString("event_id"));

                JSONArray array2 = new JSONArray();
                array2.put(tmp);

                JSONObject packet = new JSONObject();
                packet.put("events",array2);
                packet.put("request_type","sensor_service");
                packet.put("session_id",original_request.getString("session_id"));

                MessagesSingleton.getInstance().addMessageReceived( packet);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    @Override
    public void onInterrupt() {

    }


    @Override
    public void onDestroy(){
        Log.d(TAG, "onDestroy is called.");
        stopForeground(false);
        super.onDestroy();
    }




}
