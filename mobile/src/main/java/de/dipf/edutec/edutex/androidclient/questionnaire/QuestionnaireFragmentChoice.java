package de.dipf.edutec.edutex.androidclient.questionnaire;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.widget.TextViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.QuestionnaireActivity;
import de.dipf.edutec.edutex.androidclient.gesture.OnSwipeTouchListener;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;


public class QuestionnaireFragmentChoice extends Fragment implements ChoiceAdapter.ItemClickListener {

    private static String TAG = QuestionnaireFragmentChoice.class.getSimpleName();
    public ChoiceAdapter choiceAdapter;
    public RecyclerView recyclerView;
    private Context context;
    private JSONObject question_data;
    private JSONArray answers;
    private int selectedItem;
    private int position_added = -1 ;
    public UserInputFragment frag_user_input;
    protected QuestionnaireActivity activity;


    public QuestionnaireFragmentChoice(Context context, JSONObject obj){
        this.context = context;
        this.question_data = obj;
    }

    public QuestionnaireFragmentChoice(Context context, JSONObject obj, int selected ) throws JSONException {
        this.context = context;
        this.question_data = obj;
        this.selectedItem = selected;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        activity = (QuestionnaireActivity) context;
    }


    @SuppressLint("ClickableViewAccessibility")
    @SneakyThrows
    @Override
    public void onStart(){
        super.onStart();

        TextView question = getView().findViewById(R.id.tv_question_survey);

        question.setText(question_data.getString("question"));
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(question,6,16,1, TypedValue.COMPLEX_UNIT_SP);
        answers = question_data.getJSONArray("answers");
        String[] answers_strings = new String[answers.length()];

        for(int i = 0; i < answers.length(); i++){
            answers_strings[i] = answers.getJSONObject(i).getString("answer");
        }

        ArrayList<String> poss_answers = new ArrayList<>((Arrays.asList(answers_strings)));

        if(question_data.has("custom_answers_allowed")){
            int add_choice = question_data.getInt("custom_answers_allowed");
            if(add_choice == 1){
                poss_answers.add("Voice input");
                position_added = poss_answers.indexOf("Voice input");
                this.frag_user_input = new UserInputFragment("voice_only");

            }
            if(add_choice == 2){
                poss_answers.add("Keyboard input");
                position_added = poss_answers.indexOf("Keyboard input");
                this.frag_user_input = new UserInputFragment("keyboard_only");
            }
            if(add_choice == 3){
                poss_answers.add("Generate input");
                position_added = poss_answers.indexOf("Generate input");
                this.frag_user_input = new UserInputFragment("allow_both");
            }
        }


        recyclerView = getView().findViewById(R.id.rec_choice);
        recyclerView.setHasFixedSize(true);


        recyclerView.setOnTouchListener(new OnSwipeTouchListener(getContext()){


            @Override
            public void onSwipeLeft() throws JSONException {
                Log.d("GestureTAG", "onSwipeLeft");
                ((QuestionnaireActivity)getActivity()).setLastFragment();
            }

            @Override
            public void onSwipeRight() throws JSONException {
                ((QuestionnaireActivity)getActivity()).setNextFragment();
                Log.d("GestureTAG", "onSwipeRight");
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        if(question_data.has("user_answer")){

            try{
                int user_answer = question_data.getInt("user_answer");
                String selected_answer = getAnswerPosition(user_answer);
                Log.e(TAG,selected_answer);
                choiceAdapter = new ChoiceAdapter(getContext(), poss_answers, selected_answer);
            }catch (Exception e){
                Log.e(TAG,e.toString());
                choiceAdapter = new ChoiceAdapter(getContext(), poss_answers, String.valueOf(position_added));
            }
        } else {
            choiceAdapter = new ChoiceAdapter(getContext(),poss_answers);
        }
        choiceAdapter.setClickListener(this);
        recyclerView.setAdapter(choiceAdapter);
    }

    @SneakyThrows
    @Override
    public void onItemClick(View view, int position) {
        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();

        if(position == position_added ){
            ((QuestionnaireActivity)getActivity()).setOnTopFragment(frag_user_input);
            frag_user_input.registerListener(new UserInputFragment.Listener() {

                @Override
                public void onStateChange(String user_input) throws JSONException {
                    question_data.put("user_answer",user_input);
                    question_data.put("custom_answer_given",true);
                    question_data.put("timestamp_answered", RequestResponseUtils.getTimeStamps(now, elapsed));
                    System.out.println(question_data);
                    activity.replaceQuestion(question_data);
                    activity.setNextFragment();
                    onDestroy();
                }

                @Override
                public void onReturn() {
                    activity.getBackToFragment();
                }

            });
        } else {

            question_data.put("user_answer",answers.getJSONObject(position).getInt("id"));
            question_data.put("custom_answer_given",false);
            question_data.put("timestamp_answered", RequestResponseUtils.getTimeStamps(now, elapsed));
            activity.replaceQuestion(question_data);
            activity.setNextFragment();
            onDestroy();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_choice, container, false);
        return view;
    }


    private String getAnswerPosition(int id){
        for(int i=0; i < answers.length(); i++){
            try {
                if(answers.getJSONObject(i).getInt("id") == id){
                    return String.valueOf(i);
                }
            } catch (JSONException e) {
                Log.e(TAG,e.toString());
            }

        }
        return "Error";

    }

}
