package de.dipf.edutec.edutex.androidclient.sensorservice;

import android.app.Service;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.time.Instant;
import java.util.ArrayList;

import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class AmbientNoiseRecordingService extends Service {
    private static final String TAG = AmbientNoiseRecordingService.class.getSimpleName() + "-Mobile";
    public String ServiceName = "Mobile Ambient Noise Sensor Recording Service";
    public static double ServiceVersion = 1.2;

    private String fileName = null;
    private MediaRecorder recorder = null;
    private int noiseLevel;
    private ArrayList<JSONObject> events = new ArrayList<>();
    private final Handler handler = new Handler();
    // Value is in seconds.
    private int recording_segment_time = 5;
    // Value is in seconds.
    private int recording_time;
    private int max_counter;

    private JSONObject request;
    private JSONObject original_request;
    private JSONArray jsonArrayValues = new JSONArray();

    private int count = 0;
    String mFilePath;

    Handler packetHandler;
    int packetIntervalSec = 50;

    private final Runnable noiseSamplingRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "Recording Runnable: " + count + "/" + max_counter);
            if (count == 0 && recorder != null) {
                recorder.reset();
            } else {
                stopRecording();
            }
            start_recording();

            // Service is called every X milliseconds.
            handler.postDelayed(this, recording_segment_time * 1000);
            if (count >= max_counter) {
                packetHandler.removeCallbacks(transfer_Packets);
                stopSelf();
            }
            count += 1;
        }
    };

    Runnable transfer_Packets = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "Inside Runnable. Transfer Packets");
            JSONArray valuesCopy = jsonArrayValues;
            respond();
            packetHandler.postDelayed(this, packetIntervalSec * 1000);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(), fgNotificationCreator.getNotification());

        if (intent.hasExtra("request")) {
            String request_string = intent.getStringExtra("request");
            String original_request_string = intent.getStringExtra("original_request");
            try {
                request = new JSONObject(request_string);
                original_request = new JSONObject(original_request_string);
                recording_time = request.optInt("duration", 60);

            } catch (JSONException e) {
                Log.e(TAG, e.toString());
                stopSelf();
                return START_NOT_STICKY;
            }
        } else {
            Log.e(TAG, "Did not received the correct intent. Closing Service.");
            stopSelf();
            return START_NOT_STICKY;
        }
        max_counter = recording_time / recording_segment_time;

        noiseSamplingRunnable.run();
        packetHandler = new Handler();
        packetHandler.postDelayed(transfer_Packets, packetIntervalSec * 1000);

        return START_NOT_STICKY;
    }

    public void start_recording() {
        if (recorder != null) {
            recorder.reset();
        } else {
            recorder = new MediaRecorder();
            String manufacturer = Build.MANUFACTURER;
            Log.d(TAG, "Successfully created recorder with Manufacturer '" + manufacturer + "'.");
        }

        String path = getApplicationContext().getFilesDir().getPath();
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }

        mFilePath = file + "/" + "REC_TMP.3gp";
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(mFilePath);
        recorder.setOnErrorListener((mr, what, extra) -> {

            Log.e(TAG, "MR: " + mr + ", Error Code: " + String.valueOf(what) + ", Extra Code: " + String.valueOf(extra));
            try {
                recorder.release();
            } catch (Exception er) {
                Log.e(TAG, er.toString());
            }
            recorder = null;
        });

        try {
            recorder.prepare();
            recorder.start();
            recorder.getMaxAmplitude();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void stopRecording() {
        if (recorder == null) {
            return;
        }
        try {
            Instant instant = Instant.now();
            long elapsed = SystemClock.elapsedRealtimeNanos();

            noiseLevel = recorder.getMaxAmplitude();

            JSONObject sample = new JSONObject();
            try {
                ArrayList<Float> valuesArray = new ArrayList<Float>();
                valuesArray.add(Float.valueOf(noiseLevel));
                JSONObject values = new JSONObject();
                values.put("values", new JSONArray(valuesArray));
                values.put("event_timestamp", elapsed);

                sample.put("service_name", ServiceName);
                sample.put("service_version", ServiceVersion);
                sample.put("sensor_name", "MediaRecorder");
                sample.put("sensor_type", -5);
                sample.put("datasource", "mobile");
                sample.put("timestamp", RequestResponseUtils.getTimeStamps(instant, elapsed));
                sample.put("values", values);
                sample.put("sensor_service_id", request.getInt("id"));
                sample.put("service_type", request.getInt("service_type"));
                sample.put("request_id", original_request.getInt("id"));
                sample.put("request_type", original_request.getString("request_type"));
                sample.put("event_id", original_request.getString("event_id"));
            } catch (JSONException e) {
                Log.e(TAG, "Unable to record sample!");
                Log.e(TAG, e.toString());
                throw e;
            }

            events.add(sample);
        } catch (Exception er) {
            Log.e(TAG, "Unable to stop recording!");
            Log.e(TAG, er.toString());
        }
    }

    public void stopRepeat() {
        handler.removeCallbacks(noiseSamplingRunnable);
    }

    private void respond() {
        if (events.size() > 0) {
            events.remove(events.size() - 1);
            JSONObject final_obj = new JSONObject();
            try {
                final_obj.put("events", new JSONArray(events));
                final_obj.put("request_type", "sensor_service");
                final_obj.put("session_id", original_request.getString("session_id"));
                Log.d(TAG, final_obj.toString());
                MessagesSingleton.getInstance().addMessageReceived(final_obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "No events where recorded!");
        }
    }

    @Override
    public void onDestroy() {
        try {
            stopRecording();
            stopRepeat();
            respond();
            packetHandler.removeCallbacks(transfer_Packets);
            recorder.release();
            recorder = null;
        } catch (Exception e) {
            Log.e(TAG, "Recorder destruction failed!");
            Log.e(TAG, e.toString());
        }
        try {
            File file = new File(mFilePath);
            if (file.exists()) {
                file.delete();
                Log.d(TAG, "File deleted");
            }
        } catch (Exception er) {
            Log.e(TAG, "File deletion failed! " + er.toString());
        }
        stopForeground(false);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}