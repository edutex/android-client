package de.dipf.edutec.edutex.androidclient.sensorservice;

import android.Manifest;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.ArrayList;

import de.dipf.edutec.edutex.androidclient.R;
//import de.dipf.edutec.thriller.experiencesampling.messageservice.MessageLayerSender;
import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class BluetoothRecordingService extends Service {


    private static final String TAG = "BluetoothRecordingService";
    public String ServiceName = "Wifi Recording Service";
    public double ServiceVersion = 1.0;

    Handler handler;
    Runnable runnable;

    JSONObject request;
    JSONObject original_request;
    ArrayList<JSONObject> events;


    JSONObject jsonRequest;
    JSONArray jsonArrayValues;
    JSONObject standardJson;
    JSONArray jsonArrayTimestamps;
    JSONArray value_unit;
    Context myContext;
    private BluetoothAdapter bluetoothAdapter;


    @SneakyThrows
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(), fgNotificationCreator.getNotification());

        try {
            request = new JSONObject(intent.getStringExtra("request"));
            original_request = new JSONObject(intent.getStringExtra("original_request"));
            events = new ArrayList<>();
        } catch (Exception er) {
            stopSelf();
            return START_NOT_STICKY;
        }


        // Initialize Json Arrays and context
        jsonArrayValues = new JSONArray();
        jsonArrayTimestamps = new JSONArray();
        value_unit = new JSONArray();
        standardJson = new JSONObject();
        myContext = getApplicationContext();

        try {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_SCAN)
                    == PackageManager.PERMISSION_GRANTED) {

                // Set Bluetooth Adapter and Start discovery of bluetooth devices
                bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                bluetoothAdapter.startDiscovery();

            }

            // Create Intent Filter and register receiver for bluetooth broadcast receiver
            IntentFilter intentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            getApplicationContext().registerReceiver(receiver, intentFilter);

        } catch (Exception er) {

        }

        // Use handler to wait for bluetooth devices to be found
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                respond();
            }
        };

        handler.postDelayed(runnable, 15000);


        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    // For detecting bluetooth devices
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @SneakyThrows
        @Override
        public void onReceive(Context context, Intent intent) {
            Instant now = Instant.now();
            long elasped = SystemClock.elapsedRealtimeNanos();
            String action = intent.getAction();

            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("SHA-1");
            } catch (NoSuchAlgorithmException e) {
                Log.e(TAG, e.toString());
            }
            try {
                byte[] salt = getResources().getString(R.string.salt_v1).getBytes();
                md.update(salt);
            } catch (Exception er) {
                Log.e(TAG, er.toString());
            }

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                JSONObject devidebt = new JSONObject();
                try {
                    devidebt.put("value", md.digest(device.getAddress().getBytes(StandardCharsets.UTF_8)).toString());
                    devidebt.put("timestamp", RequestResponseUtils.getTimeStamps(now, elasped));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                events.add(devidebt);

            }
        }
    };

    @SneakyThrows
    private void respond() {
        Instant now = Instant.now();
        long elasped = SystemClock.elapsedRealtimeNanos();
        JSONObject tmp = new JSONObject();
        try {
            tmp.put("service_name", ServiceName);
            tmp.put("service_version", ServiceVersion);
            tmp.put("sensor_name", "BluetoothManager");
            tmp.put("sensor_type", -3);
            tmp.put("datasource", "mobile");
            tmp.put("timestamp", RequestResponseUtils.getTimeStamps(now, elasped));
            tmp.put("values", events);
            tmp.put("sensor_service_id", request.getInt("id"));
            tmp.put("service_type", request.getInt("service_type"));
            tmp.put("request_id", original_request.getInt("id"));
            tmp.put("request_type", original_request.getString("request_type"));
            tmp.put("event_id", original_request.getString("event_id"));

        } catch (Exception er) {

        }


        JSONArray array = new JSONArray();
        array.put(tmp);

        JSONObject packet = new JSONObject();
        try {
            packet.put("events", array);
            packet.put("request_type", "sensor_service");
            packet.put("session_id", original_request.getString("session_id"));
            getApplicationContext().unregisterReceiver(receiver);
        } catch (Exception er) {

        }

        stopSelf();

    }


    @Override
    public void onDestroy() {
        try {
            handler.removeCallbacks(runnable);
            getApplicationContext().unregisterReceiver(receiver);
        } catch (Exception er) {
        }

        stopForeground(false);
        super.onDestroy();
    }
}
