package de.dipf.edutec.edutex.androidclient.sensorservice;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.firebase.geofire.GeoFireUtils;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.Instant;
import java.util.ArrayList;

//import de.dipf.edutec.thriller.experiencesampling.messageservice.MessageLayerSender;
import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class GPSRecordingService extends Service {

    FusedLocationProviderClient mFusedLocationClient;
    double latitude = -1L;
    double longitude = -1L;
    int PRECISION = 6;

    static final String TAG = GPSRecordingService.class.getSimpleName();
    public String ServiceName = "GPS Recording Service";
    public double ServiceVersion = 1.0;

    Boolean isReplaced = false;
    JSONObject request;
    JSONObject original_request;
    ArrayList<JSONObject> events;
    OnCompleteListener<Location> onCompleteListener;

    @SneakyThrows
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication)getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(),fgNotificationCreator.getNotification());

        try{
            request = new JSONObject(intent.getStringExtra("request"));
            original_request = new JSONObject(intent.getStringExtra("original_request"));
            Log.d(TAG, original_request.getString("event_id"));
            events = new ArrayList<>();
        } catch (Exception er){

            stopSelf();
            return START_NOT_STICKY;
        }


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());

        getLastLocation(getApplicationContext());

        return START_NOT_STICKY;
    }

    // Get location and send it back to ESM
    @SuppressLint("MissingPermission")
    private void getLastLocation(Context context)
    {


        onCompleteListener = new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Instant now = Instant.now();
                long elapsed = SystemClock.elapsedRealtimeNanos();
                try{
                    // Get latitude and longitude
                    Location location = task.getResult();
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }

                    String hash = GeoFireUtils.getGeoHashForLocation(new GeoLocation(latitude,longitude), PRECISION);
                    JSONObject event = new JSONObject();
                    event.put("values", hash);
                    event.put("precision",PRECISION);
                    event.put("timestamp", RequestResponseUtils.getTimeStamps(now, elapsed));

                    events.add(event);

                    respond();
                }catch (Exception er){}

            }
        };
        // Get location
        mFusedLocationClient.getLastLocation().addOnCompleteListener(onCompleteListener);
    }

    // Send data back to ESM
    @SneakyThrows
    private void respond(){
        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();

        for(int i = 0; i < events.size(); i++){
            JSONObject tmp = new JSONObject();
            try{

                tmp.put("service_name",ServiceName);
                tmp.put("service_version",ServiceVersion);
                tmp.put("sensor_name","GPSManager");
                tmp.put("sensor_type",-4);
                tmp.put("datasource","mobile");
                tmp.put("timestamp", RequestResponseUtils.getTimeStamps(now, elapsed));
                tmp.put("values",events.get(i));
                tmp.put("sensor_service_id",request.getInt("id"));
                tmp.put("service_type",request.getInt("service_type"));
                tmp.put("request_id", original_request.getInt("id"));
                tmp.put("request_type", original_request.getString("request_type"));
                tmp.put("event_id", original_request.getString("event_id"));
                JSONArray array = new JSONArray();
                array.put(tmp);

                JSONObject packet = new JSONObject();
                packet.put("events",array);
                packet.put("request_type","sensor_service");
                packet.put("session_id",original_request.getString("session_id"));

                if(!isReplaced){
                    MessagesSingleton.getInstance().addMessageReceived(packet);
                }

            stopSelf();

        }catch (Exception er){}
    }


    }

    @Override
    public void onDestroy(){
        try{
            isReplaced = true;
        }catch (Exception er){}
        stopForeground(false);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
