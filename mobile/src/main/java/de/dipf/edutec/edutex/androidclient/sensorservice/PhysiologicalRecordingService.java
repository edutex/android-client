package de.dipf.edutec.edutex.androidclient.sensorservice;

import android.hardware.Sensor;

public class PhysiologicalRecordingService extends BaseSensorService {

    public static String TAG = PhysiologicalRecordingService.class.getSimpleName();
    public static String ServiceName = "Physiological Sensor Recording Service";
    public static double ServiceVersion = 1.0;


    public static int TYPE_SP02 = 69640;
    public static int TYPE_SDNN = 69641;

    private static int[] sensorList = new int[]{
            Sensor.TYPE_HEART_RATE,
            Sensor.TYPE_HEART_BEAT,
            TYPE_SP02,
            TYPE_SDNN,
    };

    public static int interval_packets = 10;
    public static int batch_size = 100;

    public PhysiologicalRecordingService() {
        super(sensorList, ServiceName, ServiceVersion,batch_size,  interval_packets, TAG, TransferOption.TIMER);
    }
}
