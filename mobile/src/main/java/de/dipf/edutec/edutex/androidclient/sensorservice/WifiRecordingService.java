package de.dipf.edutec.edutex.androidclient.sensorservice;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class WifiRecordingService extends Service {
    private static final String TAG = "WifiRecordingService";
    public String ServiceName = "Wifi Recording Service";
    public double ServiceVersion = 1.0;

    WifiManager wifiManager;
    JSONObject request;
    JSONObject original_request;
    ArrayList<JSONObject> events;

    Context myContext;
    boolean failure_called;

    MessagesSingleton messagesSingleton;

    @SneakyThrows
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(), fgNotificationCreator.getNotification());

        messagesSingleton = MessagesSingleton.getInstance();
        failure_called = false;

        try {
            request = new JSONObject(intent.getStringExtra("request"));
            original_request = new JSONObject(intent.getStringExtra("original_request"));
            events = new ArrayList<>();
        } catch (Exception er) {
            stopSelf();
            return START_NOT_STICKY;
        }

        myContext = getApplicationContext();
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        getApplicationContext().registerReceiver(wifiScanReceiver, intentFilter);

        if (!wifiManager.startScan()) {
            scanFailure();
        }

        return START_NOT_STICKY;
    }

    BroadcastReceiver wifiScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "In BC onReceive");
            boolean success = intent.getBooleanExtra(WifiManager.EXTRA_RESULTS_UPDATED, false);
            if (success) {
                scanSuccess();
            } else {
                scanFailure();
            }
        }
    };

    @SneakyThrows
    private void scanSuccess() {
        Log.d(TAG, "in scanSuccess");
        List<ScanResult> scanResults = wifiManager.getScanResults();
        accumulate(scanResults);
    }

    @SneakyThrows
    private void scanFailure() {
        Log.d(TAG, "in scanFailure");
        if (failure_called == false) {
            failure_called = true;
        } else {
            return;
        }
        List<ScanResult> scanResults = wifiManager.getScanResults();
        accumulate(scanResults);
    }

    @SneakyThrows
    public void accumulate(List<ScanResult> scanResults) {
        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, e.toString());
        }
        try {
            byte[] salt = getResources().getString(R.string.salt_v1).getBytes();
            md.update(salt);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        for (int i = 0; i < scanResults.size(); i++) {

            ScanResult scanResult = scanResults.get(i);
            String bssid = scanResult.BSSID;
            JSONObject event = new JSONObject();
            try {
                event.put("values", md.digest(bssid.getBytes(StandardCharsets.UTF_8)).toString());
                event.put("timestamp", RequestResponseUtils.getTimeStamps(now, elapsed));
            } catch (JSONException e) {
                Log.e(TAG, e.toString());
            }
            events.add(event);
        }

        for (int i = 0; i < this.events.size(); i++) {
            JSONObject tmp = new JSONObject();
            try {
                tmp.put("service_name", ServiceName);
                tmp.put("service_version", ServiceVersion);
                tmp.put("sensor_name", "WifiManager");
                tmp.put("sensor_type", -2);
                tmp.put("datasource", "mobile");
                tmp.put("timestamp", RequestResponseUtils.getTimeStamps(now, elapsed));
                tmp.put("values", events.get(i));
                tmp.put("sensor_service_id", request.getInt("id"));
                tmp.put("service_type", request.getInt("service_type"));
                tmp.put("request_id", original_request.getInt("id"));
                tmp.put("request_type", original_request.getString("request_type"));
                tmp.put("event_id", original_request.getString("event_id"));

                JSONArray array = new JSONArray();
                array.put(tmp);

                JSONObject packet = new JSONObject();
                packet.put("events", array);
                packet.put("request_type", "sensor_service");
                packet.put("session_id", original_request.getString("session_id"));

                MessagesSingleton.getInstance().addMessageReceived(packet);
            } catch (Exception er) {
                Log.e(TAG, er.toString());
            }
        }

        try {
            getApplicationContext().unregisterReceiver(wifiScanReceiver);
        } catch (Exception er) {
            Log.e(TAG, er.toString());
        }
        stopSelf();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        try {
            getApplicationContext().unregisterReceiver(wifiScanReceiver);

        } catch (Exception er) {
            Log.e(TAG, er.toString());
        }
        stopForeground(false);
        super.onDestroy();
    }


}
