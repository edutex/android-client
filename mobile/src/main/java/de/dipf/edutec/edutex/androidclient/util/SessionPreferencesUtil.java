package de.dipf.edutec.edutex.androidclient.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import de.dipf.edutec.edutex.androidclient.auth.AuthPreferences;
import lombok.SneakyThrows;

public class SessionPreferencesUtil {

    private static String TAG = SessionPreferencesUtil.class.getSimpleName();
    public static String PREFS_NAME = "SessionPreferenceUtil";

    public static final String KEY_PARTICIPANT_ID = "participant_id";
    public static final String KEY_STUDY_NAME = "study_name";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_SESSION_ID = "session_id";
    public static final String KEY_ANDROID_ID = "android_id";
    public static final String KEY_SESSION_RUNNING = "session_running";
    public static final String KEY_SESSION_REQUESTED_PRE = "session_pre";
    public static final String KEY_SESSION_REQUESTED_INTER = "session_inter";
    public static final String KEY_SESSION_REQUESTED_POST = "session_post";
    public static final String KEY_SESSION_TERMINATED = "session_terminated";
    public static final String KEY_RECOVERY_NECESSARY = "session_recovery_necessary";
    public static final String KEY_SP_STARTING_POINT = "sp_starting_point";
    public static final String KEY_RECEIVED_MSG = "session_received_msg";
    public static final String KEY_RECEIVED_RSP = "session_received_rsp";
    public static final String KEY_ERROR = "session_error";

    private SharedPreferences sharedPreferences;
    private Context context;

    public SessionPreferencesUtil(Context context){
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    // Setter Methods
    public void setParticipantId(String id){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_PARTICIPANT_ID, id);
        editor.commit();
    }

    public void setError(String error){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_ERROR, error);
        editor.commit();
        Log.d(TAG,"setError called");
    }
    public void setSessionId(String id){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_SESSION_ID,id);
        editor.commit();
    }

    public void setSPSessionTimer(long value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putLong(KEY_SP_STARTING_POINT,value);
        editor.commit();
    }
    public void setSessionRunning(Boolean value){
        Log.d(TAG, "Session running set to: " + String.valueOf(value));
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_RUNNING,value);
        editor.commit();
    }
    public void setSessionRequestedPre(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_REQUESTED_PRE,value);
        editor.commit();
    }
    public void setSessionRequestedInter(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_REQUESTED_INTER,value);
        editor.commit();
    }
    public void setSessionRequestedPost(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_REQUESTED_POST,value);
        editor.commit();
    }
    public void setSessionTerminated(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_TERMINATED,value);
        editor.commit();
    }
    public void setSessionReceivedMsg(int value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(KEY_RECEIVED_MSG,value);
        editor.commit();
    }
    public void setSessionReceivedRsp(int value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(KEY_RECEIVED_RSP,value);
        editor.commit();
    }
    public void setSessionRecoveryNecessary(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_RECOVERY_NECESSARY,value);
        editor.commit();
    }
    public void setAndroidId(){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        try{
            editor.putString(KEY_ANDROID_ID, Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID));

        }catch (Exception er){
            Log.e(TAG, er.toString());
        }
        editor.commit();

    }

    public void setStudyName(String name){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_STUDY_NAME, name);
        editor.commit();
    }
    public String getStudyName(){return this.sharedPreferences.getString(KEY_STUDY_NAME, "");}


    public void setUsername(String name){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_USERNAME,name);
        editor.commit();
    }
    public String getUsername(){return this.sharedPreferences.getString(KEY_USERNAME,"");}


    @SneakyThrows
    public void setSessionObject(JSONObject obj){
        setSessionRunning(true);
        setSessionRequestedPre(obj.getBoolean("requested_pre"));
        setSessionRequestedInter(obj.getBoolean("requested_inter"));
        setSessionRequestedPost(obj.getBoolean("requested_post"));
        setSessionTerminated(obj.getBoolean("terminated"));
        setSessionId(obj.getString("session_id"));
        setAndroidId();
    }

    // Getter Methods
    public Boolean getSessionRequestedPre(){
        return sharedPreferences.getBoolean(KEY_SESSION_REQUESTED_PRE,false);
    }
    public Boolean getSessionRequestedInter(){
        return sharedPreferences.getBoolean(KEY_SESSION_REQUESTED_INTER,false);
    }
    public Boolean getSessionRequestedPost(){
        return sharedPreferences.getBoolean(KEY_SESSION_REQUESTED_POST,false);
    }
    public Boolean getIsSessionTerminated(){
        return sharedPreferences.getBoolean(KEY_SESSION_TERMINATED,false);
    }
    public Boolean getSessionRecovery(){return sharedPreferences.getBoolean(KEY_RECOVERY_NECESSARY,false);}
    public String getParticipantId(){
        return sharedPreferences.getString(KEY_PARTICIPANT_ID,"");
    }
    public String getSessionId(){
        return sharedPreferences.getString(KEY_SESSION_ID,"");
    }
    public Boolean getSessionRunning(){return sharedPreferences.getBoolean(KEY_SESSION_RUNNING,false);}
    public int getUserId(){
        AuthPreferences authPreferences = new AuthPreferences(context);
        return authPreferences.getUserID();
    }
    public String getAndroidId(){return sharedPreferences.getString(KEY_ANDROID_ID,"");}

    // Utility Methods
    public List<String> getObservationList(){
        return Arrays.asList(
                KEY_SESSION_ID,
                KEY_SESSION_RUNNING,
                KEY_SESSION_REQUESTED_PRE,
                KEY_SESSION_REQUESTED_INTER,
                KEY_SESSION_REQUESTED_POST,
                KEY_SESSION_TERMINATED
        );
    }
    public static String getPrefsName(){
        return PREFS_NAME;
    }

    public JSONObject getSessionInformation(){

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(context);

        JSONObject response = new JSONObject();
        try {
            response.put("recovery_possible",getSessionRecovery());
            response.put("session_running",getSessionRunning());
            response.put("requested_pre",getSessionRequestedPre());
            response.put("requested_inter",getSessionRequestedInter());
            response.put("requested_post",getSessionRequestedPost());
            response.put("terminated",getIsSessionTerminated());
            response.put("esm_connected",esmConfigurationPreferences.getConnectionStatus());
            response.put("user_id",getParticipantId());
            response.put("session_id",getSessionId());
            response.put("sessionID",getSessionId());
            response.put("studymechanics",esmConfigurationPreferences.getStudyStartMechanics());
            response.put("used_device",esmConfigurationPreferences.getPrefDevice());
            return response;
        }catch (Exception er){
            er.printStackTrace();
            return null;
        }

    }

    public void incrementReceivedMsg(){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        int current = sharedPreferences.getInt(KEY_RECEIVED_MSG,0);
        current += 1;
        editor.putInt(KEY_RECEIVED_MSG,current);
        editor.apply();
    }
    public void incrementReceivedRsp(){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        int current = sharedPreferences.getInt(KEY_RECEIVED_RSP,0);
        current += 1;
        editor.putInt(KEY_RECEIVED_RSP,current);
        editor.apply();
    }

    public void cleanUpSessionPreferences(){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_RECOVERY_NECESSARY,false);
        editor.putBoolean(KEY_SESSION_REQUESTED_PRE, false);
        editor.putBoolean(KEY_SESSION_REQUESTED_INTER,false);
        editor.putBoolean(KEY_SESSION_REQUESTED_POST,false);
        editor.putBoolean(KEY_SESSION_RUNNING,false);
        editor.putInt(KEY_RECEIVED_MSG,0);
        editor.putInt(KEY_RECEIVED_RSP,0);
        editor.apply();


    }

    public Long getSPSessionTimer(){return this.sharedPreferences.getLong(KEY_SP_STARTING_POINT,-1);}


}
