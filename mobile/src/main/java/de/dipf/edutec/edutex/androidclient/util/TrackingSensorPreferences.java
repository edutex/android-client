package de.dipf.edutec.edutex.androidclient.util;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONObject;

import lombok.SneakyThrows;

public class TrackingSensorPreferences {

    public static String PREFS_NAME = "TrackingSensorPreferences";
    private SharedPreferences sharedPreferences;
    private Context context;

    private static final String KEY_TRACK_APP_USAGE = "track_app_usage";
    private static final String KEY_TRACK_WINDOW_CHANGE = "track_window_change";
    private static final String KEY_TRACK_NOTIFCATION_CHANNEL = "track_notification_channel";
    private static final String KEY_REQUEST = "track_request";
    private static final String KEY_ORIGINAL_REQUEST = "track_original_request";


    public TrackingSensorPreferences(Context context){
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    // Setter Functions
    public void setTrackAppUsage(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_TRACK_APP_USAGE,value);
        editor.apply();
    }
    public void setTrackWindowChangeDetection(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_TRACK_WINDOW_CHANGE,value);
        editor.apply();
    }
    public void setTrackNotificationChannel(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_TRACK_NOTIFCATION_CHANNEL,value);
        editor.apply();
    }
    public void setTrackRequest(JSONObject service){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_REQUEST,service.toString());
        editor.apply();
    }
    public void setTrackOriginalRequest(JSONObject original){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_ORIGINAL_REQUEST,original.toString());
        editor.apply();
    }
    // Getter Functions
    public Boolean getTrackAppUsage(){return sharedPreferences.getBoolean(KEY_TRACK_APP_USAGE,false);}
    public Boolean getTrackWindowChange(){return sharedPreferences.getBoolean(KEY_TRACK_WINDOW_CHANGE,false);}
    public Boolean getTrackNotificationChannel(){return sharedPreferences.getBoolean(KEY_TRACK_NOTIFCATION_CHANNEL,false);}
    @SneakyThrows
    public JSONObject getTrackOriginalRequest(){
        String request = sharedPreferences.getString(KEY_ORIGINAL_REQUEST, "");
        return new JSONObject(request);
    }
    @SneakyThrows
    public JSONObject getTrackRequest(){
        String request = sharedPreferences.getString(KEY_REQUEST, "");
        return new JSONObject(request);
    }

    // Utility Functions
    public void setAllTrue(){
        setTrackAppUsage(true);
        setTrackWindowChangeDetection(true);
        setTrackNotificationChannel(true);
    }
    public void setAllFalse(){
        setTrackWindowChangeDetection(false);
        setTrackAppUsage(false);
        setTrackNotificationChannel(false);
        setTrackRequest(new JSONObject());
        setTrackOriginalRequest(new JSONObject());
    }





}
