package de.dipf.edutec.edutex.androidclient.sharedUtils;

public class NotificationPrefixStatics {

    public static int prefix_questionnaire = 0;
    public static int prefix_micro_questionnaire = 1;
    public static int prefix_notification = 2;
    public static int prefix_sensor_request = 3;

}
