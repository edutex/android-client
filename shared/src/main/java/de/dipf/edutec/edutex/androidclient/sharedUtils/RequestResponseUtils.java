package de.dipf.edutec.edutex.androidclient.sharedUtils;

import android.util.Log;

import org.json.JSONObject;

import java.time.Instant;

public class RequestResponseUtils {
    private static final String TAG = RequestResponseUtils.class.getSimpleName();

    public static JSONObject getTimeStamps(Instant now, long elapsed) {
        JSONObject response = new JSONObject();
        try {
            response.put("elapsed_realtime_nano", elapsed);
            response.put("unix_timestamp_milli", now.toEpochMilli());

        } catch (Exception er) {
            Log.e(TAG, er.toString());
        }
        return response;
    }

}
