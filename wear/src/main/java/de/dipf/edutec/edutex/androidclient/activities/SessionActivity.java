package de.dipf.edutec.edutex.androidclient.activities;

import static java.lang.StrictMath.abs;
import static java.lang.StrictMath.min;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.messageservice.MessageLayerSender;
import de.dipf.edutec.edutex.androidclient.messageservice.OffBodyRecordingService;
import de.dipf.edutec.edutex.androidclient.microsurvey.MicroQuestionnairePreference;
import de.dipf.edutec.edutex.androidclient.session.SessionFragmentGeneral;
import de.dipf.edutec.edutex.androidclient.session.SessionFragmentMain;
import de.dipf.edutec.edutex.androidclient.session.SessionFragmentSensors;
import de.dipf.edutec.edutex.androidclient.utils.ActionUtils;
import de.dipf.edutec.edutex.androidclient.utils.ServiceRunningUtil;
import de.dipf.edutec.edutex.androidclient.utils.SessionPreferences;
import de.dipf.edutec.edutex.androidclient.utils.TrackingSensorPreferences;
import lombok.SneakyThrows;

public class SessionActivity extends FragmentActivity {

    static String TAG = "SessionActivity";

    // Flow Control
    FragmentManager fragmentManager;
    int current_fragment_index;
    SessionFragmentSensors sessionFragmentSensors;
    SessionFragmentMain sessionFragmentMain;

    // Gesture Detection
    GestureDetector mDetector;
    private long lastGestureEvent;

    // STOPWATCH
    Handler handler;
    TextView sw_text;
    long start_value;

    // Exceptionhandling;
    int retryOnStopSessionException = 25;
    int currentTry = 0;
    private ArrayList<BroadcastReceiver> broadcastReceivers;


    BroadcastReceiver receiver_micro_survey = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(microSurveyAvailalbe()){
                finishAffinity();
            }
            Log.d(TAG, "Receiver Micro Survey in SessionActivity is called");
        }
    };


    BroadcastReceiver receiver_session_duration = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra("remote_start_timer")) {
                if(intent.getBooleanExtra("remote_start_timer",false)){
                    start_value = -1L;
                    runTimer();
                }
            } else if(intent.hasExtra("remote_stop_timer")){
                if(intent.getBooleanExtra("remote_stop_timer",false)){
                    handler.postDelayed(service_closing,1000);
                    sessionFragmentMain.press_long_text.setText("Closing services.");
                }
            } else if(intent.hasExtra("received_onError")){
                finish();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));

            }
        }
    };





    Runnable runnable_monitor_time = new Runnable() {
        @Override

        public void run()
        {
            if(start_value == -1L){
                Log.d(TAG, "Start-Value is -1. Returning");
                return;
            }

            long current = (System.currentTimeMillis() / 1000) - start_value;
            long hours = current / 3600;
            long minutes = (current % 3600) / 60;
            long secs = current % 60;

            String time
                    = String
                    .format(Locale.getDefault(),
                            "%d:%02d:%02d", hours,
                            minutes, secs);

            if(sw_text == null){
                sw_text = findViewById(R.id.session_stopwatch);
            }
            sw_text.setText(time);
            handler.postDelayed(this, 1000);
        }
    };



    Runnable service_closing = new Runnable() {
        @Override
        public void run() {
            if(ServiceRunningUtil.areServiceClosed(getApplicationContext())){
                finish();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));

            }else{
                ServiceRunningUtil.terminate_services(getApplicationContext());
                Toast.makeText(getApplicationContext(),"Closing services", Toast.LENGTH_SHORT).show();
                handler.postDelayed(service_closing,5000);
            }

        }
    };


    @Override
    protected void onResume(){
        super.onResume();
        Log.d(TAG, "onResume called");

        IntentFilter filterMicroSurveyInterrupt = new IntentFilter(getString(R.string.ACTION_MICRO_SURVEY_INTERRUPT));
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver_micro_survey,filterMicroSurveyInterrupt);

        IntentFilter filterRemoteTimer = new IntentFilter(ActionUtils.ACTION_REMOTE_TIMER);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver_session_duration,filterRemoteTimer);

        if(microSurveyAvailalbe()){finishAffinity();}

        SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
        if(sessionPreferences.getSWSessionTimer() != -1L){
            runTimer();
        }
    }

    @Override
    public void onPause() {
        SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
        sessionPreferences.setSWSessionTimer(start_value);
        Log.d(TAG, "onPause: " + String.valueOf(start_value));
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy(){

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver_micro_survey);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver_session_duration);

        SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
        sessionPreferences.setSWSessionTimer(start_value);
        Log.d(TAG, "onPause: " + String.valueOf(start_value));
        handler.removeCallbacksAndMessages(null);
        unregisterAllBroadcastReceivers();

        super.onDestroy();
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);

        broadcastReceivers = new ArrayList<>();

        SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
        start_value = sessionPreferences.getSWSessionTimer();

        handler = new Handler();
        sw_text = findViewById(R.id.session_stopwatch);

        if(!Boolean.valueOf(getString(R.string.DEBUG_MODE))){
            setGestureDetector(this);
            TextView txt =  findViewById(R.id.txt_swipe_up_and_down);
            txt.setVisibility(View.VISIBLE);
        }

        //sessionFragmentSensors = new SessionFragmentSensors();
        sessionFragmentMain = new SessionFragmentMain();

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fl_session_main, sessionFragmentMain).commit();
        current_fragment_index = 0;


        if(microSurveyAvailalbe()){
            finishAffinity();
        }


    }


    private Boolean microSurveyAvailalbe(){
        MicroQuestionnairePreference microQuestionnairePreference = new MicroQuestionnairePreference(getApplicationContext());
        if(microQuestionnairePreference.isMicroQuestionnaireAvailable()){
            Intent intent = new Intent(this, MicroInteractionSurveyActivity.class);
            startActivity(intent);
            return true;
        } else {
            return false;
        }
    }




    public void initStopSession(){

        // Requestion the Post Session Survey
        MessageLayerSender messageLayerSender = new MessageLayerSender(this);
        String deliveryUUID = UUID.randomUUID().toString();

        sessionFragmentMain.spinner.setVisibility(View.VISIBLE);
        sessionFragmentMain.stopSession.setVisibility(View.GONE);
        sessionFragmentMain.press_long_text.setText("Waiting for PostQuest");

        Runnable runnableStopSession = new Runnable() {
            @Override
            public void run() {
                currentTry += 1;
                if(retryOnStopSessionException == currentTry){

                    SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
                    sessionPreferences.sessionInRecovery(true);
                    handler.removeCallbacksAndMessages(null);
                    finish();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    return;

                }else {
                    if(currentTry > 1){
                        Toast.makeText(getApplicationContext(),"Pending.. " +
                                String.valueOf(currentTry) + "/" +
                                String.valueOf(retryOnStopSessionException),
                                Toast.LENGTH_SHORT).show();
                    }
                    messageLayerSender.sendStopSession(deliveryUUID);
                    handler.postDelayed(this, 5000);
                }

            }
        };

        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getStringExtra("ping").equals(deliveryUUID)){
                    handler.removeCallbacks(runnableStopSession);
                    handler.removeCallbacksAndMessages(null);
                    stopSession();

                }
            }
        };
        registerBroadcastReceiverForDelivery(broadcastReceiver);

        handler.postDelayed(runnableStopSession,300);

    }



    public void stopSession(){

        SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
        ServiceRunningUtil.terminate_services(getApplicationContext());

        TrackingSensorPreferences trackingSensorPreferences = new TrackingSensorPreferences(getApplicationContext());
        trackingSensorPreferences.setAllFalse();

        // Cleaning Notification and Survey Data
        NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();

        MicroQuestionnairePreference microQuestionnairePreference = new MicroQuestionnairePreference(getApplicationContext());
        microQuestionnairePreference.clear();



        // Reseting the Timer
        handler.removeCallbacks(runnable_monitor_time);
        start_value = -1L;
        sessionPreferences.setSWSessionTimer(-1L);
        sessionPreferences.setSessionRunning(false);

        Log.d(TAG, "Session is over. Returning to MainActivity.");

        handler.postDelayed(service_closing,100);

    }



    public void setGestureDetector(Context context){

        mDetector = new GestureDetector(context,  new GestureDetector.SimpleOnGestureListener() {

            @SneakyThrows
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                                    float distanceY) {

                if(System.currentTimeMillis() - lastGestureEvent > 500L) {
                    lastGestureEvent = System.currentTimeMillis();
                    if (abs(distanceX - distanceY) > 0) {

                        if (distanceY < -10) {
                            if(current_fragment_index > 0){
                                current_fragment_index -= 1;
                                if(current_fragment_index == 0){
                                    fragmentManager.beginTransaction().replace(R.id.fl_session_main,sessionFragmentMain).commit();
                                } else if (current_fragment_index == 1){
                                    fragmentManager.beginTransaction().replace(R.id.fl_session_main, new SessionFragmentGeneral()).commit();
                                } else {
                                    sessionFragmentSensors.switchToLastSensor();
                                }
                            }
                        }

                        if (distanceY > 10) {

                            current_fragment_index += 1;
                            if(current_fragment_index == 1){
                                fragmentManager.beginTransaction().replace(R.id.fl_session_main, new SessionFragmentGeneral()).commit();
                            } else if (current_fragment_index == 2){
                                fragmentManager.beginTransaction().replace(R.id.fl_session_main, sessionFragmentSensors).commit();
                            } else {
                                if(!sessionFragmentSensors.switchToNextSensor()){
                                    current_fragment_index -= 1;
                                }
                            }
                        }
                    }
                }
                return true;
            }


        });
    }

    // Capture long presses

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mDetector != null) {
            return mDetector.onTouchEvent(ev) || super.dispatchTouchEvent(ev);
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }



    private void runTimer()
    {
        sessionFragmentMain.spinner.setVisibility(View.GONE);


        Log.d(TAG, "runTimer called");

        SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
        if(sessionPreferences.getStudyStartMechanics() == 1){
            sessionFragmentMain.stopSession.setVisibility(View.VISIBLE);
        }


        long starting_point = sessionPreferences.getSWSessionTimer();
        Log.d(TAG, "SessionPreferenceValueSW: " + String.valueOf(starting_point));

        if (starting_point == -1L){
            Log.d(TAG, "Starting Point was -1.");
            starting_point = System.currentTimeMillis() / 1000;
            sessionPreferences.setSWSessionTimer(starting_point);
        }

        start_value = starting_point;
        Log.d(TAG, "Current value: " + String.valueOf(start_value));
        handler.postDelayed(runnable_monitor_time,500);
    }



    public void registerBroadcastReceiverForDelivery(BroadcastReceiver receiver){
        IntentFilter ping_filter = new IntentFilter("PING_FILTER");
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
        manager.registerReceiver(receiver, ping_filter);
        broadcastReceivers.add(receiver);
    }

    public void unregisterAllBroadcastReceivers(){
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        for(BroadcastReceiver receiver: broadcastReceivers){
            try{
                manager.unregisterReceiver(receiver);
            }catch (Exception er){
                Log.e(TAG,"BroadcastReceiver could not be unregistered.");
            }
        }
    }


}