package de.dipf.edutec.edutex.androidclient.activities;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.settings.SettingsFragmentMain;
import de.dipf.edutec.edutex.androidclient.settings.SettingsFragmentSensorDelay;
import de.dipf.edutec.edutex.androidclient.settings.SettingsFragmentSensors;

public class SettingsActivity extends FragmentActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fl_settings_activity,new SettingsFragmentMain()).commit();

    }

    public void changeFragment(String className){
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (className){
            case "SettingsFragmentsSensors":
                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                        .replace(R.id.fl_settings_activity, new SettingsFragmentSensors())
                        .commit();
                break;
            case "SettingsFragmentMain":
                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.fl_settings_activity, new SettingsFragmentMain())
                        .commit();
                break;
            case "SettingsFragmentsSensorDelay":
                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations( R.anim.slide_in_right, R.anim.slide_out_left)
                        .replace(R.id.fl_settings_activity, new SettingsFragmentSensorDelay())
                        .commit();
                break;
            default:
                break;
        }
    }
}