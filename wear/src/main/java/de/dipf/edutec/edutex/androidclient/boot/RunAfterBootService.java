package de.dipf.edutec.edutex.androidclient.boot;

import static com.google.android.gms.wearable.Wearable.getCapabilityClient;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.wearable.CapabilityClient;

import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.conf.ReceiverManager;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;

public class RunAfterBootService extends Service {


    private static final String TAG = RunAfterBootService.class.getSimpleName();
    Handler handler;
    Runnable checkSessionRunning = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(this,3000);
        }
    };



    public RunAfterBootService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "RunAfterBootService onCreate() method.");
        ReceiverManager.getInstance().registerSessionReceiver(getApplicationContext());
        ReceiverManager.getInstance().registerCapabilityClient(getApplicationContext());

    }




    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(), fgNotificationCreator.getNotification());
        Log.d(TAG, "RunAfterBootService onStartCommand() method.");
        return super.onStartCommand(intent, flags, startId);
    }




    @Override
    public void onDestroy() {
        ReceiverManager.getInstance().unregisterSessionReceiver(getApplicationContext());
        super.onDestroy();
    }
}