package de.dipf.edutec.edutex.androidclient.conf;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.preference.PreferenceManager;

import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.Set;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.microsurvey.MicroQuestionnairePreference;
import de.dipf.edutec.edutex.androidclient.notifications.NotificationPublisher;
import de.dipf.edutec.edutex.androidclient.sensorservice.SensorDataService;
import de.dipf.edutec.edutex.androidclient.survey.QuestionnaireNotificationPublisher;
import de.dipf.edutec.edutex.androidclient.microsurvey.MicroSurveyNotificationPublisher;
import de.dipf.edutec.edutex.androidclient.utils.SessionPreferences;

public class ReceiverManager {

    // brauche ich

    private static String TAG = "ReceiverManager";

    public static ReceiverManager instance;
    private Boolean receiverRegistered;
    private Boolean capabilityClientRegistered;
    CapabilityClient capabilityClient;
    CapabilityClient.OnCapabilityChangedListener listener;
    QuestionnaireNotificationPublisher surveyNotificationPublisher;
    MicroSurveyNotificationPublisher microSurveyNotificationPublisher;
    NotificationPublisher notificationPublisher;

    public ReceiverManager(){
        this.capabilityClientRegistered = false;

    }

    public static ReceiverManager getInstance(){
        if(ReceiverManager.instance == null){
            ReceiverManager.instance = new ReceiverManager();
        }
        return ReceiverManager.instance;
    }

    public void registerSessionReceiver(Context context){

        try{


            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(context);

            IntentFilter filterMicroSurvey = new IntentFilter(context.getString(R.string.ACTION_MICRO_SURVEY));
            microSurveyNotificationPublisher = new MicroSurveyNotificationPublisher();


            IntentFilter filterSurvey = new IntentFilter(context.getString(R.string.ACTION_SURVEY));
            surveyNotificationPublisher = new QuestionnaireNotificationPublisher();

            IntentFilter filterNotify = new IntentFilter(context.getString(R.string.ACTION_NOTIFY));
            notificationPublisher = new NotificationPublisher();

            manager.registerReceiver(surveyNotificationPublisher,filterSurvey);
            manager.registerReceiver(microSurveyNotificationPublisher,filterMicroSurvey);
            manager.registerReceiver(notificationPublisher,filterNotify);


            receiverRegistered = true;
            Log.d(TAG, "Receiver are registered");

        }catch (Exception er){
            Log.e(TAG,er.getMessage());
        }

    }


    public void registerCapabilityClient(Context context){

        try{
            if(! this.capabilityClientRegistered){
                capabilityClient = Wearable.getCapabilityClient(context);
                listener = new CapabilityClient.OnCapabilityChangedListener() {
                    @Override
                    public void onCapabilityChanged(@NonNull CapabilityInfo capabilityInfo) {

                        // Notify User about connection status
                        Set<Node> nodes = capabilityInfo.getNodes();
                        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        if(nodes.size() == 0){

                            // Make Session-Running Statement for the whole application
                            SessionPreferences sessionPreferences = new SessionPreferences(context.getApplicationContext());
                            boolean session_running_at_call = sessionPreferences.isSessionRunning();

                            String CHANNEL_ID = context.getResources().getString(R.string.notiChannelID);

                            if (session_running_at_call == true){

                                Notification notification = new NotificationCompat.Builder(context,CHANNEL_ID)
                                        .setContentText("LOST CONNECTION TO PHONE. Find your phone and open app please.")
                                        .setSmallIcon(R.drawable.ic_survey)
                                        .build();

                                manager.notify(0101010,notification);
                            }


                        } else {
                            manager.cancel(0101010);
                        }

                        Log.d(TAG, capabilityInfo.getName());
                        Log.d(TAG, capabilityInfo.getNodes().toString());
                    }
                };

                capabilityClient.addListener(listener,"bluetooth-status-mobile");
            }
        }catch (Exception er){
            Log.e(TAG,er.getMessage());
        }
    }


    public void unregisterSessionReceiver(Context context){

        try{
            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(context);
            manager.unregisterReceiver(surveyNotificationPublisher);
            manager.unregisterReceiver(microSurveyNotificationPublisher);
            manager.unregisterReceiver(notificationPublisher);
            capabilityClient.removeListener(listener);
            Log.d(TAG,"Unregistered");
        }catch (Exception er){
            Log.e(TAG,er.getMessage());
        }

    }




}
