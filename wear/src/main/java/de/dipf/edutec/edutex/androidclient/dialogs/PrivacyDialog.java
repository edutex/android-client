package de.dipf.edutec.edutex.androidclient.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.utils.SessionPreferences;


public class PrivacyDialog {

    private Dialog cDialog;
    private Context cContext;
    private Button accept;
    private TextView tvPrivacyText;
    private Activity activity;

    public PrivacyDialog(Context context, Activity activity){
        this.cContext =context;
        this.activity = activity;
    }

    public void showDialog(){

        cDialog = new Dialog(cContext);
        cDialog.setContentView(R.layout.dialog_privacy);
        cDialog.setCancelable(false);
        cDialog.setCanceledOnTouchOutside(false);
        cDialog.show();

        findGUIElements();
    }



    private void findGUIElements(){

       tvPrivacyText = cDialog.findViewById(R.id.dialog_priv_reference);
       accept = cDialog.findViewById(R.id.button_priv_continue);
       String text = "The privacy policy can be viewed, accepted or rejected in the mobile application.";

       tvPrivacyText.setText(text);
       accept.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               SessionPreferences sessionPreferences = new SessionPreferences(activity.getApplicationContext());
               sessionPreferences.setPrivacyNotificationDisplayed(true);
               cDialog.dismiss();
           }
       });

    }


    public void hideProgress(){
        if(cDialog != null){
            cDialog.dismiss();
            cDialog = null;
        }
    }

}

