package de.dipf.edutec.edutex.androidclient.messageservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.Node;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import de.dipf.edutec.edutex.androidclient.R;

import static com.google.android.gms.wearable.Wearable.DataApi;
import static com.google.android.gms.wearable.Wearable.getCapabilityClient;
import static com.google.android.gms.wearable.Wearable.getMessageClient;
import static com.google.android.gms.wearable.Wearable.getNodeClient;

import org.json.JSONException;
import org.json.JSONObject;

public class MessageLayerSender {

    Context context;
    protected Handler myHandler;
    private String TAG = MessageLayerSender.class.getSimpleName();

    public MessageLayerSender(Context context){
        this.context = context;

        myHandler = new android.os.Handler(new android.os.Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                Bundle stuff = msg.getData();
                return true;
            }
        });
    }


    public void sendAck(String path, String message){
        new NewThread(path,message).start();
    }


    public String sendAck(String path, String message, Boolean waitForDelivery){
        String deliveryUUID = UUID.randomUUID().toString();
        try{
            JSONObject object = new JSONObject(message);

            object.put("deliveryUUID",deliveryUUID);
            new NewThread(path,object.toString()).start();
        }catch (Exception er){
            JSONObject object = new JSONObject();
            try {
                object.put("deliveryUUID",deliveryUUID);
                new NewThread(path,object.toString()).start();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



        return deliveryUUID;
    }





    public String sendStartSession(){

        String deliveryUUID = UUID.randomUUID().toString();
        JSONObject object = new JSONObject();

        try {
            object.put("deliveryUUID",deliveryUUID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new NewThread(context.getString(R.string.PATH_TO_HANDHELD_ESM_SERVICE_START), object.toString()).start();
        return deliveryUUID;
    }

    public void sendStopSession(String uuid){

        JSONObject object = new JSONObject();

        try {
            object.put("deliveryUUID",uuid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

         new NewThread(context.getString(R.string.PATH_TO_HANDHELD_ESM_SERVICE_STOP),object.toString()).start();

    }


    public void sendForceStop(){
        new NewThread("/tohandheld/esm_service_force_stop","inactive_user").start();
    }

    public void sendCrashNotification(){
        new  NewThread("/tohandheld/smartwatch_crashed","").start();
    }

    class NewThread extends Thread {

        String path;
        String message;



        NewThread(String path, String message){
            this.path = path;
            this.message = message;

        }




        public void run(){

            try {
                Task<List<Node>> nodeListTask =
                        getNodeClient(context).getConnectedNodes();

                /* Block on a task and get the result synchronously */
                List<Node> nodes = Tasks.await(nodeListTask);
                for (Node node : nodes) {
                    Task<Integer> sendMessageTask =
                            getMessageClient(context).sendMessage(node.getId(), path, message.getBytes());

                    try {
                        Integer result = Tasks.await(sendMessageTask);
                        Log.v(TAG, "SendThread: message send to " + node.getDisplayName() +" "+  String.valueOf(result));
                    } catch (ExecutionException exception) {
                        Log.e(TAG, "Task failed: " + exception);
                    } catch (InterruptedException exception) {
                        Log.e(TAG, "Interrupt occurred: " + exception);
                    }
                }

            } catch (Exception er){
                Log.e(TAG,er.toString());
            }
        }

    }

}



/***
 sendMessageTask.addOnSuccessListener(new OnSuccessListener<Integer>() {
@Override
public void onSuccess(Integer integer) {
Log.d(TAG, "Sended to handheld successfully. Path: " + path);
success = true;
}
});

 sendMessageTask.addOnFailureListener(new OnFailureListener() {
@Override
public void onFailure(@NonNull Exception e) {
Log.d(TAG, "Sended to handheld not successfully. Path: " + path);
Toast.makeText(context,"onFailureSend",Toast.LENGTH_LONG).show();
success = false;
}
});
 **/