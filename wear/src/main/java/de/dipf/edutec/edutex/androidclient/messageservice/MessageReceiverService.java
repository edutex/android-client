
package de.dipf.edutec.edutex.androidclient.messageservice;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import org.acra.ACRA;
import org.acra.BuildConfig;
import org.acra.config.CoreConfigurationBuilder;
import org.acra.config.HttpSenderConfigurationBuilder;
import org.acra.config.LimiterConfigurationBuilder;
import org.acra.config.NotificationConfigurationBuilder;
import org.acra.data.StringFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.RecoveryActivity;
import de.dipf.edutec.edutex.androidclient.activities.SessionActivity;
import de.dipf.edutec.edutex.androidclient.activities.SurveyActivity;
import de.dipf.edutec.edutex.androidclient.microsurvey.MicroQuestionnairePreference;
import de.dipf.edutec.edutex.androidclient.processtracking.ProcessTrackingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.SensorDataService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.AmbientNoiseRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.BehavioralRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.PhysicalRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.PhysiologicalRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.VirtualSensorService;
import de.dipf.edutec.edutex.androidclient.utils.ActionUtils;
import de.dipf.edutec.edutex.androidclient.utils.ServiceRunningUtil;
import de.dipf.edutec.edutex.androidclient.utils.SessionPreferences;
import de.dipf.edutec.edutex.androidclient.utils.TrackingSensorPreferences;
import de.dipf.edutec.edutex.androidclient.sharedUtils.MessageLayerStatics;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class MessageReceiverService extends WearableListenerService {

    static final String TAG = MessageReceiverService.class.getSimpleName();
    static final String errorServiceAlreadyRunning = "Seems like the service is already running. Trying post delayed.";
    static final int delayPostStartService = 15000; //ms
    static final int delayTries = 3;
    Handler postdelayHandler = new Handler();


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();


        SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
        String received_path = messageEvent.getPath();
        String message = new String(messageEvent.getData());
        Intent intent;
        Context context = getApplicationContext();


        Log.d(TAG, received_path);

        switch (received_path){
            case MessageLayerStatics.TOSMARTWATCH_QUESTIONNAIRE:

                intent = new Intent();
                intent.setAction(getString(R.string.ACTION_SURVEY));
                intent.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                Log.d(TAG,"Survey broadcasted");
                break;

            case MessageLayerStatics.TOSMARTWATCH_PRE_POST_QUESTIONNAIRE:

                try {

                    JSONObject object = new JSONObject(message);

                    if(object.has("faked")){

                        if(object.getString("faked").equals("pre_survey")){

                            Intent intent1 = new Intent(context.getApplicationContext(), VibrationService.class);
                            context.getApplicationContext().startForegroundService(intent1);

                            SessionPreferences sessionPreferences_tmp = new SessionPreferences(getApplicationContext());
                            sessionPreferences_tmp.setSessionRunning(true);
                            object.put("android_smartwatch_timestamp_start", RequestResponseUtils.getTimeStamps(Instant.now(),SystemClock.elapsedRealtimeNanos()));
                            MessageLayerSender messageLayerSender = new MessageLayerSender(this);
                            messageLayerSender.sendAck(getString(R.string.PATH_TO_HANDHELD_RESPONSE_SURVEY), object.toString());
                            sessionPreferences_tmp.setSWSessionTimer(System.currentTimeMillis() / 1000);

                        } else if (object.getString("faked").equals("post_survey")){

                            Intent intent1 = new Intent(context.getApplicationContext(), VibrationService.class);
                            context.getApplicationContext().startForegroundService(intent1);

                            object.put("android_smartwatch_timestamp_end", RequestResponseUtils.getTimeStamps(Instant.now(),SystemClock.elapsedRealtimeNanos()));
                            MessageLayerSender messageLayerSender = new MessageLayerSender(this);
                            messageLayerSender.sendAck(getString(R.string.PATH_TO_HANDHELD_RESPONSE_SURVEY), object.toString());
                            ServiceRunningUtil.terminate_services(getApplicationContext());
                            TrackingSensorPreferences trackingSensorPreferences = new TrackingSensorPreferences(getApplicationContext());
                            trackingSensorPreferences.setAllFalse();
                            NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                            manager.cancelAll();
                            MicroQuestionnairePreference microQuestionnairePreference = new MicroQuestionnairePreference(getApplicationContext());
                            microQuestionnairePreference.clear();
                            sessionPreferences.setSWSessionTimer(-1L);
                            sessionPreferences.setSessionRunning(false);
                        }

                    } 

                    int esm_msg_hash = message.hashCode();
                    //object.put("timestamp_question_asked", NotificationSingleton.getInstance().checkHash(esm_msg_hash));
                    object.put("timestamp_question_asked", RequestResponseUtils.getTimeStamps(now, elapsed));
                    object.put("identifier", esm_msg_hash);
                    intent = new Intent(this, SurveyActivity.class);
                    intent.putExtra("message", object.toString());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case MessageLayerStatics.TOSMARTWATCH_MICRO_SURVEY:
                intent = new Intent();
                intent.setAction(getString(R.string.ACTION_MICRO_SURVEY));
                intent.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                Log.d("0000", "Called Action: " + getString(R.string.ACTION_MICRO_SURVEY));
                break;

            case MessageLayerStatics.TOSMARTWATCH_NOTIFICATION:
                intent = new Intent();
                intent.setAction(getString(R.string.ACTION_NOTIFY));
                intent.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                Log.d(TAG, "Called Action: " + getString(R.string.ACTION_NOTIFY));

            case MessageLayerStatics.TOSMARTWATCH_SENSOR_REQUEST:

                try {
                    JSONObject j_msg = new JSONObject(message);
                    if(j_msg.has("sensor_services")){
                        JSONArray array = j_msg.getJSONArray("sensor_services");

                        for(int i = 0; i < array.length(); i++){

                            JSONObject obj = (JSONObject) array.get(i);
                            intent = null;
                            Class tmp_class = null;

                            if(obj.getInt("datasource") == 1){
                                continue;
                            }

                            switch(obj.getInt("service_type")){

                                case 1: // Physical environment data
                                    intent = new Intent(getApplicationContext(), PhysicalRecordingService.class);
                                    intent.putExtra("original_request",j_msg.toString());
                                    intent.putExtra("request", obj.toString());

                                    if(!ServiceRunningUtil.isMyServiceRunning(PhysicalRecordingService.class,
                                            context)){
                                        context.startForegroundService(intent);
                                    }

                                    intent = new Intent(getApplicationContext(), AmbientNoiseRecordingService.class);
                                    intent.putExtra("original_request",j_msg.toString());
                                    intent.putExtra("request", obj.toString());
                                    tmp_class = AmbientNoiseRecordingService.class;
                                    break;

                                case 2: // Physiological data
                                    intent = new Intent(getApplicationContext(), PhysiologicalRecordingService.class);
                                    intent.putExtra("original_request",j_msg.toString());
                                    intent.putExtra("request", obj.toString());
                                    tmp_class = PhysiologicalRecordingService.class;
                                    break;

                                case 3: // Behavioural data
                                    intent = new Intent(getApplicationContext(), BehavioralRecordingService.class);
                                    intent.putExtra("original_request",j_msg.toString());
                                    intent.putExtra("request", obj.toString());
                                    tmp_class = BehavioralRecordingService.class;
                                    break;

                                case 4: // Offtask detection data
                                    intent = new Intent(getApplicationContext(), VirtualSensorService.class);
                                    intent.putExtra("original_request",j_msg.toString());
                                    intent.putExtra("request", obj.toString());
                                    tmp_class = VirtualSensorService.class;
                                    break;

                                case 5: // Device process tracking data
                                    intent = new Intent(getApplicationContext(), ProcessTrackingService.class);
                                    intent.putExtra("original_request",j_msg.toString());
                                    intent.putExtra("request", obj.toString());
                                    tmp_class = ProcessTrackingService.class;
                                    break;

                                default:
                                    break;
                            }

                            if(tmp_class != null && intent != null){
                                if(!ServiceRunningUtil.isMyServiceRunning(tmp_class, context)){
                                    context.startForegroundService(intent);
                                }
                            }
                        }
                    }
                } catch (JSONException e ) {
                    e.printStackTrace();
                }
                break;


            case MessageLayerStatics.TOSMARTWATCH_REQUEST_RECOVERY:
                intent = new Intent(this, RecoveryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;

            case MessageLayerStatics.TOSMARTWATCH_RESPONSE_RECOVERY:
                try {
                    JSONObject recovered_session = new JSONObject(message);
                    if (recovered_session.has("requested_inter")) {
                        if (recovered_session.getBoolean("requested_inter")) {
                            sessionPreferences.setSessionRunning(true);

                            intent = new Intent(this, SessionActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                            Intent mintent = new Intent(getApplicationContext(), SensorDataService.class);
                            mintent.setClassName(getPackageName(), SensorDataService.class.getName());
                            startService(mintent);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case MessageLayerStatics.TOSMARTWATCH_CONTROL_FLOW:
                try {
                    JSONObject session_state = new JSONObject(message);
                    sessionPreferences.setSessionRunning(true);
                    sessionPreferences.setSessionRequestedPre(session_state.getBoolean("requested_pre"));
                    sessionPreferences.setSessionRequestedInter(session_state.getBoolean("requested_inter"));
                    sessionPreferences.setSessionRequestedPost(session_state.getBoolean("requested_post"));
                    sessionPreferences.setSessionTerminated(session_state.getBoolean("terminated"));
                    sessionPreferences.setSessionId(session_state.getString("sessionID"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case MessageLayerStatics.TOSMARTWATCH_RESPONSE_SESSION_STATE:
                try {
                    JSONObject session_state = new JSONObject(message);
                    Log.d(TAG,"NEW SESSIONSTATE");
                    Log.d(TAG,message);

                    if (session_state.has("user_id")) {
                        sessionPreferences.setUserId(String.valueOf(session_state.get("user_id")));
                    }

                    if (session_state.has("studymechanics")){
                        sessionPreferences.setStudyStartMechanics(session_state.getInt("studymechanics"));
                    }

                    if (session_state.has("session_id")) {
                        sessionPreferences.setSessionId(String.valueOf(session_state.get("session_id")));
                    }

                    if(session_state.has("used_device")){
                        sessionPreferences.setUsedDevice(session_state.getInt("used_device"));
                    }


                    if (!session_state.getString("esm_connected").equals("Connected")) {

                        intent = new Intent();
                        intent.setAction(getString(R.string.ACTION_DESTROY_SESSION_ACTIVITY));
                        intent.putExtra("command", "handheld_lost_connection");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

                        intent = new Intent();
                        intent.setAction(getString(R.string.ACTION_MAIN_UI));
                        intent.putExtra("command", "disable_start_button");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        return;
                    }

                    if (session_state.getBoolean("session_running") == sessionPreferences.isSessionRunning()) {
                        if (sessionPreferences.isSessionRunning() &&  session_state.getInt("used_device") == 0) {
                            // Case: Beide sind auf dem gleichen stand und die Session läuft gerade.

                            intent = new Intent(this, SessionActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            Log.d(TAG, "(re)-started SessionActivity.class");

                        } else if (!sessionPreferences.isSessionRunning()) {

                            // Case: Beide sind auf dem gleichen stand und die Session läuft gerade nicht.

                            if(session_state.getInt("studymechanics") == 0){
                                intent = new Intent();
                                intent.setAction(getString(R.string.ACTION_MAIN_UI));
                                intent.putExtra("command", "remote_started");
                                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

                            } else {

                                intent = new Intent();
                                intent.setAction(getString(R.string.ACTION_MAIN_UI));
                                intent.putExtra("command", "enable_start_button");
                                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                                Log.d(TAG, "Enable Session send..");
                            }

                            if(session_state.getInt("used_device") == 1){
                                intent = new Intent();
                                intent.setAction(getString(R.string.ACTION_MAIN_UI));
                                intent.putExtra("command", "exclude_smartwatch");
                                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                            }
                        }

                    } else {
                        if (!session_state.getBoolean("session_running")) {
                            sessionPreferences.setSessionRunning(session_state.getBoolean("session_running"));
                            if(session_state.getInt("studymechanics") == 0){
                                intent = new Intent();
                                intent.setAction(getString(R.string.ACTION_MAIN_UI));
                                intent.putExtra("command", "remote_started");
                                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

                            } else {
                                intent = new Intent();
                                intent.setAction(getString(R.string.ACTION_MAIN_UI));
                                intent.putExtra("command", "enable_start_button");
                                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                                Log.d(TAG, "Enable Session send..");
                            }
                            if(session_state.getInt("used_device") == 1){
                                intent = new Intent();
                                intent.setAction(getString(R.string.ACTION_MAIN_UI));
                                intent.putExtra("command", "exclude_smartwatch");
                                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                            }
                        } else {
                            if (session_state.has("requested_inter")) {
                                if (session_state.getBoolean("requested_inter")  && session_state.getInt("used_device") == 0) {
                                    sessionPreferences.setSessionRunning(true);

                                    intent = new Intent(this, SessionActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                    intent = new Intent(getApplicationContext(), SensorDataService.class);
                                    intent.setClassName(getPackageName(), SensorDataService.class.getName());
                                    startService(intent);
                                }
                            } else {
                                if( session_state.getInt("used_device") == 0) {
                                    MessageLayerSender messageLayerSender = new MessageLayerSender(getApplicationContext());
                                    messageLayerSender.sendAck(getString(R.string.PATH_TO_HANDHELD_ESM_SERVICE_STOP), "");
                                }
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG,e.getMessage());
                }
                break;
            case MessageLayerStatics.TOSMARTWATCH_RESPONSE_ACRA_CREDENTIALS:

                if(! sessionPreferences.isAcraSet()){

                    try {

                        JSONObject object = new JSONObject(message);
                        String username = object.getString("username");
                        String password = object.getString("pw");
                        String url = object.getString("url") + "/acra/_design/acra-storage/_update/report/";
                        Log.d(TAG, "ACRA URL " + url);

                        if(username != null && password != null && url != null){


                            CoreConfigurationBuilder builder = new CoreConfigurationBuilder(this);
                            //core configuration:
                            builder
                                    .withBuildConfigClass(BuildConfig.class)
                                    .withReportFormat(StringFormat.JSON);

                            builder.getPluginConfigurationBuilder(HttpSenderConfigurationBuilder.class)
                                    .withBasicAuthLogin(username)
                                    .withBasicAuthPassword(password)
                                    .withUri(url)
                                    .withSocketTimeout(20000)
                                    .withConnectionTimeout(20000)
                                    .withEnabled(true);


                            builder.getPluginConfigurationBuilder(LimiterConfigurationBuilder.class)
                                    .withEnabled(true)
                                    .withDeleteReportsOnAppUpdate(true)
                                    .withFailedReportLimit(5)
                                    .withStacktraceLimit(5);

                            //each plugin you chose above can be configured with its builder like this:
                            builder.getPluginConfigurationBuilder(NotificationConfigurationBuilder.class)
                                    .withEnabled(true)
                                    .withResTitle(R.string.acra_notification_title)
                                    .withResText(R.string.acra_notification_text)
                                    .withResChannelName(R.string.acra_notification_channel_name)
                                    .withResChannelDescription(R.string.acra_notification_channel_description)
                                    .withResChannelImportance(NotificationManager.IMPORTANCE_HIGH)
                                    .withResTickerText(R.string.acra_notification_ticker)
                                    .withResIcon(R.drawable.ic_notifications_black_24dp)
                                    .withResSendButtonText(R.string.acra_notification_submit)
                                    .withResDiscardButtonText(R.string.acra_notification_discard)
                                    .withResSendWithCommentButtonText(R.string.acra_notification_send_with_comment)
                                    .withResSendWithCommentButtonIcon(R.drawable.icon_reply)
                                    .withResCommentPrompt(R.string.acra_notification_comment_hint)
                                    .withSendOnClick(false);

                            ACRA.init(getApplication(), builder);
                            sessionPreferences.setIsAcraSet(true);

                    }                } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case MessageLayerStatics.TOSMARTWATCH_ERROR_NOTIFICATION:

                try {

                    JSONObject object = new JSONObject(message);
                    NotificationManager notificationManager =
                            (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

                    String CHANNEL_ID = getApplicationContext().getResources().getString(R.string.notiChannelID);

                    NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(),CHANNEL_ID)
                            .setContentText(object.getString("error"))
                            .setSmallIcon(R.drawable.ic_cancel_red_35dp)
                            .setOngoing(false)
                            .setAutoCancel(true);

                    notificationManager.notify("error-message", 9697, notification.build());

                    Log.d(TAG,"onError");
                    Intent intent1 = new Intent();
                    intent1.setAction(ActionUtils.ACTION_REMOTE_TIMER);
                    intent1.putExtra("received_onError",true);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent1);

                } catch (JSONException e) {
                    Log.e(TAG,e.toString());
                }
                break;


            case MessageLayerStatics.TOSMARTWATCH_PING:

                try {
                    JSONObject object = new JSONObject(message);
                    intent = new Intent();
                    intent.setAction("PING_FILTER");
                    intent.putExtra("ping", object.getString("deliveryUUID"));
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

                    Log.d(TAG, "PING: ");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            default:
                Log.i(TAG, "Message could not be assigned.");
                Log.d(TAG, message);
                break;

        }
        super.onMessageReceived(messageEvent);

    }




    @Override
    public void onDestroy() {
        postdelayHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }



    class RunnableWithArguments implements Runnable {

        Intent targetintent;
        Class targetClass;
        int counter = 0;

        RunnableWithArguments(Intent intent, Class classObj) {
            targetintent = intent;
            targetClass = classObj;
            counter = delayTries;
        }

        @Override
        public void run() {
            SystemClock.sleep(delayPostStartService);
            if (!ServiceRunningUtil.isMyServiceRunning(targetClass, getApplicationContext())) {
                Log.d(TAG, "Inside Postdelayed Start. Service was Running. Counter at: " + counter);
                getApplicationContext().startForegroundService(targetintent);
            } else {
                Log.d(TAG, "Service could is still running. " + targetClass.getName() + ". Counter at: " + counter);
                if (counter > 0) {
                    counter -= 1;
                    run();
                } else {
                    try {
                        JSONObject request = new JSONObject(targetintent.getStringExtra("jsonrequest"));
                        MessageLayerSender messageLayerSender = new MessageLayerSender(getApplicationContext());
                        request.put("sensor", targetClass.getName());
                        request.put("value", "Service could not be started. Service already Running. ");
                        messageLayerSender.sendAck(getApplicationContext().getString(R.string.PATH_TO_HANDHELD_RESPONSE_SENSOR_REQUEST), request.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


}
