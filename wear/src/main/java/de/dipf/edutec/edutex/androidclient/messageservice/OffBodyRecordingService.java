package de.dipf.edutec.edutex.androidclient.messageservice;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class OffBodyRecordingService extends Service implements SensorEventListener {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    /*
    final static String TAG = "OffBodyRecordingService";

    SharedPreferences globalsPreferences;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    SensorManager sensorManager;
    Sensor sensor;

    int notification_id = 16845;
    Handler handler;
    Runnable runnable;
    NotificationManager notificationManager;

    @SneakyThrows
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "onStartCommand called");

        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(), fgNotificationCreator.getNotification());

        notificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        globalsPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences = getApplicationContext().getSharedPreferences("OffBodyRecognition", 0);
        editor = sharedPreferences.edit();
        editor.putInt("reminder_count", 0);
        editor.apply();

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LOW_LATENCY_OFFBODY_DETECT, true);
        if (sensor == null) {
            Log.d(TAG, "Device does not have a Off-Body Sensor.");
            stopForeground(false);
            stopSelf();
            return START_NOT_STICKY;
        } else {
            Log.d(TAG, "Registering Off-Body Sensor.");
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {

                    if (globalsPreferences.getBoolean(getString(R.string.key_session_running), false)) {

                        stopForeground(true);
                        stopSelf();

                    } else {
                        int reminder_count = sharedPreferences.getInt("reminder_count", 0);
                        if (reminder_count <= 9) {
                            create_notification();
                            editor.putInt("reminder_count", reminder_count + 1);
                            editor.apply();
                            handler.postDelayed(this, 60000);
                        } else {

                            editor.putBoolean("remote_stopped", true);
                            editor.apply();

                            // Stopping SensorDataService which was started by SurveySubmitFragment
                            Intent sensorDataServiceIntnet = new Intent(getApplicationContext(), SensorDataService.class);
                            sensorDataServiceIntnet.setClassName(getPackageName(), SensorDataService.class.getName());
                            stopService(sensorDataServiceIntnet);

                            // Cleaning Notification and Survey Data
                            NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                            manager.cancelAll();
                            MicroQuestionnairePreference microQuestionnairePreference = new MicroQuestionnairePreference(getApplicationContext());
                            microQuestionnairePreference.clear();

                            // Make Session-Running Statement for the whole application
                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean(getString(R.string.key_session_running), false);
                            editor.apply();

                            // Requestion the Post Session Survey
                            MessageLayerSender messageLayerSender = new MessageLayerSender(getApplicationContext());
                            messageLayerSender.sendForceStop();

                            stopForeground(true);
                            stopSelf();
                        }
                    }
                }
            };

        }

        return START_NOT_STICKY;
    }

    private void create_notification() {

        Boolean is_off = sharedPreferences.getBoolean("off_body", false);
        Log.d(TAG, "createNotification(): " + is_off);
        if (is_off) {
            String CHANNEL_ID = getApplicationContext().getResources().getString(R.string.notiChannelID);

            Notification notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                    .setOngoing(false)
                    .setContentText("Stop Learning Session or wear the watch")
                    .setSmallIcon(R.drawable.ic_survey)
                    .build();

            notificationManager.notify(notification_id, notification);

            Intent intent1 = new Intent(getApplicationContext(), VibrationService.class);
            getApplicationContext().startForegroundService(intent1);

        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float off_body_value = event.values[0];
        if (off_body_value == 0.0) {
            Log.d(TAG, "onSensorChanged(): The Watch is off body!");
            editor.putBoolean("off_body", true);
            editor.putLong("last_seen", Instant.now().toEpochMilli());
            editor.apply();
            handler.postDelayed(runnable, 60000);
        } else if (off_body_value == 1.0) {
            Log.d(TAG, "onSensorChanged(): The Watch is on the body!");
            editor.putBoolean("off_body", false);
            editor.putLong("last_seen", Instant.now().toEpochMilli());
            editor.putInt("reminder_count", 0);
            editor.apply();
            handler.removeCallbacks(runnable);
            notificationManager.cancel(notification_id);
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestory(): Destroying service!");
        try {
            handler.removeCallbacks(runnable);

        } catch (Exception er) {
            er.printStackTrace();
        }

        try {
            sensorManager.unregisterListener(this);
        } catch (Exception er) {
            er.printStackTrace();
        }
        stopForeground(false);
        super.onDestroy();
    }
    */
}
