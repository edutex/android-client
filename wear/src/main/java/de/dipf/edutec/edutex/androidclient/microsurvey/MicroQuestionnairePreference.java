package de.dipf.edutec.edutex.androidclient.microsurvey;


import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.json.JSONObject;

import java.time.Instant;

import de.dipf.edutec.edutex.androidclient.utils.ServiceRunningUtil;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class MicroQuestionnairePreference {

    private static String TAG = MicroQuestionnairePreference.class.getSimpleName();
    private static String PREFS_NAME = "mircoquestionnairepreferences";
    private static final String NOTIFICATION_TAG = "micronotificationtag";

    private SharedPreferences sharedPreferences;
    private Context context;
    long minute_millis = 60000;

    private static final String KEY_CURRENT_MICRO_QUESTIONNAIRE = "current_micro_questionnaire";
    private static final String KEY_CURRENT_MICRO_QUESTIONNAIRE_NOTIFICATION_ID = "current_micro_id";
    private static final String KEY_CANCEL_INTENT = "cancel_intent";
    private static final String KEY_REMINDER_INTENT = "reminder_intent";

    public MicroQuestionnairePreference(Context context){
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    @SneakyThrows
    public JSONObject getCurrentMicroQuestionnaire(){
        String obj = this.sharedPreferences.getString(KEY_CURRENT_MICRO_QUESTIONNAIRE,null);
        if(obj != null){
            JSONObject object = new JSONObject(obj);
            return object;
        }
        return null;
    }

    @SneakyThrows
    public void setCurrentMicroQuestionnaire(JSONObject object, int id){
        Log.d(TAG, "Setting new micro");

        if(this.sharedPreferences.contains(KEY_CURRENT_MICRO_QUESTIONNAIRE)){
            Log.d(TAG, "found old micro");
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(NOTIFICATION_TAG, this.getNotificationID());
            clear();
        }

        Instant instant = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();
        object.put("initial_timestamp_asked",RequestResponseUtils.getTimeStamps(instant, elapsed));
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_CURRENT_MICRO_QUESTIONNAIRE, object.toString());
        editor.putInt(KEY_CURRENT_MICRO_QUESTIONNAIRE_NOTIFICATION_ID,id);
        editor.commit();

        setReminder(object);
        setCancel(object);
    }
    @SneakyThrows
    public void updateCurrentMicroQuestionnaire(){
        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();
        JSONObject object = getCurrentMicroQuestionnaire();
        object.put("second_timestamp_asked", RequestResponseUtils.getTimeStamps(now,elapsed));

        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_CURRENT_MICRO_QUESTIONNAIRE,object.toString());
        editor.commit();

        Intent intent = new Intent();
        intent.setAction("micro_scheduling_remove");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        setCancel(object);

    }


    public boolean isMicroQuestionnaireAvailable(){
        return this.sharedPreferences.contains(KEY_CURRENT_MICRO_QUESTIONNAIRE);
    }
    public int getNotificationID(){return this.sharedPreferences.getInt(KEY_CURRENT_MICRO_QUESTIONNAIRE_NOTIFICATION_ID,-1);}

    // MicroQuestionnaire Lifetime Functions
    @SneakyThrows
    private void setCancel(JSONObject object){
        if(object.getBoolean("question_lifetime")){

            if(!ServiceRunningUtil.isMyServiceRunning(MicroQuestionnaireSchedulingService.class, context)){
                context.startService(new Intent(context,MicroQuestionnaireSchedulingService.class));
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent reminder_intent = new Intent();
                    reminder_intent.putExtra("remove_message",object.toString());
                    reminder_intent.setAction("micro_scheduling_add");
                    LocalBroadcastManager.getInstance(context).sendBroadcast(reminder_intent);
                }
            },2000);




            Log.d(TAG,"Cancel Alarm set. Current Time: "+ String.valueOf( SystemClock.elapsedRealtime())+
                    " Trigger: "+String.valueOf( SystemClock.elapsedRealtime() + minute_millis) );
        }
    }


    // MicroQuestionnaire Reminder Fuctions
    @SneakyThrows
    private void setReminder(JSONObject object){
        if(object.getBoolean("question_reminder")){

            if(!ServiceRunningUtil.isMyServiceRunning(MicroQuestionnaireSchedulingService.class, context)){
                context.startService(new Intent(context,MicroQuestionnaireSchedulingService.class));
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent();
                    i.putExtra("renotify", object.toString());
                    i.setAction("micro_scheduling_add");
                    LocalBroadcastManager.getInstance(context).sendBroadcast(i);
                }
            },2000);

            Log.d(TAG,"Reminder Alarm set. Current Time: "+ String.valueOf( SystemClock.elapsedRealtime())+
                    " Trigger: "+String.valueOf( SystemClock.elapsedRealtime() + minute_millis) );
        }
    }


    public void clear(){
        Log.d(TAG,"Cleared Prefs.");
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.remove(KEY_CURRENT_MICRO_QUESTIONNAIRE);
        editor.remove(KEY_CURRENT_MICRO_QUESTIONNAIRE_NOTIFICATION_ID);
        editor.commit();

        Intent intent = new Intent();
        intent.setAction("micro_scheduling_remove");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


    public String getNotificationTag(){return NOTIFICATION_TAG;}










}
