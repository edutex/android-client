package de.dipf.edutec.edutex.androidclient.microsurvey;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.json.JSONException;
import org.json.JSONObject;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.MainActivity;
import de.dipf.edutec.edutex.androidclient.activities.MicroInteractionSurveyActivity;
import de.dipf.edutec.edutex.androidclient.messageservice.MessageLayerSender;
import de.dipf.edutec.edutex.androidclient.messageservice.VibrationService;

public class MicroSurveyNotificationPublisher  extends BroadcastReceiver {

    final static String TAG = MicroSurveyNotificationPublisher.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "onReceive is called");

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        MicroQuestionnairePreference prefs = new MicroQuestionnairePreference(context);
        String CHANNEL_ID = context.getResources().getString(R.string.notiChannelID);

        if(intent.hasExtra("message")){

            // Adding new MicroQuestionnaires to the notification channel

            String received_message = intent.getStringExtra("message");
            Log.d(TAG, "New Message received");
            Log.d(TAG, received_message);

            try {
                JSONObject object = new JSONObject(received_message);
                prefs.setCurrentMicroQuestionnaire(object, object.hashCode());
                Intent sendIntent = new Intent(context, MicroInteractionSurveyActivity.class);

                PendingIntent pendingIntent = TaskStackBuilder.create(context.getApplicationContext())
                        .addNextIntent(sendIntent)
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                Notification notification =  new NotificationCompat.Builder(context,CHANNEL_ID)
                        .setContentTitle("uSurvey")
                        .setSmallIcon(R.drawable.ic_cc_checkmark)
                        .setContentIntent(pendingIntent)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(prefs.getNotificationTag(),object.hashCode(), notification);

                Intent intent1 = new Intent(context.getApplicationContext(), VibrationService.class);
                context.getApplicationContext().startForegroundService(intent1);
                Log.d(TAG, "Should notified");


                Intent interrupt_intent = new Intent();
                interrupt_intent.setAction(context.getString(R.string.ACTION_MICRO_SURVEY_INTERRUPT));
                LocalBroadcastManager.getInstance(context).sendBroadcast(interrupt_intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (intent.hasExtra("remove_message")){

            // Removing Microquestionnaires from the notification channel and from the memory
            Log.d(TAG, "Command remove_message received");
            try {
                notificationManager.cancel(prefs.getNotificationTag(),prefs.getNotificationID());
                prefs.clear();
                Log.d(TAG,"Removed Notification for uSurvey");
                if(intent.hasExtra("acknowledge_not_answered")){
                    String mSurvey = intent.getStringExtra("remove_message");
                    JSONObject jsonObject = new JSONObject(mSurvey);
                    jsonObject.put("user_answer",-1);

                    MessageLayerSender messageLayerSender = new MessageLayerSender(context);
                    messageLayerSender.sendAck(context.getString(R.string.PATH_TO_HANDHELD_RESPONSE_SURVEY),jsonObject.toString());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if( intent.hasExtra("renotify")){

            Log.d(TAG,"Command renotify received");
            Intent sendIntent = new Intent(context, MainActivity.class);

            PendingIntent pendingIntent = TaskStackBuilder.create(context.getApplicationContext())
                    .addNextIntent(sendIntent)
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification notification =  new NotificationCompat.Builder(context,CHANNEL_ID)
                    .setContentTitle("uSurvey")
                    .setSmallIcon(R.drawable.ic_cc_checkmark)
                    .setContentIntent(pendingIntent)
                    .build();

            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify(prefs.getNotificationTag(),prefs.getNotificationID(), notification);
            prefs.updateCurrentMicroQuestionnaire();

            Intent intent1 = new Intent(context.getApplicationContext(), VibrationService.class);
            context.getApplicationContext().startForegroundService(intent1);


        }




    }
}
