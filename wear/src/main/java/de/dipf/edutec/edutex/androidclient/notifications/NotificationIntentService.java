package de.dipf.edutec.edutex.androidclient.notifications;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;

import android.os.SystemClock;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.app.RemoteInput;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.Instant;
import java.util.Objects;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.messageservice.MessageLayerSender;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class NotificationIntentService extends IntentService {

    static String TAG = "NotificationIntentService";

    public NotificationIntentService(){
        super("NotificationPublisher");
    }


    @SneakyThrows
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Instant instant = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();


        if(intent.hasExtra("intent_destination") &&
                intent.getStringExtra("intent_destination")
                        .equals("NotificationIntentService")){

            Log.d(TAG, "received user input to digest");

            MessageLayerSender messageLayerSender = new MessageLayerSender(this);

            Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
            if(remoteInput != null){
                CharSequence userReply = remoteInput.getCharSequence(getString(R.string.key_result_intent_notification));
                Log.d(TAG, "user answer: " + userReply);
                if(userReply != null){
                    JSONObject modifiedMessage = new JSONObject(Objects.requireNonNull(intent.getStringExtra("original_esm_message")));

                    JSONArray answers = modifiedMessage.getJSONArray("notification_answer");
                    for(int i = 0; i < answers.length(); i++){
                        if(answers.getJSONObject(i).getString("answer").equals(userReply)){
                            modifiedMessage.put("user_answer",answers.getJSONObject(i).getInt("id"));
                            break;
                        }
                    }

                    modifiedMessage.put("timestamp_question_answered", RequestResponseUtils.getTimeStamps(instant,elapsed));

                    Log.d(TAG, "added user_answer and answering time to message");
                    messageLayerSender.sendAck(getString(R.string.PATH_TO_HANDHELD_RESPONSE_NOTIFICATION), modifiedMessage.toString());
                    Log.d(TAG, "passed new message to Class MessageLayerSender");
                }
            }

        }
    }
}


