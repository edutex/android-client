package de.dipf.edutec.edutex.androidclient.notifications;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.wearable.input.RemoteInputConstants;
import android.util.Log;


import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.Instant;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.messageservice.VibrationService;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class NotificationPublisher extends BroadcastReceiver {

    public static int flagCancelCurrent = FLAG_CANCEL_CURRENT;
    private static String TAG = "NotificationPublisher";



    @SneakyThrows
    @Override
    public void onReceive(Context context, Intent intent) {
        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();

        String CHANNEL_ID = context.getResources().getString(R.string.notiChannelID);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        String received_message = intent.getStringExtra("message");
        int esm_msg_hash = received_message.hashCode();

        // Adding additional infos for esm database
        JSONObject received_message_json = new JSONObject(received_message);


        //received_message_json.put("timestamp_question_asked", NotificationSingleton.getInstance().checkHash(esm_msg_hash));
        received_message_json.put("timestamp_question_asked", RequestResponseUtils.getTimeStamps(now, elapsed));
        received_message_json.put("identifier", esm_msg_hash);


        // Preparations for IntentService to callback to handheld
        Intent replyIntent = new Intent(context, NotificationIntentService.class);
        replyIntent.putExtra("original_esm_message",received_message_json.toString());
        replyIntent.putExtra("intent_destination","NotificationIntentService");

        // Received Notification Job from Handheld
        JSONObject jsonMessage = new JSONObject(received_message);



        // Preparing notification content and execution
        Log.d(TAG, "Preparing Notification with ID: " + String.valueOf(esm_msg_hash));
        Log.d(TAG,jsonMessage.toString());
        String esm_question = jsonMessage.getString("question");
        CharSequence[] esm_answers = new CharSequence[]{};
        try {
            JSONArray esm_answers_json = jsonMessage.getJSONArray("notification_answer");
            esm_answers = new CharSequence[esm_answers_json.length()];
            for (int i = 0; i < esm_answers_json.length() ; i++) {
                esm_answers[i] = esm_answers_json.getJSONObject(i).getString("answer");
            }
        } catch (Exception e){
            Log.d(TAG, e.toString());
        }


        PendingIntent directReplyPendingIntent = PendingIntent.getService(context, 0, replyIntent, flagCancelCurrent);
        Bundle extras = new Bundle();
        extras.putBoolean(RemoteInputConstants.EXTRA_DISALLOW_EMOJI, true);


        if(esm_answers.length == 0){

            Log.d(TAG, "Notification without Answers");

            Notification notification = new NotificationCompat.Builder(context,CHANNEL_ID)
                    .setOngoing(true)
                    .setContentText(esm_question)
                    .setSmallIcon(R.drawable.ic_survey)
                    .build();

            notificationManager.notify(esm_msg_hash,notification);
            Log.d(TAG, "Notification was published successfully.");

        } else {
            RemoteInput remoteInput = new RemoteInput.Builder(context.getString(R.string.key_result_intent_notification))
                    .setLabel("Type..")
                    .setChoices(esm_answers)
                    .addExtras(extras)
                    .build();

            NotificationCompat.Action action = new NotificationCompat.Action.Builder(R.drawable.icon_reply,"Choices",directReplyPendingIntent)
                    .setAllowGeneratedReplies(false)
                    .addRemoteInput(remoteInput)
                    .build();


            Notification notification = new NotificationCompat.Builder(context,CHANNEL_ID)
                    .setAutoCancel(true)
                    .setOngoing(true)
                    .setContentText(esm_question)
                    .setSmallIcon(R.drawable.ic_survey)
                    .addAction(action)
                    .build();

            notificationManager.notify(esm_msg_hash,notification);
            Log.d(TAG, "Notification was published successfully.");

            Intent intent1 = new Intent(context.getApplicationContext(), VibrationService.class);
            context.getApplicationContext().startForegroundService(intent1);

        }






    }
}
