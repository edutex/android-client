package de.dipf.edutec.edutex.androidclient.processtracking;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import de.dipf.edutec.edutex.androidclient.utils.TrackingSensorPreferences;

public class ProcessTrackingService extends Service {

    public static final String TAG = ProcessTrackingService.class.getSimpleName();
    TrackingSensorPreferences trackingSensorPreferences;
    JSONObject request;
    JSONObject original_request;
    int duration;
    long begin;
    long end;



    private Handler handler;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            stopSelf();
        }
    };



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(), fgNotificationCreator.getNotification());

        try{

            request = new JSONObject(intent.getStringExtra("request"));
            original_request = new JSONObject(intent.getStringExtra("original_request"));
            duration = request.getInt("duration");
            begin = System.currentTimeMillis();
        }catch (Exception er){

            stopSelf();

        }



        // Window Change Detection
        // NotificationListener
        trackingSensorPreferences = new TrackingSensorPreferences(getApplicationContext());
        trackingSensorPreferences.setTrackRequest(request);
        trackingSensorPreferences.setTrackOriginalRequest(original_request);
        trackingSensorPreferences.setAllTrue();


        // AppUsageStats
        UStats.getUsageStatsList(getApplicationContext());


        handler = new Handler();
        handler.postDelayed(runnable,duration*1000);

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy(){
        end = System.currentTimeMillis();
        UStats.getUsageStatsList(getApplicationContext());
        //UStats.queryUsageStatsListAggregatet(getApplicationContext(),begin,end);
        try{
            UStats.getUsageStatistics(begin, end,getApplicationContext());
            UStats.getAllEvents(begin,end,getApplicationContext());
        }catch (Exception er){

        }

        trackingSensorPreferences.setAllFalse();
        handler.removeCallbacks(runnable);
        stopForeground(false);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
