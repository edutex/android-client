package de.dipf.edutec.edutex.androidclient.sensorsnapshotservices;

import android.hardware.Sensor;

public class PhysicalRecordingService extends BaseSensorService {

    public static String TAG = PhysicalRecordingService.class.getSimpleName();
    public static String ServiceName = "Physical Sensor Recording Service";
    public static double ServiceVersion = 1.0;

    public static int[] sensorList = new int[]{
            Sensor.TYPE_AMBIENT_TEMPERATURE,
            Sensor.TYPE_LIGHT,
            Sensor.TYPE_RELATIVE_HUMIDITY,
//            Sensor.TYPE_PRESSURE,
    };

    public static int interval_packets = 5;
    public static int batch_size = 100;


    public PhysicalRecordingService() {
        super(sensorList, ServiceName, ServiceVersion, batch_size, interval_packets, TAG, TransferOption.TIMER);
    }

}
