package de.dipf.edutec.edutex.androidclient.sensorsnapshotservices;


import android.hardware.Sensor;

public class VirtualSensorService extends BaseSensorService {

    private static final String TAG = VirtualSensorService.class.getSimpleName();
    public static String ServiceName = "Virtual Sensor Recording Service";
    public static double ServiceVersion = 1.0;


    private static final int TYPE_ACTIVITY_RECOGNITION = 69632;
    private static final int TYPE_ACTIVITY_DATA = 69633;
    private static final int TYPE_WAKE_GESTURE = 23;
    private static final int TYPE_GLANCE_GESTURE = 24;
    private static final int TYPE_TILT = 26;


    private static int[] sensorList = new int[]{
            TYPE_ACTIVITY_RECOGNITION,
            TYPE_ACTIVITY_DATA,
            TYPE_TILT,
            Sensor.TYPE_LOW_LATENCY_OFFBODY_DETECT,
            Sensor.TYPE_SIGNIFICANT_MOTION,
            TYPE_WAKE_GESTURE,
            TYPE_GLANCE_GESTURE,
            Sensor.TYPE_STEP_DETECTOR,
            Sensor.TYPE_STEP_COUNTER

    };

    public static int interval_packets = 10;
    public static int batch_size = 5;

    public VirtualSensorService() {
        super(sensorList, ServiceName, ServiceVersion, batch_size, interval_packets, TAG, TransferOption.TIMER);
    }

}
