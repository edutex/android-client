package de.dipf.edutec.edutex.androidclient.session;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import de.dipf.edutec.edutex.androidclient.R;

public class SessionFragmentGeneral extends Fragment {

    LocalBroadcastManager localBroadcastManager;
    TextView tv_last_second, tv_per_second;

    public SessionFragmentGeneral() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.localBroadcastManager = LocalBroadcastManager.getInstance(getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_session_general, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        findGUIElements();
    }


    public void findGUIElements(){
        tv_per_second = getActivity().findViewById(R.id.tv_session_general_avg);
        tv_last_second = getActivity().findViewById(R.id.tv_session_general_last);
    }
}