package de.dipf.edutec.edutex.androidclient.session;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.SessionActivity;
import de.dipf.edutec.edutex.androidclient.utils.SessionPreferences;
import pl.droidsonroids.gif.GifImageView;

public class SessionFragmentMain extends Fragment {


    public ImageButton stopSession;
    public TextView tvRemoteText, press_long_text;
    public GifImageView spinner;

    Handler handler;

    Runnable reactive_stop_button = new Runnable() {
        @Override
        public void run() {
            stopSession.setEnabled(true);
        }
    };

    public SessionFragmentMain() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        handler = new Handler();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_session_main, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();

        stopSession = getActivity().findViewById(R.id.ib_session_stop);
        tvRemoteText = getActivity().findViewById(R.id.tv_remote_stop);
        press_long_text = getActivity().findViewById(R.id.help_text_press_long);
        spinner = getActivity().findViewById(R.id.spinner);

        stopSession.setVisibility(View.GONE);
        spinner.setVisibility(View.VISIBLE);


        SessionPreferences sessionPreferences = new SessionPreferences(getContext().getApplicationContext());

        spinner.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                SessionPreferences sessionPreferences1 = new SessionPreferences(getContext().getApplicationContext());
                Log.e("123",String.valueOf(sessionPreferences1.getSWSessionTimer()));
                Log.e("123",String.valueOf(sessionPreferences1.isSessionRunning()));

                return false;
            }
        });


        if(sessionPreferences.getStudyStartMechanics() == 0){

            stopSession.setVisibility(View.GONE);
            press_long_text.setVisibility(View.GONE);
            spinner.setVisibility(View.GONE);
            tvRemoteText.setVisibility(View.VISIBLE);

        } else {
            stopSession.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    stopSession.setVisibility(View.GONE);
                    spinner.setVisibility(View.VISIBLE);
                    ((SessionActivity)getActivity()).initStopSession();
                    return true;
                }
            });

            stopSession.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }




    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(reactive_stop_button);

        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        SessionPreferences sessionPreferences = new SessionPreferences(getContext());
        if(sessionPreferences.isSessionInRecovery()){
            stopSession.setEnabled(true);
            stopSession.setVisibility(View.VISIBLE);
            spinner.setVisibility(View.GONE);
            sessionPreferences.sessionInRecovery(false);
        }
    }



}