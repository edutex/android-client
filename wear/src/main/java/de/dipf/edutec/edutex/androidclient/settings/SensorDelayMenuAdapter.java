package de.dipf.edutec.edutex.androidclient.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.dipf.edutec.edutex.androidclient.R;


public class SensorDelayMenuAdapter extends RecyclerView.Adapter<SensorDelayMenuAdapter.RecyclerViewHolder>{

    private ArrayList<MainMenuItem> dataSource = new ArrayList<MainMenuItem>();
    public interface AdapterCallback{
        void onItemClicked(Integer menuPosition);
    }
    private SensorDelayMenuAdapter.AdapterCallback callback;


    private static String TAG = "SensorDelayMenuAdapter";
    private String drawableIcon;
    private Context context;
    SharedPreferences sensorPrefs;
    SharedPreferences.Editor editorSensorPrefs;
    String key_back_to_settings = "Back to Settings";


    public SensorDelayMenuAdapter(Context context, ArrayList<MainMenuItem> dataArgs, SensorDelayMenuAdapter.AdapterCallback callback){
        this.context = context;
        this.dataSource = dataArgs;
        this.callback = callback;
        this.sensorPrefs = context.getSharedPreferences("SensorPreferences", Context.MODE_PRIVATE);
        this.editorSensorPrefs = this.sensorPrefs.edit();
    }

    @Override
    public SensorDelayMenuAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.settings_sensor_menu_item,parent,false);

        SensorDelayMenuAdapter.RecyclerViewHolder recyclerViewHolder = new SensorDelayMenuAdapter.RecyclerViewHolder(view);

        return recyclerViewHolder;
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        RelativeLayout menuContainer;
        TextView menuItem;
        ImageView menuIcon;

        public RecyclerViewHolder(View view) {
            super(view);
            menuContainer = view.findViewById(R.id.menu_container);
            menuItem = view.findViewById(R.id.menu_item);
            menuIcon = view.findViewById(R.id.menu_icon);
        }
    }

    @Override
    public void onBindViewHolder(SensorDelayMenuAdapter.RecyclerViewHolder holder, final int position) {
        MainMenuItem data_provider = dataSource.get(position);

        int selected_delay_int = sensorPrefs.getInt("sensor-delay",-1);
        if(selected_delay_int == -1){
            editorSensorPrefs.putInt("sensor-delay",3);
            editorSensorPrefs.apply();
            selected_delay_int = 3;
        }

        String holder_text = data_provider.getText();
        holder.menuItem.setText(holder_text);

        if(SettingsFragmentSensorDelay.SENSOR_DELAY_NAME[selected_delay_int].equals(holder_text) &&
                !holder_text.equals(key_back_to_settings)){

            holder.menuIcon.setImageResource(R.drawable.ic_baseline_check_circle_outline_24);

        } else if(holder_text.equals(key_back_to_settings)){

            holder.menuIcon.setImageResource(R.drawable.ic_baseline_arrow_back_24);

        } else {

            holder.menuIcon.setImageResource(R.drawable.ic_baseline_radio_button_unchecked_24);

        }




        holder.menuContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (!holder.menuItem.getText().equals(key_back_to_settings)){

                    editorSensorPrefs.putInt("sensor-delay",position);
                    editorSensorPrefs.apply();


                }


                if(callback != null) {
                    callback.onItemClicked(position);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }
}

