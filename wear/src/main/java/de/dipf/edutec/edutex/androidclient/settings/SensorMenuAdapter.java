package de.dipf.edutec.edutex.androidclient.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import de.dipf.edutec.edutex.androidclient.R;

public class SensorMenuAdapter extends RecyclerView.Adapter<SensorMenuAdapter.RecyclerViewHolder> {

    private ArrayList<MainMenuItem> dataSource = new ArrayList<MainMenuItem>();
    public interface AdapterCallback{
        void onItemClicked(Integer menuPosition);
    }
    private AdapterCallback callback;

    private String drawableIcon;
    private Context context;
    SharedPreferences sensorPrefs;
    SharedPreferences.Editor editorSensorPrefs;
    String key_back_to_settings = "Back to Settings";


    public SensorMenuAdapter(Context context, ArrayList<MainMenuItem> dataArgs, AdapterCallback callback){
        this.context = context;
        this.dataSource = dataArgs;
        this.callback = callback;
        this.sensorPrefs = context.getSharedPreferences("SensorPreferences", Context.MODE_PRIVATE);
        this.editorSensorPrefs = this.sensorPrefs.edit();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.settings_sensor_menu_item,parent,false);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);

        return recyclerViewHolder;
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        RelativeLayout menuContainer;
        TextView menuItem;
        ImageView menuIcon;

        public RecyclerViewHolder(View view) {
            super(view);
            menuContainer = view.findViewById(R.id.menu_container);
            menuItem = view.findViewById(R.id.menu_item);
            menuIcon = view.findViewById(R.id.menu_icon);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        MainMenuItem data_provider = dataSource.get(position);

        int selected = sensorPrefs.getInt(data_provider.getText(),2);
        if(selected == 2){
            editorSensorPrefs = sensorPrefs.edit();
            editorSensorPrefs.putInt(data_provider.getText(),1);
            editorSensorPrefs.apply();
            selected = 1;
        }

        holder.menuItem.setText(data_provider.getText());

        if(selected == 1 && !data_provider.getText().equals(key_back_to_settings)){
            holder.menuIcon.setImageResource(R.drawable.ic_baseline_check_circle_outline_24);
        } else if(data_provider.getText().equals(key_back_to_settings)){
            holder.menuIcon.setImageResource(R.drawable.ic_baseline_arrow_back_24);
        } else {
            holder.menuIcon.setImageResource(R.drawable.ic_baseline_radio_button_unchecked_24);
        }

        holder.menuContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (!holder.menuItem.getText().equals(key_back_to_settings)){

                    int current_value = sensorPrefs.getInt((String) holder.menuItem.getText(),0);

                    if(current_value == 1){

                        editorSensorPrefs.putInt((String) holder.menuItem.getText(),0);
                        holder.menuIcon.setImageResource(R.drawable.ic_baseline_radio_button_unchecked_24);

                    } else {

                        editorSensorPrefs.putInt((String) holder.menuItem.getText(),1);
                        holder.menuIcon.setImageResource(R.drawable.ic_baseline_check_circle_outline_24);

                    }
                    editorSensorPrefs.apply();
                }


                if(callback != null) {
                    callback.onItemClicked(position);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }
}

