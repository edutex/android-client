package de.dipf.edutec.edutex.androidclient.settings;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.wear.widget.WearableLinearLayoutManager;
import androidx.wear.widget.WearableRecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.SettingsActivity;


public class SettingsFragmentMain extends Fragment {




    public SettingsFragmentMain() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings_main, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        WearableRecyclerView recyclerView = getActivity().findViewById(R.id.rv_settings_main);
        recyclerView.setHasFixedSize(true);
        recyclerView.setEdgeItemsCenteringEnabled(true);
        recyclerView.setLayoutManager(new WearableLinearLayoutManager(getContext()));

        ArrayList<MainMenuItem> menuItems = new ArrayList<>();
        menuItems.add(new MainMenuItem(R.drawable.ic_baseline_data_usage_24, "Sensors"));
        menuItems.add(new MainMenuItem(R.drawable.ic_dummy_24, "Sensor Delay"));
        menuItems.add(new MainMenuItem(R.drawable.ic_baseline_arrow_back_24, "Close"));


        recyclerView.setAdapter(new MainMenuAdapter(getContext(), menuItems, new MainMenuAdapter.AdapterCallback() {
            @Override
            public void onItemClicked(Integer menuPosition) {
                switch (menuPosition) {
                    case 0:
                        ((SettingsActivity)getActivity()).changeFragment("SettingsFragmentsSensors");
                        break;
                    case 1:
                        ((SettingsActivity)getActivity()).changeFragment("SettingsFragmentsSensorDelay");
                        break;
                    case 2:
                        ((SettingsActivity)getActivity()).finish();
                        break;
                    default:
                        System.out.println("Default");
                }
            }
        }));

    }
}