package de.dipf.edutec.edutex.androidclient.settings;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.wear.widget.WearableLinearLayoutManager;
import androidx.wear.widget.WearableRecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.util.ArrayUtils;

import java.util.ArrayList;
import java.util.List;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.SettingsActivity;
import de.dipf.edutec.edutex.androidclient.sensorservice.SensorDataService;


public class SettingsFragmentSensors extends Fragment {

    static String TAG = "SettingsFragmentSensors";




    public SettingsFragmentSensors() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings_sensors, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();


        SensorManager sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);

        WearableRecyclerView wearableRecyclerView = getActivity().findViewById(R.id.rv_settings_sensor);
        wearableRecyclerView.setHasFixedSize(true);
        wearableRecyclerView.setEdgeItemsCenteringEnabled(true);
        wearableRecyclerView.setLayoutManager(new WearableLinearLayoutManager(getContext()));

        ArrayList<MainMenuItem> menuItems = new ArrayList<>();
        for(int i = 0; i < sensors.size(); i ++ ){
            if (ArrayUtils.contains(SensorDataService.ENABLED_SENSORS, sensors.get(i).getType())){
                try{
                    String sensor_name = String.valueOf(sensors.get(i).getName());
                    Log.d(TAG, "Sensor Name: " + sensor_name);
                    menuItems.add(new MainMenuItem(R.drawable.ic_baseline_check_circle_outline_24,sensors.get(i).getName()));
                } catch (NullPointerException e){

                    Log.d(TAG, "Sensor is not available");

                }
            }
        }

        menuItems.add(new MainMenuItem(R.drawable.ic_baseline_arrow_back_24, "Back to Settings"));

        int key_back_to_settings = menuItems.size() - 1;
        Log.d(TAG, "key_back_to_settings value: " + String.valueOf(key_back_to_settings));
        wearableRecyclerView.setAdapter(new SensorMenuAdapter(getContext(), menuItems, new SensorMenuAdapter.AdapterCallback() {
            @Override
            public void onItemClicked(Integer menuPosition) {

                Log.d(TAG, "Pressed menuPosition " + String.valueOf(menuPosition));

                if(menuPosition == key_back_to_settings){
                    ((SettingsActivity)getActivity()).changeFragment("SettingsFragmentMain");
                }

            }
        }));



    }
}