package de.dipf.edutec.edutex.androidclient.survey;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.SurveyActivity;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class QuestionnaireFragmentSlider extends Fragment {

    private static String TAG = QuestionnaireFragmentChoice.class.getSimpleName();
    private SeekBar seekBar;
    private TextView seekBarTv;
    private TextView tv_question_survey;
    private JSONArray answers;
    private ImageButton checkAnswer;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.4F);
    private JSONObject question_data;
    private  String[] poss_answers;

    @SneakyThrows
    @Override
    public void onStart(){
        super.onStart();
        tv_question_survey = getView().findViewById(R.id.tv_question_survey);
        tv_question_survey.setText(question_data.getString("question"));
        seekBarTv = getView().findViewById(R.id.tv_answer_display);
        seekBar = getView().findViewById(R.id.slider_answer);
        seekBar.setMax(poss_answers.length - 1);
        seekBar.setMin(0);

        if(question_data.has("user_answer")){
            int userAnswer = question_data.getInt("user_answer");
            seekBarTv.setText(poss_answers[0]);
            seekBar.setProgress(0);

            for(int i = 0; i < answers.length(); i++){
                if(userAnswer == (answers.getJSONObject(i).getInt("id"))){
                    seekBar.setProgress(i);
                    seekBarTv.setText(poss_answers[i]);
                    break;
                }
            }



        } else {
            seekBar.setProgress(0);
            seekBarTv.setText(poss_answers[0]);
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarTv.setText(poss_answers[progress]);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        checkAnswer = getView().findViewById(R.id.bt_check_slider);
        checkAnswer.setOnClickListener(new View.OnClickListener() {
            @SneakyThrows
            @Override
            public void onClick(View v) {
                Instant now = Instant.now();
                long elapsed = SystemClock.elapsedRealtimeNanos();
                v.startAnimation(buttonClick);
                question_data.put("user_answer", answers.getJSONObject(seekBar.getProgress()).getString("id"));
                question_data.put("timestamp_answered", RequestResponseUtils.getTimeStamps(now, elapsed));
                ((SurveyActivity)getActivity()).replaceQuestion(question_data);
                ((SurveyActivity)getActivity()).setNextFragment();
                onDestroy();
            }
        });
    }


    public QuestionnaireFragmentSlider(JSONObject obj) throws JSONException {
        this.question_data = obj;

        this.answers = obj.getJSONArray("answers");
        String[] answers_strings = new String[answers.length()];
        for(int i = 0; i < answers.length() ; i++){
            answers_strings[i] = answers.getJSONObject(i).getString("answer");
        }

        this.poss_answers = answers_strings;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_survey_slider, container, false);
    }

}
