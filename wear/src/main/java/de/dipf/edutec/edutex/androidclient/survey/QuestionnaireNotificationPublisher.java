package de.dipf.edutec.edutex.androidclient.survey;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import org.json.JSONObject;

import java.time.Instant;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.SurveyActivity;
import de.dipf.edutec.edutex.androidclient.messageservice.VibrationService;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class QuestionnaireNotificationPublisher extends BroadcastReceiver {

    static final String TAG = "SurveyNotificationPublisher";




    @SneakyThrows
    @Override
    public void onReceive(Context context, Intent intent) {

        Instant now = Instant.now();
        long elasped = SystemClock.elapsedRealtimeNanos();

        String received_survey = intent.getStringExtra("message");
        JSONObject received_survey_json = new JSONObject(received_survey);
        Log.d(TAG, "is Called. " + received_survey_json.getString("name") + " " + intent.toString());
        int esm_msg_hash = received_survey.hashCode();

        if(! intent.hasExtra("renotification")){

            //received_survey_json.put("timestamp_question_asked", SurveySingleton.getInstance().checkHash(esm_msg_hash));
            received_survey_json.put("timestamp_question_asked", RequestResponseUtils.getTimeStamps(now,elasped));
            received_survey_json.put("identifier", esm_msg_hash);

        } else {

            received_survey_json.put("timestamp_question_asked",  RequestResponseUtils.getTimeStamps(now, elasped));
        }


        String CHANNEL_ID = context.getResources().getString(R.string.notiChannelID);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent sendIntent = new Intent(context, SurveyActivity.class);
        sendIntent.putExtra("message",received_survey_json.toString());

        PendingIntent pendingIntent = TaskStackBuilder.create(context.getApplicationContext())
                .addNextIntent(sendIntent)
                .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification =  new NotificationCompat.Builder(context,CHANNEL_ID)
                .setContentTitle("Survey")
                .setSmallIcon(R.drawable.ic_survey)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setAutoCancel(true)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(esm_msg_hash, notification);

        Intent intent1 = new Intent(context.getApplicationContext(), VibrationService.class);
        context.getApplicationContext().startForegroundService(intent1);


    }
}
