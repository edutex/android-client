package de.dipf.edutec.edutex.androidclient.survey;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import org.json.JSONObject;

import java.time.Instant;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.SurveyActivity;
import de.dipf.edutec.edutex.androidclient.messageservice.MessageLayerSender;
import de.dipf.edutec.edutex.androidclient.utils.ActionUtils;
import de.dipf.edutec.edutex.androidclient.utils.SessionPreferences;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;


public class SubmitFragment extends Fragment {

    private JSONObject responseSurvey;
    private String TAG = SubmitFragment.class.getSimpleName();

    public SubmitFragment(JSONObject editedSurvey) {
        this.responseSurvey = editedSurvey;
    }


    @Override
    public void onStart(){
        super.onStart();
        ImageButton submit = getView().findViewById(R.id.bt_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @SneakyThrows
            @Override
            public void onClick(View v) {

                Instant now = Instant.now();
                long elapsed = SystemClock.elapsedRealtimeNanos();

                if(responseSurvey.has("request_type")){
                    if(responseSurvey.get("request_type").equals("pre_post_questionnaire")){
                        if(responseSurvey.getInt("questionnaire_type") == 2){

                            // Telling the whole application that the session just started.
                            SessionPreferences sessionPreferences = new SessionPreferences(getActivity().getApplicationContext());
                            sessionPreferences.setSessionRunning(true);
                            sessionPreferences.setSWSessionTimer(System.currentTimeMillis() / 1000);

                            Intent timerIntent = new Intent();
                            timerIntent.setAction(ActionUtils.ACTION_REMOTE_TIMER);
                            timerIntent.putExtra("remote_start_timer", true);
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(timerIntent);
                            Log.d(TAG,"Remote start timer send..");

                        }

                        if(responseSurvey.getInt("questionnaire_type") == 2){
                            responseSurvey.put("android_smartwatch_timestamp_start", RequestResponseUtils.getTimeStamps(now, elapsed));
                        }

                        if(responseSurvey.getInt("questionnaire_type") == 3){
                            responseSurvey.put("android_smartwatch_timestamp_end", RequestResponseUtils.getTimeStamps(now,elapsed));
                            Intent timerIntent = new Intent();
                            timerIntent.setAction(ActionUtils.ACTION_REMOTE_TIMER);
                            timerIntent.putExtra("remote_stop_timer", true);
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(timerIntent);
                            Log.d(TAG, "LocalBroadcast stopping Timer send..");
                        }
                    }
                }

                responseSurvey.put("timestamp_question_answered", RequestResponseUtils.getTimeStamps(now, elapsed));

                MessageLayerSender messageLayerSender = new MessageLayerSender(getContext());
                messageLayerSender.sendAck(getString(R.string.PATH_TO_HANDHELD_RESPONSE_SURVEY),responseSurvey.toString());

                Log.d(TAG, responseSurvey.toString());
                ((SurveyActivity)getActivity()).surveyFinished = true;
                ((SurveyActivity)getActivity()).finish();

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_submit, container, false);
        return view;
    }
}
