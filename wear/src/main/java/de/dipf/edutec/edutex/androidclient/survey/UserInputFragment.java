package de.dipf.edutec.edutex.androidclient.survey;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Locale;
import de.dipf.edutec.edutex.androidclient.R;
import lombok.SneakyThrows;

public class UserInputFragment extends Fragment {

    private static String TAG = "UserInputFragment";

    private ImageButton bt_mic, bt_key, bt_return, bt_key_return, bt_key_submit, bt_micro_return, bt_micro_toggler, bt_micro_submit;
    private LinearLayout lin_buttons, lin_key, lin_mic;
    private SpeechRecognizer sr;
    private TextView txt_user_input;
    private EditText et_key_txt;
    private String pref;
    boolean mircoRunning;
    public UserInputFragment(String preference) {
        this.pref = preference;
        // Required empty public constructor
    }

    @Override
    public void onStart(){
        super.onStart();

        if(ContextCompat.checkSelfPermission(getContext(),Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED){
            checkPermission();
        }

        findViews();
        sr = SpeechRecognizer.createSpeechRecognizer(getActivity().getApplicationContext());
        sr.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {
                txt_user_input.setText("Listening ...");
            }

            @Override
            public void onBeginningOfSpeech() {
                txt_user_input.setText("Listening ...");
            }

            @Override
            public void onRmsChanged(float rmsdB) {
                txt_user_input.setText("onRmsChange ...");
            }

            @Override
            public void onBufferReceived(byte[] buffer) {
                txt_user_input.setText("onBufferedReceived ...");
            }

            @Override
            public void onEndOfSpeech() {
                txt_user_input.setText("onEndOfSpeech ...");
            }

            @Override
            public void onError(int error) {
                txt_user_input.setText("onError: " + String.valueOf(error));
                return;
            }

            @Override
            public void onResults(Bundle results) {
                Log.d(TAG, results.keySet().toString());
                ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                txt_user_input.setText(data.get(0));
                sr.cancel();
                mircoRunning = false;
                bt_micro_toggler.setImageResource(R.drawable.ic_mic_white_40dp);
            }

            @Override
            public void onPartialResults(Bundle partialResults) {
                ArrayList<String> data = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                txt_user_input.setText(data.get(0));

            }

            @Override
            public void onEvent(int eventType, Bundle params) {
                txt_user_input.setText("onEvent ...");
            }
        });

        bt_mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lin_buttons.setVisibility(View.GONE);
                lin_mic.setVisibility(View.VISIBLE);
            }
        });
        bt_key.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lin_buttons.setVisibility(View.GONE);
                lin_key.setVisibility(View.VISIBLE);
            }
        });

        bt_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListener != null) mListener.onReturn();
                Toast.makeText(getContext(),"onClick", Toast.LENGTH_SHORT).show();
                sr.destroy();
            }
        });

        bt_key_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lin_key.setVisibility(View.GONE);
                lin_buttons.setVisibility(View.VISIBLE);
            }
        });
        bt_micro_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lin_mic.setVisibility(View.GONE);
                lin_buttons.setVisibility(View.VISIBLE);
            }
        });

        bt_micro_toggler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mircoRunning){
                    final Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                    intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS,1 );
                    sr.startListening(intent);
                    mircoRunning = true;
                    txt_user_input.setText("Preparing..");
                    bt_micro_toggler.setImageResource( R.drawable.ic_baseline_360_24);
                } else {
                    sr.cancel();
                    mircoRunning = false;
                    bt_micro_toggler.callOnClick();
                }

            }
        });
        bt_micro_submit.setOnClickListener(new View.OnClickListener() {
            @SneakyThrows
            @Override
            public void onClick(View view) {
                // ToDo Sumbit
                String toSubmit = txt_user_input.getText().toString();
                if(mListener != null) mListener.onStateChange(toSubmit);
                onDestroy();

            }
        });
        bt_key_submit.setOnClickListener(new View.OnClickListener() {
            @SneakyThrows
            @Override
            public void onClick(View view) {
                // ToDo Submit
                String toSubmit = et_key_txt.getText().toString();
                if(mListener != null) mListener.onStateChange(toSubmit);
                onDestroy();
            }
        });

        // Disable
        if(this.pref.equals("keyboard_only")){

            bt_mic.setVisibility(View.GONE);
        }

        if(this.pref.equals("voice_only")){

            bt_key.setVisibility(View.GONE);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_input, container, false);
        return v;
    }


    public void findViews(){
        bt_mic = getView().findViewById(R.id.bt_micro);
        bt_key = getView().findViewById(R.id.bt_keyboard);
        bt_return = getView().findViewById(R.id.bt_return);
        lin_buttons = getView().findViewById(R.id.lin_buttons);
        lin_key = getView().findViewById(R.id.lin_key);
        lin_mic = getView().findViewById(R.id.lin_micro);
        bt_key_return = getView().findViewById(R.id.bt_key_return);
        bt_key_submit = getView().findViewById(R.id.bt_key_submit);
        bt_micro_return = getView().findViewById(R.id.bt_micro_return);
        bt_micro_toggler = getView().findViewById(R.id.bt_micro_toggler);
        bt_micro_submit = getView().findViewById(R.id.bt_micro_submit);
        txt_user_input = getView().findViewById(R.id.txt_user_input);
        et_key_txt = getView().findViewById(R.id.et_key_txt);
    }


    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.RECORD_AUDIO},1);
        }
    }

    // Variabel change Listener
    private UserInputFragment.Listener mListener = null;
    public void registerListener(UserInputFragment.Listener listener) {mListener = listener; }
    public void unregisterListener(){mListener = null;}
    public interface Listener{
        void onStateChange(String user_input) throws JSONException;
        void onReturn();
    }

}
