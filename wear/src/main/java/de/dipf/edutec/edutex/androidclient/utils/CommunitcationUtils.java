package de.dipf.edutec.edutex.androidclient.utils;

public class CommunitcationUtils {

    public static final String TOHANDHELD_REQUEST_SESSION_STATE = "/tohandheld/request_session_state";
    public static final String TOHANDHELD_ESM_SERVICE_STOP = "/tohandheld/esm_service_stop";
    public static final String TOHANDHELD_RESPONSE_SENSOR_REQUEST = "/tohandheld/response_sensor_request";



    public static final String TOSMARTWATCH_SURVEY = "/tosmartwatch/survey";
    public static final String TOSMARTWATCH_SURVEY_PRE = "/tosmartwatch/survey_pre";
    public static final String TOSMARTWATCH_SURVEY_POST = "/tosmartwatch/survey_post";
    public static final String TOSMARTWATCH_MICRO_SURVEY = "/tosmartwatch/micro_survey";
    public static final String TOSMARTWATCH_NOTIFICATION = "/tosmartwatch/notification";
    public static final String TOSMARTWATCH_REQUEST_RECOVERY = "/tosmartwatch/recovery_question";
    public static final String TOSMARTWATCH_RESPONSE_RECOVERY = "/tosmartwatch/response_recovery";
    public static final String TOSMARTWATCH_CONTROL_FLOW = "/tosmartwatch/control_flow";
    public static final String TOSMARTWATCH_RESPONSE_SESSION_STATE = "/tosmartwatch/response_session_state";
    public static final String TOSMARTWATCH_BACKEND_SENSOR_REQUEST = "/tosmartwatch/backend_request";

}
