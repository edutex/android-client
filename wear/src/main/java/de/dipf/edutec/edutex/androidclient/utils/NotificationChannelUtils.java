package de.dipf.edutec.edutex.androidclient.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import java.util.List;

public class NotificationChannelUtils {

    private static final String TAG = NotificationChannelUtils.class.getSimpleName();

    public static String NOTIFICATION_CHANNEL_ID = "EsmChannelId07";
    public static String NOTIFICATION_CHANNEL_NAME = "ESM02";



    public static void create_notification_channel_if_not_exist(Context context) {
        String channelID = NOTIFICATION_CHANNEL_ID;
        String channelName = NOTIFICATION_CHANNEL_NAME;
        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        List<NotificationChannel> channels = notificationManager.getNotificationChannels();

        boolean channel_already_exists = false;

        for (int i = 0; i < channels.size(); i++) {
            String channel_name = channels.get(i).getId();
            if (channel_name.equals(channelID)) {
                channel_already_exists = true;
                break;
            }
        }

        if (!channel_already_exists) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                int importance = NotificationManager.IMPORTANCE_HIGH;

                NotificationChannel channel = new NotificationChannel(channelID, channelName, importance);
                //channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                channel.enableVibration(true);
                channel.setVibrationPattern(new long[]{0, 1000, 0, 1000, 0, 1000, 0, 1000});

                notificationManager.createNotificationChannel(channel);
                Log.d(TAG, "NotificationChannel: " + channelName + " was created");
            }
        } else {
            Log.d(TAG, "NotificationChannel: " + channelName + " already exists. Do not need to recreate.");
        }

    }


}
