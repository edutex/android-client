package de.dipf.edutec.edutex.androidclient.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.time.Instant;

public class SessionPreferences {

    private static String TAG = SessionPreferences.class.getSimpleName();
    private static String PREFS_NAME = "tracking_sensor_preferences";

    private static final String KEY_SESSION_TERMINATED = "session_terminated";
    private static final String KEY_SESSION_RUNNING = "session_running";
    private static final String KEY_SESSION_REQUESTED_PRE = "session_running";
    private static final String KEY_SESSION_REQUESTED_INTER = "session_running";
    private static final String KEY_SESSION_REQUESTED_POST = "session_running";
    private static final String KEY_SESSION_ID = "session_id";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_SW_STARTING_POINT = "sw_starting_point";

    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_HOST_PORT = "host_port_combination";
    private static final String KEY_ACRA_SET = "is_acra_set";
    private static final String KEY_ACRA_SET_TS = "timestamp_acra_set";

    private static final String KEY_PRIVACY_NOTIFICATION_SEND = "notified_privacy";

    private static final String KEY_CURRENT_STUDY_MECHANICS = "study_mechanics";
    private static final String KEY_USED_DEVICE = "used_device";
    private static final String KEY_ERROR = "error_key";

    private static final String KEY_SESSION_IN_RECOVERY = "session_in_recovery";

    private SharedPreferences sharedPreferences;
    private Context context;


    public SessionPreferences(Context context){
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    // Setter Functions
    public void setSessionTerminated(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_TERMINATED,value);
        editor.commit();

    }

    public void setError(String error){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_SESSION_RUNNING,error);
        editor.commit();
    }

    public void setSessionRunning(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_RUNNING,value);
        editor.commit();
    }
    public void setSessionId(String id){
        Log.d(TAG,"Session ID is set: " + id);
        if(id == "-"){return;}
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_SESSION_ID,id);
        editor.commit();

    }

    public void setStudyStartMechanics(int mechanics){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(KEY_CURRENT_STUDY_MECHANICS,mechanics);
        editor.commit();
    }
    public void setSessionRequestedPre(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_REQUESTED_PRE,value);
        editor.commit();
    }
    public void setSessionRequestedInter(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_REQUESTED_INTER,value);
        editor.commit();
    }
    public void setSessionRequestedPost(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_REQUESTED_POST,value);
        editor.commit();
    }
    public void setUserId(String id){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_USER_ID, id);
        editor.commit();
    }
    public void setSWSessionTimer(long value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putLong(KEY_SW_STARTING_POINT, value);
        editor.commit();
    }

    public void setUsername(String name){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_USERNAME, name);
        editor.commit();
    }
    public void setPassword(String password){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_PASSWORD, password);
        editor.commit();
    }
    public void setHostPort(String url){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_HOST_PORT, url);
        editor.commit();
    }
    public void setIsAcraSet(boolean value){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        if(value){
            Log.d(TAG,"ACRA is set.");
            long new_ts = Instant.now().toEpochMilli() / 1000;
            Log.d(TAG, "ACRA new timestamp: " + String.valueOf(new_ts));
            editor.putLong(KEY_ACRA_SET_TS,new_ts );
            editor.commit();
        }


        editor.putBoolean(KEY_ACRA_SET,value);
        editor.commit();


    }

    public void setUsedDevice(int i){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(KEY_USED_DEVICE, i);
        editor.commit();
    }

    public void setPrivacyNotificationDisplayed(boolean value){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_PRIVACY_NOTIFICATION_SEND,value);
        editor.commit();
    }

    public boolean isAcraSet(){
        boolean returnValue = false;

        long seconds = Instant.now().toEpochMilli() / 1000;

        long seconds_last_set = this.sharedPreferences.getLong(KEY_ACRA_SET_TS,-1);

        if(seconds_last_set == -1){
            Log.d(TAG,"ACRE need to get renewed.");

        } else {
            seconds_last_set = seconds_last_set;
            long difference = seconds - seconds_last_set;
            if((difference/3600) < 8){
                Log.d(TAG, "ACRA is fine");
                returnValue = this.sharedPreferences.getBoolean(KEY_ACRA_SET,false);
            } else {
                Log.d(TAG, "ACRA is overtimed: " + String.valueOf(difference/3600));

            }

        }

        return returnValue;

    }

    public void sessionInRecovery(boolean bool){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_SESSION_IN_RECOVERY, bool);
        editor.commit();
    }


    public boolean isSessionInRecovery(){return this.sharedPreferences.getBoolean(KEY_SESSION_IN_RECOVERY,false);}


    // Getter Functions
    public Boolean isSessionRunning(){return this.sharedPreferences.getBoolean(KEY_SESSION_RUNNING,false);}
    public Boolean getRequestedPre(){return this.sharedPreferences.getBoolean(KEY_SESSION_REQUESTED_PRE,false);}
    public Boolean getRequestedInter(){return this.sharedPreferences.getBoolean(KEY_SESSION_REQUESTED_INTER,false);}
    public Boolean getRequestedPost(){return this.sharedPreferences.getBoolean(KEY_SESSION_REQUESTED_POST,false);}
    public Boolean isSessionTerminated(){return this.sharedPreferences.getBoolean(KEY_SESSION_TERMINATED,false);}
    public String getSessionId(){return this.sharedPreferences.getString(KEY_SESSION_ID,"-");}
    public String getUserId(){return this.sharedPreferences.getString(KEY_USER_ID,"");}
    public Long getSWSessionTimer(){return this.sharedPreferences.getLong(KEY_SW_STARTING_POINT,-1);}
    public String getUsername(){return this.sharedPreferences.getString(KEY_USERNAME,null);}
    public String getPassword(){return this.sharedPreferences.getString(KEY_PASSWORD, null);}
    public String getHostPort(){return this.sharedPreferences.getString(KEY_HOST_PORT, null);}
    public int getStudyStartMechanics(){return this.sharedPreferences.getInt(KEY_CURRENT_STUDY_MECHANICS,0);}
    public boolean getPrivayNotificationSended(){return this.sharedPreferences.getBoolean(KEY_PRIVACY_NOTIFICATION_SEND,false);}
    public int getUsedDevice(){return this.sharedPreferences.getInt(KEY_USED_DEVICE,0);}
    // Utility Functions
    public void createInternalSessionVariables(){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        if(! this.sharedPreferences.contains(KEY_SESSION_TERMINATED)){
            setSessionTerminated(false);
            setSessionRunning(false);
            setSessionRequestedPre(false);
            setSessionRequestedInter(false);
            setSessionRequestedPost(false);
        }


    }




}
