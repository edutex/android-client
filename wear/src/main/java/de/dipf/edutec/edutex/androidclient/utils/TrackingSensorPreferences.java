package de.dipf.edutec.edutex.androidclient.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONObject;

import lombok.SneakyThrows;

public class TrackingSensorPreferences {

    private static String PREFS_NAME = "tracking_sensor_preferences";

    private static final String KEY_TRACK_APP_USAGE = "track_app_usage";
    private static final String KEY_TRACK_WINDOW_CHANGE = "track_window_change";
    private static final String KEY_REQUEST = "track_request";
    private static final String KEY_ORIGINAL_REQUEST = "track_original_request";
    private static final String KEY_ASKED_FOR_ACCESSIBILITY = "asked_for_accessibility";


    private SharedPreferences sharedPreferences;
    private Context context;


    public TrackingSensorPreferences(Context context){
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void setAskedForAccessibility(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_ASKED_FOR_ACCESSIBILITY,value);
        editor.apply();
    }


    public Boolean getAskedForAccessibility(){
        return this.sharedPreferences.getBoolean(KEY_ASKED_FOR_ACCESSIBILITY,false);

    }


    // Setter Functions
    public void setTrackAppUsage(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_TRACK_APP_USAGE,value);
        editor.apply();
    }
    public void setTrackWindowChangeDetection(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_TRACK_WINDOW_CHANGE,value);
        editor.apply();
    }
    public void setTrackRequest(JSONObject service){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_REQUEST,service.toString());
        editor.apply();
    }
    public void setTrackOriginalRequest(JSONObject original){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_ORIGINAL_REQUEST,original.toString());
        editor.apply();
    }

    // Getter Functions
    public Boolean getTrackAppUsage(){return sharedPreferences.getBoolean(KEY_TRACK_APP_USAGE,false);}
    public Boolean getTrackWindowChange(){return sharedPreferences.getBoolean(KEY_TRACK_WINDOW_CHANGE,false);}
    @SneakyThrows
    public JSONObject getTrackOriginalRequest(){
        String request = sharedPreferences.getString(KEY_ORIGINAL_REQUEST, "");
        return new JSONObject(request);
    }
    @SneakyThrows
    public JSONObject getTrackRequest(){
        String request = sharedPreferences.getString(KEY_REQUEST, "");
        return new JSONObject(request);
    }

    // Utility Functions
    public void setAllTrue(){
        setTrackAppUsage(true);
        setTrackWindowChangeDetection(true);

    }
    public void setAllFalse(){
        setTrackWindowChangeDetection(false);
        setTrackAppUsage(false);

    }





}
